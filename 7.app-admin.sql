ALTER TABLE `file` ADD `cid` int(11) NOT NULL DEFAULT '0';
ALTER TABLE `file` ADD `appAdmin` TINYINT(1) NOT NULL DEFAULT '0';

ALTER TABLE `file` ADD `appId` int(11) NOT NULL DEFAULT '1';
ALTER TABLE `item` ADD `appId` int(11) NOT NULL DEFAULT '1';
ALTER TABLE `user` ADD `appId` int(11) NOT NULL DEFAULT '1';
ALTER TABLE `installer` ADD `appId` int(11) NOT NULL DEFAULT '1';
ALTER TABLE `company` ADD `appId` int(11) NOT NULL DEFAULT '1';

ALTER TABLE `company` ADD `appAdmin` TINYINT(1) NOT NULL DEFAULT '0';

INSERT INTO `user` (`uid`, `cid`, `email`, `password`, `is_admin`, `active`, `marked_id`, `marked_quiz`, `appId`) VALUES
(999, 1, 'god@heaven.com', '21232f297a57a5a743894a0e4a801fc3', 4, 1, 0, 0, 0);

UPDATE `company` SET `appAdmin`=1 WHERE `cid`=1