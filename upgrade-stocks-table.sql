-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `stock_id` int(11) NOT NULL,
  `installer_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_allocation`
--

CREATE TABLE `stock_allocation` (
  `stock_allocation_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_usage`
--

CREATE TABLE `stock_usage` (
  `stock_usage_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `job_refference` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------
--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`stock_id`);
--
-- Indexes for table `stock_allocation`
--
ALTER TABLE `stock_allocation`
  ADD PRIMARY KEY (`stock_allocation_id`);
--
-- Indexes for table `stock_usage`
--
ALTER TABLE `stock_usage`
  ADD PRIMARY KEY (`stock_usage_id`);
--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `stock_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stock_allocation`
--
ALTER TABLE `stock_allocation`
  MODIFY `stock_allocation_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stock_usage`
--
ALTER TABLE `stock_usage`
  MODIFY `stock_usage_id` int(11) NOT NULL AUTO_INCREMENT;