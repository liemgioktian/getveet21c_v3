$(document).ready(function(){
	
	/** find next input when enter  */
    var $input = $('input');
    $input.on('keypress', function(e) {
        if (e.which === 13) {
            var ind = $input.index(this);
            $input.eq(ind + 1).focus();
            e.preventDefault();
        }
    });

    /** initial date-picker */
        $('.datepicker').datepicker({
            'dateFormat' : 'dd-mm-yy'
        });

});