$(document).ready(function(){
  $('.btn-34-submit').unbind('click').bind('click', function() {
    var data = {}
    for (var s=1; s<=3; s++) {
      var elId = 'sign'+s
      var elObj = $('#'+elId)
      var base30 = elObj.jSignature('getData', 'base30')
      if (base30[1] !== '') elObj.append('<input type="hidden" name="'+elId+'" value="'+base30[1]+'">')
    }
    $('form').each(function() {data[$(this).attr('id')] = $(this).serialize()})
    $.post(window.location.href, data, function (fid) {
      window.location = site_url + '/form34controller/edit/' + fid
    })
  })
/*
*   init calx
*/
    var $calc = $('#assignment34').calx();
    var $calc1 = $('#survey').calx();
    var $calc2 = $('#quote').calx();
    select2Product34()

    $('#sidebar').affix({
      offset: {
        top: 230,
        bottom: 100
      }
    }); 
    $('#midCol').affix({
          offset: {
            top: 230,
            bottom: 100
          }
    }); 
    
/** find next input when enter  */
    var $input = $('input');
    $input.on('keypress', function(e) {
        if (e.which === 13) {
            var ind = $input.index(this);
            $input.eq(ind + 1).focus();
            e.preventDefault();
        }
    });
/*
*   Hide each categories when page load
*/
    /*
    var $a;
    for ( $a=1; $a <=20; $a++) {
        $('.row_class'+$a).hide();

    };


/** initial date-picker */
    $('.datepicker').datepicker({
        'dateFormat' : 'dd-mm-yy'
    });

/*
*   up down button on each categories assignment form
*/

    $('.row_up_button').click(function(e){
        e.preventDefault();
        var $data_row_id = $(this).attr('data-button');
        $('.row_class'+$data_row_id).hide('');
        $('#row_down'+$data_row_id).removeClass('hidden');
        $('#row_up'+$data_row_id).addClass('hidden');
    });

    $('.row_down_button').click(function(e){
        e.preventDefault();
        var $data_row_id = $(this).attr('data-button');
        $('.row_class'+$data_row_id).show('');
        $('#row_down'+$data_row_id).addClass('hidden');
        $('#row_up'+$data_row_id).removeClass('hidden');
    });

/*
*   up down button on each categories quote page
*/

    $('.quote_up_button').click(function(e){
        e.preventDefault();
        var $data_row_id = $(this).attr('data-button');
        $('.quote_class'+$data_row_id).hide('');
        $('#quote_down'+$data_row_id).removeClass('hidden');
        $('#quote_up'+$data_row_id).addClass('hidden');
    });

    $('.quote_down_button').click(function(e){
        e.preventDefault();
        var $data_row_id = $(this).attr('data-button');
        $('.quote_class'+$data_row_id).show('');
        $('#quote_down'+$data_row_id).addClass('hidden');
        $('#quote_up'+$data_row_id).removeClass('hidden');
    });
/*
*   hide input field
*/  
    $('#AE25').change(function(){
        $AE25 = $('#AE25').val();
        if($(this).val() == 1 ){
            $('#coes').removeClass('hidden');
            $('#electrician_sign').addClass('hidden');
        }else{
            $('#coes').addClass('hidden');
            $('#electrician_sign').removeClass('hidden');
        }
    });

/** 
* select2 init
*/
    $(".industry").select2({
        data:{ results: industry, text: 'id' },
        formatSelection: function(item){
            
           
            //$('#AS31').val(item.value).trigger('change');
            return item.id;
        },
        formatResult: function(item){
            return item.id;
        }
    });

    $(".postcode").select2({
        data:{ results: postcode, text: 'id' },
        formatSelection: function(item){
            //var count = this.element.attr('data-count');
           
            //$('#survey').calx('getCell', 'AW69').setValue(item.value).calculate();
            $('#survey').calx('getCell', 'Y10').setValue(item.value).calculate();
            $('#survey').calx('getSheet').calculate();
            return item.id;

        },
        formatResult: function(item){
            return item.id;

        }
    });

    $(".existing_lamp").select2({
        data:{ results: existing_lamp, text: 'id' },
        formatSelection: function(item){
            var count = this.element.attr('data-count');
            $('#survey').calx('getCell', 'HA'+count).setValue(item.multi).calculate();
            $('#survey').calx('getCell', 'HB'+count).setValue(item.plus).calculate();
            $('#survey').calx('getCell', 'HC'+count).setValue(item.rate).calculate();
            $('#survey').calx('getCell', 'HD'+count).setValue(item.max).calculate();
            $('#survey').calx('getSheet').calculate();
            return item.id;

        },
        formatResult: function(item){
            return item.id;

        }
    });

/*
*   add row lighting survey
*/  $row_num = $('#survey_body tr').length + 14;
    $('#btn-add-survey').click(function(e){
        e.preventDefault();
        $row_num++;
        if ($row_num==24) {
            $('#btn-add-survey').addClass('hidden');
        }else{
            $('#btn-add-survey').removeClass('hidden');
        };
            var $survey_body = $('#survey_body');
            $survey_body.append(
                '<tr id="row1">'+
                    '<td class="text-center">'+
                        '<label for="" id="B'+$row_num+'" name="B'+$row_num+'" data-cell="B'+$row_num+'" data-formula="A'+$row_num+'"></label>'+
                        '<input type="hidden" id="A'+$row_num+'" name="A'+$row_num+'" data-cell="A'+$row_num+'" data-formula="'+$row_num+'-14">'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow width-'+$row_num+'0" id="C'+$row_num+'" name="C'+$row_num+'" data-cell="C'+$row_num+'"></td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow existing_lamp width-250" id="H'+$row_num+'" name="H'+$row_num+'" data-cell="H'+$row_num+'" data-count="'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="HA'+$row_num+'" name="HA'+$row_num+'" data-cell="HA'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="HB'+$row_num+'" name="HB'+$row_num+'" data-cell="HB'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="HC'+$row_num+'" name="HC'+$row_num+'" data-cell="HC'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="HD'+$row_num+'" name="HD'+$row_num+'" data-cell="HD'+$row_num+'">'+
                    '</td>'+
                    '<td class="hidden">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="N'+$row_num+'" name="N'+$row_num+'" data-cell="N'+$row_num+'" data-formula="IF(HD'+$row_num+' = '+'yes'+',IF(O'+$row_num+'>37,37,O'+$row_num+'),O'+$row_num+')">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="P'+$row_num+'" name="P'+$row_num+'" data-cell="P'+$row_num+'" data-formula="(N'+$row_num+'+0)*HA'+$row_num+'+HB'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="T'+$row_num+'" name="T'+$row_num+'" data-cell="T'+$row_num+'" data-formula="Q'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="V'+$row_num+'" name="V'+$row_num+'" data-cell="V'+$row_num+'" data-formula="U'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="Z'+$row_num+'" name="Z'+$row_num+'" data-cell="Z'+$row_num+'" data-formula="IF(W'+$row_num+'=0,0,IF(AL'+$row_num+'=0,(P'+$row_num+'*30000*T'+$row_num+'*V'+$row_num+'*W'+$row_num+')/1000000,(P'+$row_num+'*AK'+$row_num+'*T'+$row_num+'*V'+$row_num+'*W'+$row_num+')/1000000))">'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow width-50" id="O'+$row_num+'" name="O'+$row_num+'" data-cell="O'+$row_num+'"></td>'+
                    '<td class="">'+
                        '<select class="input-sm form-control yellow width-250" name="Q'+$row_num+'" id="Q'+$row_num+'" data-cell="Q'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">No control system</option>'+
                            '<option value="0.7">Occupancy sensor</option>'+
                            '<option value="0.7">Day-light linked control</option>'+
                            '<option value="0.85">Programmable dimming</option>'+
                            '<option value="0.9">Manual dimming</option>'+
                            '<option value="0.744">VRU</option>'+
                            '<option value="0.6">Occupancy & Daylight Control</option>'+
                            '<option value="0.6">Occupancy & Program. Dimmer</option>'+
                            '<option value="0.63">Occupancy & Manual Dimmer</option>'+
                            '<option value="0.6">Occupancy & VRU</option>'+
                            '<option value="0.6">Daylight Control & Program. Dimmer</option>'+
                            '<option value="0.63">Daylight Control & Manual Dimmer</option>'+
                            '<option value="0.6">Daylight Control & VRU</option>'+
                            '<option value="0.765">Program. Dimmer & Manual Dimmer</option>'+
                            '<option value="0.6324">Program. Dimmer & VRU</option>'+
                            '<option value="0.6696">Manual Dimmer & VRU</option>'+
                            '</select>'+
                        '</td>'+
                    '<td class="">'+
                        '<select class="input-sm form-control yellow" name="U'+$row_num+'" id="U'+$row_num+'" data-cell="U'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">No</option>'+
                            '<option value="1.05">Yes</option>'+
                        '</select>'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow width-50" id="W'+$row_num+'" name="W'+$row_num+'" data-cell="W'+$row_num+'"></td>'+
                    '<td class="">'+
                        '<select class="input-sm form-control yellow width-250" name="Y'+$row_num+'" id="Y'+$row_num+'" data-cell="Y'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">Delamping from multiple fittings</option>'+
                            '<option value="2">Replace lamp only</option>'+
                            '<option value="replace_lamp">Replace lamp and control gear </option>'+
                            '<option value="replace_full">Replace full luminaire</option>'+
                            '<option value="1">Install VRU or sensor only (on existing lighting)</option>'+
                        '</select>'+
                    '</td>'+
                    '<td class="hidden">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AL'+$row_num+'" name="AL'+$row_num+'" data-cell="AL'+$row_num+'" data-formula="IF(Y'+$row_num+'='+"'replace_lamp'"+',0,IF(Y'+$row_num+'='+"'replace_full'"+',0,Y'+$row_num+'))">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AP'+$row_num+'" name="AP'+$row_num+'" data-cell="AP'+$row_num+'" data-formula="AM'+$row_num+'">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AR'+$row_num+'" name="AR'+$row_num+'" data-cell="AR'+$row_num+'" data-formula="AQ'+$row_num+'">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AT'+$row_num+'" name="AT'+$row_num+'" data-cell="AT'+$row_num+'" data-formula="IF(AS'+$row_num+'=0,0,IF(AL'+$row_num+'=1,((P'+$row_num+'*AK'+$row_num+'*AP'+$row_num+'*AR'+$row_num+'*AS'+$row_num+')/1000000),IF(AL'+$row_num+'=2,((AI'+$row_num+'*AK'+$row_num+'*AP'+$row_num+'*AR'+$row_num+'*AS'+$row_num+')/1000000),((AI'+$row_num+'*30000*AP'+$row_num+'*AR'+$row_num+'*AS'+$row_num+')/1000000))))">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AV'+$row_num+'" name="AV'+$row_num+'" data-cell="AV'+$row_num+'" data-formula="IF(AS'+$row_num+'=0,0,(Z'+$row_num+'- AT'+$row_num+'))" data-format="0.0">'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow width-'+$row_num+'0 product34" id="AA'+$row_num+'" name="AA'+$row_num+'" data-cell="AA'+$row_num+'"></td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow width-50" id="AI'+$row_num+'" name="AI'+$row_num+'" data-cell="AI'+$row_num+'"></td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow" id="AK'+$row_num+'" name="AK'+$row_num+'" data-cell="AK'+$row_num+'" data-format="0,0[.]"></td>'+
                    '<td class="">'+
                        '<select class="input-sm form-control yellow width-250" name="AM'+$row_num+'" id="AM'+$row_num+'" data-cell="AM'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">No control system</option>'+
                            '<option value="0.7">Occupancy sensor</option>'+
                            '<option value="0.7">Day-light linked control</option>'+
                            '<option value="0.85">Programmable dimming</option>'+
                            '<option value="0.9">Manual dimming</option>'+
                            '<option value="0.744">VRU</option>'+
                            '<option value="0.6">Occupancy & Daylight Control</option>'+
                            '<option value="0.6">Occupancy & Program. Dimmer</option>'+
                            '<option value="0.63">Occupancy & Manual Dimmer</option>'+
                            '<option value="0.6">Occupancy & VRU</option>'+
                            '<option value="0.6">Daylight Control & Program. Dimmer</option>'+
                            '<option value="0.63">Daylight Control & Manual Dimmer</option>'+
                            '<option value="0.6">Daylight Control & VRU</option>'+
                            '<option value="0.765">Program. Dimmer & Manual Dimmer</option>'+
                            '<option value="0.6324">Program. Dimmer & VRU</option>'+
                            '<option value="0.6696">Manual Dimmer & VRU</option>'+
                        '</select>'+
                    '</td>'+
                    '<td class="">'+
                        '<select class="input-sm form-control yellow" name="AQ'+$row_num+'" id="AQ'+$row_num+'" data-cell="AQ'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">No</option>'+
                            '<option value="1.05">Yes</option>'+
                        '</select>'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow width-50" id="AS'+$row_num+'" name="AS'+$row_num+'" data-cell="AS'+$row_num+'"></td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow" id="AU'+$row_num+'" name="AU'+$row_num+'" data-cell="AU'+$row_num+'" data-formula="IF(AS'+$row_num+'=0,0,3000)"></td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow" id="AW'+$row_num+'" name="AW'+$row_num+'" data-cell="AW'+$row_num+'" data-formula="IFERROR(IF(AV'+$row_num+'>0,IF(AW9=0,(AV'+$row_num+'*BS106)*0.963,0),0),0)" data-format="0.0"></td>'+
               '</tr>'
            );

            var $quote_body = $('#quote_body');
            var $row_quote = $row_num - 2;
            $quote_body.append(
                '<tr>'+
                    '<td class="text-center">'+
                        '<label for="" id="B'+$row_quote+'" name="B'+$row_quote+'" data-cell="B'+$row_quote+'" data-formula="C'+$row_quote+'"></label>'+
                        '<input type="hidden" class="input-sm form-control text-center" id="C'+$row_quote+'" name="C'+$row_quote+'" data-cell="C'+$row_quote+'" data-formula="'+$row_num+'-14">'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center width-250" id="D'+$row_quote+'" name="D'+$row_quote+'" data-cell="D'+$row_quote+'" data-formula="#survey!C'+$row_num+'"></td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center width-250" id="G'+$row_quote+'" name="G'+$row_quote+'" data-cell="G'+$row_quote+'" data-formula="#survey!AA'+$row_num+'"></td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center" id="M'+$row_quote+'" name="M'+$row_quote+'" data-cell="M'+$row_quote+'" data-formula="#survey!AS'+$row_num+'"></td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow" data-format="$ 0,0[.]00" id="O'+$row_quote+'" name="O'+$row_quote+'" data-cell="O'+$row_quote+'">'+
                        '<input type="hidden" class="input-sm form-control text-center" id="N'+$row_quote+'" name="N'+$row_quote+'" data-cell="N'+$row_quote+'" data-formula="O'+$row_quote+'*M'+$row_quote+'">'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="Q'+$row_quote+'" name="Q'+$row_quote+'" data-cell="Q'+$row_quote+'">'+
                        '<input type="hidden" class="input-sm form-control text-center" id="P'+$row_quote+'" name="P'+$row_quote+'" data-cell="P'+$row_quote+'" data-formula="Q'+$row_quote+'*M'+$row_quote+'">'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="S'+$row_quote+'" name="S'+$row_quote+'" data-cell="S'+$row_quote+'">'+
                        '<input type="hidden" class="input-sm form-control text-center" id="R'+$row_quote+'" name="R'+$row_quote+'" data-cell="R'+$row_quote+'" data-formula="S'+$row_quote+'*M'+$row_quote+'">'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="U'+$row_quote+'" name="U'+$row_quote+'" data-cell="U'+$row_quote+'">'+
                        '<input type="hidden" class="input-sm form-control text-center" id="T'+$row_quote+'" name="T'+$row_quote+'" data-cell="T'+$row_quote+'" data-formula="U'+$row_quote+'*M'+$row_quote+'">'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="W'+$row_quote+'" name="W'+$row_quote+'" data-cell="W'+$row_quote+'" data-formula="IF(((O'+$row_quote+'+Q'+$row_quote+'+S'+$row_quote+'+U'+$row_quote+')*M'+$row_quote+')>0,((O'+$row_quote+'+Q'+$row_quote+'+S'+$row_quote+'+U'+$row_quote+')*M'+$row_quote+'),0)"></td>'+
                '</tr>'
            )
            
            $(".existing_lamp").select2({
                data:{ results: existing_lamp, text: 'id' },
                formatSelection: function(item){
                    var count = this.element.attr('data-count');
                    $('#survey').calx('getCell', 'HA'+count).setValue(item.multi).calculate();
                    $('#survey').calx('getCell', 'HB'+count).setValue(item.plus).calculate();
                    $('#survey').calx('getCell', 'HC'+count).setValue(item.rate).calculate();
                    $('#survey').calx('getCell', 'HD'+count).setValue(item.max).calculate();
                    $('#survey').calx('getSheet').calculate();
                    return item.id;

                },
                formatResult: function(item){
                    return item.id;

                }
            });
            select2Product34()

            $('#survey, #quote').calx('update').calx('calculate');
            /** find next input when enter  */
            var $input = $('input');
            $input.on('keypress', function(e) {
                if (e.which === 13) {
                    var ind = $input.index(this);
                    $input.eq(ind + 1).focus();
                    e.preventDefault();
                }
            });
    });

/*
*   add row addtional cost
*/  $row_cost = 54;
    $('#btn-add-cost').click(function(e){
        e.preventDefault();
        $row_cost++;
        if ($row_cost==58) {
            $('#btn-add-cost').addClass('hidden');
        }else{
            $('#btn-add-cost').removeClass('hidden');
        };
            var $quote_body2 = $('#quote_body2');
            $quote_body2.append(
                '<tr>'+
                    '<td class="text-center">'+
                        '<label class="width-50" for="" id="C'+$row_cost+'" name="C'+$row_cost+'" data-cell="C'+$row_cost+'" data-formula="B'+$row_cost+'"></label>'+
                        '<input type="hidden" class="input-sm form-control text-center" id="B'+$row_cost+'" name="B'+$row_cost+'" data-cell="B'+$row_cost+'" data-formula="'+$row_cost+'-53">'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center" id="D'+$row_cost+'" name="D'+$row_cost+'" data-cell="D'+$row_cost+'"></td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center width-150" data-format="$ 0,0[.]00" id="W'+$row_cost+'" name="W'+$row_cost+'" data-cell="W'+$row_cost+'"></td>'+
                '</tr>'
            );

     
            $('#quote').calx('update').calx('calculate');
            /** find next input when enter  */
            var $input = $('input');
            $input.on('keypress', function(e) {
                if (e.which === 13) {
                    var ind = $input.index(this);
                    $input.eq(ind + 1).focus();
                    e.preventDefault();
                }
            });
    });


/*
*   refresh graph 
*/


/*
*   jsignature
*/
    $("#sign1, #sign2, #sign3").jSignature({lineWidth:1, height:200, width:400})
    for (var s=1; s<=3; s++) {
      $('#sign'+s).jSignature('setData', signatureData[s], 'base30')
    }
    var $sign1 = $('#sign1');
    var $sign2 = $('#sign2');
    var $sign3 = $('#sign3');

    $('#resetSign2').click(function(e){
        e.preventDefault();
        $sign2.jSignature("reset");
    });

/*
* geo location
*/
    
    var map;

    function initialize() {
      var mapOptions = {
        zoom: 17
      };
      map = new google.maps.Map(document.getElementById('map-canvas'),
          mapOptions);

      // Try HTML5 geolocation
      if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          if('' !== mapsrc){
            mapsrc = mapsrc.split("&markers=");
            mapsrc = mapsrc[0].replace('https://maps.googleapis.com/maps/api/staticmap?center=','');
            mapsrc = mapsrc.split(',');
            var pos = new google.maps.LatLng(mapsrc[0],mapsrc[1]);
          }else{
            jQuery('[name="mapsrc"]').val(
                "https://maps.googleapis.com/maps/api/staticmap?center="+
                position.coords.latitude+","+position.coords.longitude+
                "&markers="+
                position.coords.latitude+","+position.coords.longitude+
                "&zoom=15&size=700x475");
            var pos = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
          }

          var infowindow = new google.maps.InfoWindow({
            map: map,
            position: pos,
            content: 'Installer location'
          });

          map.setCenter(pos);
        }, function() {
          handleNoGeolocation(true);
        });
      } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
      }
    }

    function handleNoGeolocation(errorFlag) {
      if (errorFlag) {
        var content = 'Error: The Geolocation service failed.';
      } else {
        var content = 'Error: Your browser doesn\'t support geolocation.';
      }

      var options = {
        map: map,
        position: new google.maps.LatLng(60, 105),
        content: content
      };

      var infowindow = new google.maps.InfoWindow(options);
      map.setCenter(options.position);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
});

function select2Product34 () {
  $('.product34').select2({
    data : product34,
    formatSelection: function(item){
      var cell = this.element.attr('data-cell');
      $('#survey').calx('getCell', cell.replace('AA','AI')).setValue(item.lcp).calculate();
      $('#survey').calx('getCell', cell.replace('AA','AK')).setValue(item.ratedLifetime).calculate();
      return item.text;
    },
    formatResult: function(item){
      return item.text;
    }
  });
}