$(document).ready(function(){
  
  
  /** find next input when enter  */
  var $input = $('input');
  $input.on('keypress', function(e) {
    if (e.which === 13) {
      var ind = $input.index(this);
      $input.eq(ind + 1).focus();
      e.preventDefault();
    }
  });

  /** initial date-picker */
  $('.datepicker').datepicker({
    //'dateFormat' : 'dd-mm-yy'
    'dateFormat' : 'yy-mm-dd'
  });


  var split_url = window.location.href.split('/'),
  action = window.location.href
  var last_section = split_url[split_url.length - 1];
  $('form.installer-signup').attr('action', 
    last_section === 'installer-signup' ? 
    action + '/2' : action.slice(0,-1) + (parseInt(last_section) + 1));

  var url = window.location.href;
  var segments = url.split('/')
  var clicks = segments[segments.length - 1];
  clicks = clicks/clicks !== 1 ? 1 : clicks;
  var percent = Math.min(Math.round(clicks / 3 * 100), 100);
  $('.progress-bar').width(percent + '%').text('Part ' +clicks+ ' of 3');
  $('.signup').addClass('hidden');
  $('#signup'+clicks).removeClass('hidden');
                          
  $('.back, .next').click(function(){
    var $button = $(this);
    var $prevPage = $button.attr('data-back');
    var $nextPage = $button.attr('data-next');
    $('.signup').addClass('hidden');
    if($button.hasClass('next')){
      window.location = url.slice(0,-1)+$nextPage
    }else{
      window.location = url.slice(0,-1)+$prevPage
    }
  });

  $('#next2').click(function(e){
    window.location = window.location.href+'/2';
  });

  

        $('.prev, .forward').click(function(){
            var $button = $(this);
            var $prevPage = $button.attr('data-prev');
            var $forwardPage = $button.attr('data-forward');
            $('.slide').addClass('hidden');
            if($button.hasClass('forward')){
                $('#slide'+$forwardPage).removeClass('hidden');
            }else{
                $('#slide'+$prevPage).removeClass('hidden');
            }
        });

        $('.prev_doc, .forward_doc').click(function(){
            var $button = $(this);
            var $prevPage = $button.attr('data-prev');
            var $forwardPage = $button.attr('data-forward');
            $('.guide').addClass('hidden');
            if($button.hasClass('forward')){
                $('#guide'+$forwardPage).removeClass('hidden');
            }else{
                $('#guide'+$prevPage).removeClass('hidden');
            }
        });
        $('#forward16').click(function(){
            $('#next4').removeClass('hidden');
        });


        $('.instaler_signup').validationEngine();
        $('#instaler_details').validationEngine();

        var xjpu6gw03c2n93;(function(d, t) {
        var s = d.createElement(t), options = {
        'userName':'myelect',
        'formHash':'xjpu6gw03c2n93',
        'autoResize':true,
        'height':'609',
        'async':true,
        'host':'wufoo.com',
        'header':'show',
        'ssl':true};
        s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + 'www.wufoo.com/scripts/embed/form.js';
        s.onload = s.onreadystatechange = function() {
        var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
        try { xjpu6gw03c2n93 = new WufooForm();xjpu6gw03c2n93.initialize(options);xjpu6gw03c2n93.display(); } catch (e) {}};
        var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
        });

});