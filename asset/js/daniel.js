$(document).ready(function(){
/*
$AE16 = $('#AE16').val();
$AE18 = $('#AE18').val();
alert($AE16);
alert($AE18);
      if($AE16 == 'Residential' && $AE18 == 'Individual-private use'){ 
        $('.private').addClass('hidden');
      }else if($AE16 == 'Commercial'){
        $('.private').removeClass('hidden');
      };
   

/*
*   init calx
*/
    var $calc = $('#assignment').calx();

    $('#sidebar').affix({
      offset: {
        top: 230,
        bottom: 100
      }
    }); 
    $('#midCol').affix({
          offset: {
            top: 230,
            bottom: 100
          }
    }); 
    

/*
*  hidden unhidden input
*/

    $('#C28').change(function(){
        $C28 = $('#C28').val();
        if($(this).val() != '' ){
            $('#AI228').val($C28);

        }else{
            $('#AI228').val('');
        }
    });

    $('#AE16').change(function(){
        $AE16 = $('#AE16').val();
      if($(this).val() == 'Residential'){ 
        $('#C18').removeClass('hidden');
        $('#V228').addClass('hidden');
      }else if(($(this).val() == 'Commercial')){
        $('#C18').addClass('hidden');
        $('#V228').removeClass('hidden');
        //$('.private').removeClass('hidden');
      }else{
        $('#C18').addClass('hidden');
        $('#V228').addClass('hidden');
      }
    });

    $('#AE16').change(function(){
        $AE18 = $('#AE18').val();
      if($(this).val() == 'Residential' && $AE18 == 'Individual-private use'){ 
        $('.private').addClass('hidden');
      }else{
        $('.private').removeClass('hidden');
      }
    });

    

    $('#AE18').change(function(){
        $AE16 = $('#AE16').val();
      if($(this).val() == 'Individual-private use' && $AE16 != 'Commercial'){ 
        $('.private').addClass('hidden');
      }else{
        $('.private').removeClass('hidden');
      }
    });



    $('#C42').change(function(){
      if($(this).val() == 'Yes'){ 
        $('#C43').removeClass('hidden');
      }else{
        $('#C43').addClass('hidden');
      }
    });

    $('#V115').change(function(){
      if($(this).val() == 'Yes'){ 
        $('#AJ115').removeClass('hidden');
      }else{
        $('#AJ115').addClass('hidden');
      }
    });
    $('#V117').change(function(){
      if($(this).val() == 'Yes'){ 
        $('#AM117').removeClass('hidden');
      }else{
        $('#AM117').addClass('hidden');
      }
    });
    $('#C196').change(function(){
      if($(this).val() == 'Lighting solution provider - energy consumer has received VEEC benefit'){ 
        $('#AA196').removeClass('hidden');
      }else{
        $('#AA196').addClass('hidden');
      }
    });
    
/*
*   hidden unhidden lamp product table on page 2
*/  

    
    $('#C74').change(function(){
        if($(this).val() == '12V Halogen lamp only'){
            $('#T74').removeClass('hidden');
            $('#T73').addClass('hidden');
            $('#T73').select2('val', 'All');
        }else if($(this).val() == '12V Halogen lamp & transfomer'){
            $('#T74').addClass('hidden');
            $('#T74').select2('val', 'All');
            $('#T73').removeClass('hidden');
        }else{
            $('#T74').addClass('hidden');
            $('#T73').addClass('hidden');
        }
        $('#assignment').calx('getSheet').calculate();
    });

    $('#C76').change(function(){
        if($(this).val() == '12V Halogen lamp only'){
            $('#T76').removeClass('hidden');
            $('#T75').addClass('hidden');
            $('#T75').select2('val', 'All');
        }else if($(this).val() == '12V Halogen lamp & transfomer'){
            $('#T76').addClass('hidden');
            $('#T75').removeClass('hidden');
            $('#T76').select2('val', 'All');
        }else{
            $('#T76').addClass('hidden');
            $('#T75').addClass('hidden');
        }
        $('#assignment').calx('getSheet').calculate();
    });

    $('#C78').change(function(){
        if($(this).val() == '12V Halogen lamp only'){
            $('#T78').removeClass('hidden');
            $('#T77').select2('val', 'All');
            $('#T77').addClass('hidden');
        }else if($(this).val() == '12V Halogen lamp & transfomer'){
            $('#T78').addClass('hidden');
            $('#T77').removeClass('hidden');
            $('#T78').select2('val', 'All');
        }else{
            $('#T78').addClass('hidden');
            $('#T77').addClass('hidden');
        }
        $('#assignment').calx('getSheet').calculate();
    });

    $('#C80').change(function(){
        if($(this).val() == '12V Halogen lamp only'){
            $('#T80').removeClass('hidden');
            $('#T79').select2('val', 'All');
            $('#T79').addClass('hidden');
        }else if($(this).val() == '12V Halogen lamp & transfomer'){
            $('#T80').addClass('hidden');
            $('#T79').removeClass('hidden');
            $('#T80').select2('val', 'All');
        }else{
            $('#T80').addClass('hidden');
            $('#T79').addClass('hidden');
        }
        $('#assignment').calx('getSheet').calculate();
    });



/*
*   installation date
*/
    $now = $('#C69').val();
    $install = $('#C68').val();
    if ($install == 0) {
        $('#assignment').calx('getCell', 'C68').setFormula('C69').calculate();
    };
    
/** find next input when enter  */
    var $input = $('input');
    $input.on('keypress', function(e) {
        if (e.which === 13) {
            var ind = $input.index(this);
            $input.eq(ind + 1).focus();
            e.preventDefault();
        }
    });

/** 
* select2 init
*/

    $(".industry").select2({
        data:{ results: industry, text: 'id' },
        formatSelection: function(item){
            
           
            $('#AS31').val(item.value).trigger('change');
            return item.id;
        },
        formatResult: function(item){
            return item.id;
        }
    });
    var street_type_s2 = $(".street").select2({
        data:{ results: street, text: 'id' },
        formatSelection: function(item){
            
            $('#assignment').calx('getCell', 'AP36').setValue(item.value).calculate();
            $('#assignment').calx('getCell', 'AO37').setValue(item.id).calculate();
            $('#assignment').calx('getSheet').calculate();


            return item.id;
        },
        formatResult: function(item){
            return item.id;
        }
    });
    $(".c_product").select2({
        data:{ results: c_product, text: 'id' },
        formatSelection: function(item){
            var count = this.element.attr('data-count');
           
            $('#assignment').calx('getCell', 'AM' +count).setValue(item.value).calculate();
            $('#assignment').calx('getCell', 'AN' +count).setValue(item.output).calculate();
            $('#assignment').calx('getCell', 'AP' +count).setValue(item.efficacy).calculate();
            $('#assignment').calx('getCell', 'AR' +count).setValue(item.factors).calculate();
            $('#assignment').calx('getCell', 'AS' +count).setValue(item.lifetime).calculate();
            $('#assignment').calx('getCell', 'S' +count).setValue(item.id).calculate();
            $('#assignment').calx('getSheet').calculate();
            return item.id;
        },
        formatResult: function(item){
            return item.id;
        }
    });
    $(".d_product").select2({
        data:{ results: d_product, text: 'id' },
        formatSelection: function(item){
            var count = this.element.attr('data-count');
           
            $('#assignment').calx('getCell', 'AM' +count).setValue(item.value).calculate();
            $('#assignment').calx('getCell', 'AN' +count).setValue(item.output).calculate();
            $('#assignment').calx('getCell', 'AP' +count).setValue(item.efficacy).calculate();
            $('#assignment').calx('getCell', 'AR' +count).setValue(item.factors).calculate();
            $('#assignment').calx('getCell', 'AS' +count).setValue(item.lifetime).calculate();
            $('#assignment').calx('getCell', 'S' +count).setValue(item.id).calculate();
            $('#assignment').calx('getSheet').calculate();
            return item.id;

        },
        formatResult: function(item){
            return item.id;

        }
    });
    $(".installer").select2({
        data:{ results: installer, text: 'id' },
        formatSelection: function(item){
            //var count = this.element.attr('data-count');
            $('#selected_installer_photo_placeholder').attr('src',base_url+'settings_app_installer_photos/'+item.photo).removeClass('hidden');
            $('input[name="installer"]').val('settings_app_installer_photos/'+item.photo);
            $('#selected_installer_photo2_placeholder').attr('src',base_url+'settings_app_installer_photos/'+item.photo2).removeClass('hidden');
            $('input[name="installerphoto2"]').val('settings_app_installer_photos/'+item.photo2);
            $('#assignment').calx('getCell', 'O127').setValue(item.phone).calculate();
            $('#assignment').calx('getCell', 'C127').setValue(item.id).calculate();
            $('#assignment').calx('getCell', 'Z127').setValue(item.email).calculate();
            $('#assignment').calx('getCell', 'C130').setValue(item.type).calculate();
            $('#assignment').calx('getCell', 'K130').setValue(item.number).calculate();
            $('#assignment').calx('getCell', 'V130').setValue(item.issue).calculate();
            $('#assignment').calx('getCell', 'AI130').setValue(item.expire).calculate();
            $('#assignment').calx('getSheet').calculate();
            $('input[name="iid"]').val(item.iid);
            return item.id;

        },
        formatResult: function(item){
            return item.id;

        }
    });


    $(".product").select2({
        data:{ results: product, text: 'id' },
        formatSelection: function(item){
            var count = this.element.attr('data-count');
           
            $('#assignment').calx('getCell', 'ZJ' +count).setValue(item.rate).calculate();
            $('#assignment').calx('getSheet').calculate();
            $('input[name="item_id_'+count+'"]').val(item.item_id);
            return item.id;

        },
        formatResult: function(item){
            return item.id;

        }
    });

    var postcode_s2 = $(".postcode").select2({
        data:{ results: postcode, text: 'id' },
        formatSelection: function(item){
            //var count = this.element.attr('data-count');
           
            $('#assignment').calx('getCell', 'AW69').setValue(item.value).calculate();
            $('#assignment').calx('getCell', 'Z40').setValue(item.id).calculate();
            $('#assignment').calx('getSheet').calculate();
            return item.id;

        },
        formatResult: function(item){
            return item.id;

        }
    });

/*
* button next and previous
*/

    $(function() {
    var clicks = 1;
    $('.next').on('click', function() {
    clicks++;
      var percent = Math.min(Math.round(clicks / 6 * 100), 100);
      $('.progress-bar').width(percent + '%').text('Part ' +clicks+ ' of 6');
    });

    $('.back').on('click', function(){
        clicks--
        var percent = Math.min(Math.round(clicks / 6 *100), 100);
        $('.progress-bar').width(percent + '%').text('Part ' +clicks+ ' of 6')
    });

    });

    var $jq = window.parent.jQuery;
    var $iframeContainer = $jq('#assesmentContainer');

    $iframeContainer.css('height', $('body').height()+20);

    $('.back, .next').click(function(){
        var $button = $(this);
        var $prevPage = $button.attr('data-back');
        var $nextPage = $button.attr('data-next');

        $('.page').addClass('hidden');

        if($button.hasClass('next')){
            $('#page'+$nextPage).removeClass('hidden');
        }else{
            $('#page'+$prevPage).removeClass('hidden');
        }

        $iframeContainer.css('height', $('body').height()+20);

    });

    /** initial date-picker */
    $('.datepicker').datepicker({
        'dateFormat' : 'dd-mm-yy'
    });

/*
* button submit pdf
*/
    $('#pdf').click(function(e){
        e.preventDefault();
        $('#action').val('pdf');
        $('#assignment').attr('action',site_url+'/mainController/pdf').submit();
    });

/*
* geo location
*/
    
    var map;

    function initialize() {
      var mapOptions = {
        zoom: 17
      };
      map = new google.maps.Map(document.getElementById('map-canvas'),
          mapOptions);

      // Try HTML5 geolocation
      if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          if('' !== mapsrc){
            mapsrc = mapsrc.split("&markers=");
            mapsrc = mapsrc[0].replace('https://maps.googleapis.com/maps/api/staticmap?center=','');
            mapsrc = mapsrc.split(',');
            var pos = new google.maps.LatLng(mapsrc[0],mapsrc[1]);
          }else{
            jQuery('[name="mapsrc"]').val(
                "https://maps.googleapis.com/maps/api/staticmap?center="+
                position.coords.latitude+","+position.coords.longitude+
                "&markers="+
                position.coords.latitude+","+position.coords.longitude+
                "&zoom=15&size=700x475");
            var pos = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
          }

          var infowindow = new google.maps.InfoWindow({
            map: map,
            position: pos,
            content: 'Installer location'
          });

          map.setCenter(pos);

          var geocoder = new google.maps.Geocoder();
          // pos = new google.maps.LatLng(-37.806525, 144.943315);// COMMENT THIS OUT
          geocoder.geocode({'latLng':pos},function(data,status){
            if(status == google.maps.GeocoderStatus.OK){
              var addr_comp = data[0]['address_components']
              var street_number = addr_comp[0].long_name,
              street = addr_comp[1].long_name
              var streetsplit = street.split(' ')
              var street_type = streetsplit[streetsplit.length-1]
              var street_name = street.replace(street_type, ''),
              town = addr_comp[2].long_name,
              state = addr_comp[3].long_name,
              post_code = addr_comp[5].long_name

              $('#assignment').calx('getCell', 'P36').setValue(street_number).calculate()
              $('#assignment').calx('getCell', 'W36').setValue(street_name).calculate()
              $('#assignment').calx('getCell', 'AO36').setValue(street_type).calculate()
              street_type_s2.trigger('change')
              $('#assignment').calx('getCell', 'C39').setValue(town).calculate()
              $('#assignment').calx('getCell', 'U39').setValue(state).calculate()
              $('#assignment').calx('getCell', 'Z39').setValue(post_code).calculate()
              postcode_s2.trigger('change')
              $('#assignment').calx('getSheet').calculate();
            }
          })

        }, function() {
          handleNoGeolocation(true);
        });
      } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
      }
    }

    function handleNoGeolocation(errorFlag) {
      if (errorFlag) {
        var content = 'Error: The Geolocation service failed.';
      } else {
        var content = 'Error: Your browser doesn\'t support geolocation.';
      }

      var options = {
        map: map,
        position: new google.maps.LatLng(60, 105),
        content: content
      };

      var infowindow = new google.maps.InfoWindow(options);
      map.setCenter(options.position);
    }

    google.maps.event.addDomListener(window, 'load', initialize);

});
