-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Feb 01, 2016 at 07:47 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `getveet21c_v3`
--

-- --------------------------------------------------------

--
-- Table structure for table `cd_product`
--

CREATE TABLE `cd_product` (
  `item_id` int(11) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `id` varchar(255) NOT NULL,
  `type` char(3) NOT NULL DEFAULT '21C',
  `value` int(11) NOT NULL DEFAULT '1',
  `lifetime` varchar(255) NOT NULL DEFAULT '20000+',
  `output` int(11) NOT NULL DEFAULT '350',
  `efficacy` int(11) NOT NULL DEFAULT '62',
  `factors` float NOT NULL DEFAULT '0.8715',
  `abatement` float NOT NULL,
  `power` float NOT NULL,
  `veec_vic` float NOT NULL,
  `veec_metro` float NOT NULL,
  `electronic` varchar(255) NOT NULL,
  `magnetic` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1454301325 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cd_product`
--

INSERT INTO `cd_product` (`item_id`, `brand`, `id`, `type`, `value`, `lifetime`, `output`, `efficacy`, `factors`, `abatement`, `power`, `veec_vic`, `veec_metro`, `electronic`, `magnetic`) VALUES
(1454301324, 'PRIMSAL', 'PMR166WWHPFB', '21C', 0, '2000', 350, 62, 0.8715, 0.83, 0.97, 0.90636, 0.85407, 'YES', 'YES');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cd_product`
--
ALTER TABLE `cd_product`
  ADD PRIMARY KEY (`item_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cd_product`
--
ALTER TABLE `cd_product`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1454301325;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
