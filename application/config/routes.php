<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['form/(:any)'] = 'LandingPageController/landing/$1';
$route['default_controller'] = "dashboardcontroller";
$route['404_override'] = '';
$route['signup'] = "authcontroller/register";
$route['signup/(:num)'] = "authcontroller/register/$1";
$route['signup/(:num)/(:num)'] = "authcontroller/register/$1/$2";
$route['app-admin-signup'] = "authcontroller/app_admin_signup";
$route['company_agreement'] = "authcontroller/company_agreement";
$route['installer_agreement'] = "authcontroller/installer_agreement";
$route['lead-manager'] = 'LeadController/index';
$route['(:any)/installer-signup'] = 'authcontroller/installer_signup/$1';
$route['(:any)/installer-signup/(:num)'] = 'authcontroller/installer_signup/$1/$2';

$existsRoute = $route;
foreach ($existsRoute as $key => $value) {
	$value = strpos($value,'$2') !== false ? str_replace('$2', '$3', $value) : $value;
	$value = strpos($value,'$1') !== false ? str_replace('$1', '$2', $value) : $value;
	$route['(:any)/' . $key] = $value;
	if ($key == '(:any)/installer-signup' || $key == '(:any)/installer-signup/(:num)') unset($route[$key]);
}

$conts = array();
$currCont = '';
foreach (scandir('./application/controllers/') as $controller) {
	switch ($controller) {
		case '.':
		case '..':
		case 'index.html':continue;break;
		default:
			$controller = "./application/controllers/$controller";
			$myfile = fopen($controller, "r") or die("Unable to open file!");
			while(!feof($myfile)) {
				$line = strtolower(fgets($myfile));
				if (strpos($line,'class') !== false && strpos($line,'extends') !== false) {
					$split = explode(' extends', $line);
					$controllerName = str_replace('class ', '', $split[0]);
					$currCont = $controllerName;
					$conts[$currCont] = array();
					$route["(:any)/$controllerName"] = $controllerName;
				}else if (strpos($line,'function') !== false && strpos($line,'__construct') == false) {
					$split = explode('function ', $line);
					$method = explode('(', $split[1]);
					$functionParam =  $method[1];
					$method = $method[0];
					$method = trim($method);
					$conts[$currCont][] = $method;
					$route["(:any)/$currCont/$method"] = "$currCont/$method";
					if (strpos($functionParam,'$') !== false) {
						$route["(:any)/$currCont/$method/(:any)"] = $currCont . '/' . $method . '/$2';
						$route["(:any)/$currCont/$method/(:any)/(:any)"] = $currCont . '/' . $method . '/$2/$3';
					}
				}
			}
			fclose($myfile);
			break;
	}
}
// echo'<pre>';print_r($route);die();
/* End of file routes.php */
/* Location: ./application/config/routes.php */