<form enctype="multipart/form-data" action="" class="form-horizontal" method="post" role="form" id="survey">
                                        <input type="hidden" id="action1" name="action1">
                                        <div class="page">
                                            <div class="row">
                                                <div class="col-sm-4 hidden-xs">
                                                    <img src="<?= $logo ?>" style="height:80px" alt="">
                                                </div>
                                                <div class="col-sm-4">
                                                    <h2 class="text-center bold">CL VEEC</h2>
                                                    <h4 class="text-center bold" style="margin-bottom:10px">Lighting Survey Form</h4>
                                                </div>
                                                <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                                <div class="col-sm-3 hidden-xs">
                                                    <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 box_label">
                                                    <h5 style="padding-top:5px">Energy Consumer's details</h5>
                                                </div>
                                            </div>
                                            <div class="row box_desc">
                                                <div class="col-sm-12">
                                                    <div class="form-group small_space">
                                                        <div class="col-sm-4">
                                                            <label for="C6">Company name</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C6" name="C6" data-cell="C6"
                                                            value="<?= isset($data['survey']['C6']) ? $data['survey']['C6'] : '' ?>">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label for="U6">Contact person</label>
                                                            <input type="text" class="form-control input-sm yellow" id="U6" name="U6" data-cell="U6"
                                                            value="<?= isset($data['survey']['U6']) ? $data['survey']['U6'] : '' ?>">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label for="AA6">Contact phone</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AA6" name="AA6" data-cell="AA6"
                                                            value="<?= isset($data['survey']['AA6']) ? $data['survey']['AA6'] : '' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="AI6">Contact email</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AI6" name="AI6" data-cell="AI6"
                                                            value="<?= isset($data['survey']['AI6']) ? $data['survey']['AI6'] : '' ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group small_space">
                                                        <div class="col-sm-3">
                                                            <label for="C9">Installation street address</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C9" name="C9" data-cell="C9"
                                                            value="<?= isset($data['survey']['C9']) ? $data['survey']['C9'] : '' ?>">
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label for="M9">Installation subrb</label>
                                                            <input type="text" class="form-control input-sm yellow" id="M9" name="M9" data-cell="M9"
                                                            value="<?= isset($data['survey']['M9']) ? $data['survey']['M9'] : '' ?>">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label for="W9">State</label>
                                                            <input type="text" value="VIC" class="form-control input-sm text-center" id="W9" name="W9" data-cell="W9"
                                                            value="<?= isset($data['survey']['W9']) ? $data['survey']['W9'] : '' ?>">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label for="Y9">Postcode</label>
                                                            <input type="text" class="form-control input-sm yellow postcode text-center" id="Y9" name="Y9" data-cell="Y9"
                                                            value="<?= isset($data['survey']['Y9']) ? $data['survey']['Y9'] : '' ?>">
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label for="" class="col-sm-10 col-xs-9">Is the lighting upgrade at this site undertaken as part of building work which required a building permit according to the Building Act 1993 and the Building Regulations 2006 (i.e. a J6 upgrade)? </label>
                                                            <div class="col-sm-2 col-xs-3">
                                                                <select type="text" class="form-control input-sm" id="AW9" name="AW9" data-cell="AW9">
                                                                    <option value="0" <?= isset($data['survey']['AW9']) && $data['survey']['AW9'] == 0?'selected':'' ?>>NO</option>
                                                                    <option value="1" <?= isset($data['survey']['AW9']) && $data['survey']['AW9'] == 1?'selected':'' ?>>YES</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="table-responsive" style="">
                                                        <table id="survey_tbl" cellspasing="0" class="table table-bordered table-condensed table-hover" border="0" cellpadding="0" >
                                                            <thead>
                                                                <tr>
                                                                    <th colspan="2">&nbsp;</th>
                                                                    <th class="skyblue"colspan="6">Existing lighting (baseline)</th>
                                                                    <th class="warm" colspan="6">Upgraded lighting</th>
                                                                    <th colspan="2">&nbsp;</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="small center" style="width:50px !important"></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Room/area</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Lamp type & ballast/control gear</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Nom. watts</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Control ystem</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Air-Con?</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">No of lamps</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Activy type</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Brand & model number</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">LCP</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Rate Lifetime(hrs)</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Control ystem</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Air-Con?</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">No of lamps</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Annual operating hours</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">No. VEECs</label></th>

                                                                </tr>
                                                            </thead>
                                                            <tbody id="survey_body" >
                                                              <?php $svbd=15; while($svbd==15 || isset($data['survey']["A$svbd"])): ?>
                                                                <tr id="row<?= $svbd-14 ?>">
                                                                    <td class="text-center">
                                                                        <label for="" id="B<?= $svbd ?>" name="B<?= $svbd ?>" data-cell="B<?= $svbd ?>" data-formula="A<?= $svbd ?>"></label>
                                                                        <input type="hidden" id="A<?= $svbd ?>" name="A<?= $svbd ?>" data-cell="A<?= $svbd ?>" value="<?= $svbd-14 ?>"
                                                                        value="<?= isset($data['survey']["A$svbd"])?$data['survey']["A$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-150" id="C<?= $svbd ?>" name="C<?= $svbd ?>" data-cell="C<?= $svbd ?>"
                                                                      value="<?= isset($data['survey']["C$svbd"])?$data['survey']["C$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                        <input type="text" class="input-sm form-control text-center yellow existing_lamp width-250" id="H<?= $svbd ?>" name="H<?= $svbd ?>" data-cell="H<?= $svbd ?>" data-count="<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["H$svbd"])?$data['survey']["H$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="HA<?= $svbd ?>" name="HA<?= $svbd ?>" data-cell="HA<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["HA$svbd"])?$data['survey']["HA$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="HB<?= $svbd ?>" name="HB<?= $svbd ?>" data-cell="HB<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["HB$svbd"])?$data['survey']["HB$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="HC<?= $svbd ?>" name="HC<?= $svbd ?>" data-cell="HC<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["HC$svbd"])?$data['survey']["HC$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="HD<?= $svbd ?>" name="HD<?= $svbd ?>" data-cell="HD<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["HD$svbd"])?$data['survey']["HD$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="hidden">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="N<?= $svbd ?>" name="N<?= $svbd ?>" data-cell="N<?= $svbd ?>" data-formula="IF(HD15 = 'yes',IF(O15>37,37,O15),O15)"
                                                                        value="<?= isset($data['survey']["N$svbd"])?$data['survey']["N$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="P<?= $svbd ?>" name="P<?= $svbd ?>" data-cell="P<?= $svbd ?>" data-formula="(N15+0)*HA15+HB<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["P$svbd"])?$data['survey']["P$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="T<?= $svbd ?>" name="T<?= $svbd ?>" data-cell="T<?= $svbd ?>" data-formula="Q<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["T$svbd"])?$data['survey']["T$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="V<?= $svbd ?>" name="V<?= $svbd ?>" data-cell="V<?= $svbd ?>" data-formula="U<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["U$svbd"])?$data['survey']["U$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="Z<?= $svbd ?>" name="Z<?= $svbd ?>" data-cell="Z<?= $svbd ?>" data-formula="IF(W15=0,0,IF(AL15=0,(P15*30000*T15*V15*W15)/1000000,(P15*AK15*T15*V15*W15)/1000000))"
                                                                        value="<?= isset($data['survey']["Z$svbd"])?$data['survey']["Z$svbd"]:'' ?>">

                                                                        <?php if($svbd==15): ?>
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="Y10" name="Y10" data-cell="Y10"
                                                                        value="<?= isset($data['survey']['Y10'])?$data['survey']['Y10']:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="BS106" name="BS106" data-cell="BS106" data-formula="IF(Y10='Metropolitan',0.98,IF(Y10='Regional',1.04,0))"
                                                                        value="<?= isset($data['survey']['BS106'])?$data['survey']['BS106']:'' ?>">
                                                                        <?php endif; ?>

                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-50" id="O<?= $svbd ?>" name="O<?= $svbd ?>" data-cell="O<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["O$svbd"])?$data['survey']["O$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow width-250" name="Q<?= $svbd ?>" id="Q<?= $svbd ?>" data-cell="Q<?= $svbd ?>">
                                                                            <option value="0" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0?'selected':'' ?>></option>
                                                                            <option value="1" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==1?'selected':'' ?>>No control system</option>
                                                                            <option value="0.7" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.7?'selected':'' ?>>Occupancy sensor</option>
                                                                            <option value="0.7" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.7?'selected':'' ?>>Day-light linked control</option>
                                                                            <option value="0.85" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.85?'selected':'' ?>>Programmable dimming</option>
                                                                            <option value="0.9" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.9?'selected':'' ?>>Manual dimming</option>
                                                                            <option value="0.744" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.744?'selected':'' ?>>VRU</option>
                                                                            <option value="0.6" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.6?'selected':'' ?>>Occupancy & Daylight Control</option>
                                                                            <option value="0.6" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.6?'selected':'' ?>>Occupancy & Program. Dimmer</option>
                                                                            <option value="0.63" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.63?'selected':'' ?>>Occupancy & Manual Dimmer</option>
                                                                            <option value="0.6" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.6?'selected':'' ?>>Occupancy & VRU</option>
                                                                            <option value="0.6" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.6?'selected':'' ?>>Daylight Control & Program. Dimmer</option>
                                                                            <option value="0.63" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.63?'selected':'' ?>>Daylight Control & Manual Dimmer</option>
                                                                            <option value="0.6" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.6?'selected':'' ?>>Daylight Control & VRU</option>
                                                                            <option value="0.765" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.765?'selected':'' ?>>Program. Dimmer & Manual Dimmer</option>
                                                                            <option value="0.6324" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.6324?'selected':'' ?>>Program. Dimmer & VRU</option>
                                                                            <option value="0.6696" <?= isset($data['survey']["Q$svbd"])&&$data['survey']["Q$svbd"]==0.6696?'selected':'' ?>>Manual Dimmer & VRU</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow" name="U<?= $svbd ?>" id="U<?= $svbd ?>" data-cell="U<?= $svbd ?>">
                                                                            <option value="0" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0?'selected':'' ?>></option>
                                                                            <option value="1" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==1?'selected':'' ?>>No</option>
                                                                            <option value="1.05" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==1.05?'selected':'' ?>>Yes</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-50" id="W<?= $svbd ?>" name="W<?= $svbd ?>" data-cell="W<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["W$svbd"])?$data['survey']["W$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow width-250" name="Y<?= $svbd ?>" id="Y<?= $svbd ?>" data-cell="Y<?= $svbd ?>">
                                                                            <option value="0" <?= isset($data['survey']["Y$svbd"])&&$data['survey']["Y$svbd"]==0?'selected':'' ?>></option>
                                                                            <option value="1" <?= isset($data['survey']["Y$svbd"])&&$data['survey']["Y$svbd"]==1?'selected':'' ?>>Delamping from multiple fittings</option>
                                                                            <option value="2" <?= isset($data['survey']["Y$svbd"])&&$data['survey']["Y$svbd"]==2?'selected':'' ?>>Replace lamp only</option>
                                                                            <option value="replace_lamp" <?= isset($data['survey']["Y$svbd"])&&$data['survey']["Y$svbd"]=='replace_lamp'?'selected':'' ?>>Replace lamp and control gear </option>
                                                                            <option value="replace_full" <?= isset($data['survey']["Y$svbd"])&&$data['survey']["Y$svbd"]=='replace_full'?'selected':'' ?>>Replace full luminaire</option>
                                                                            <option value="1" <?= isset($data['survey']["Y$svbd"])&&$data['survey']["Y$svbd"]==1?'selected':'' ?>>Install VRU or sensor only (on existing lighting)</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="hidden">
                                                                        <input type="text" class="input-sm form-control text-center yellow" id="AL<?= $svbd ?>" name="AL<?= $svbd ?>" data-cell="AL<?= $svbd ?>" data-formula="IF(Y15='replace_lamp',0,IF(Y15='replace_full',0,Y15))"
                                                                        value="<?= isset($data['survey']["AL$svbd"])?$data['survey']["AL$svbd"]:'' ?>">
                                                                        <input type="text" class="input-sm form-control text-center yellow" id="AP<?= $svbd ?>" name="AP<?= $svbd ?>" data-cell="AP<?= $svbd ?>" data-formula="AM<?= $svbd ?>" data-format="0.0000"
                                                                        value="<?= isset($data['survey']["AP$svbd"])?$data['survey']["AP$svbd"]:'' ?>">
                                                                        <input type="text" class="input-sm form-control text-center yellow" id="AR<?= $svbd ?>" name="AR<?= $svbd ?>" data-cell="AR<?= $svbd ?>" data-formula="AQ<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["AR$svbd"])?$data['survey']["AR$svbd"]:'' ?>">
                                                                        <input type="text" class="input-sm form-control text-center yellow" id="AT<?= $svbd ?>" name="AT<?= $svbd ?>" data-cell="AT<?= $svbd ?>" data-formula="IF(AS15=0,0,IF(AL15=1,((P15*AK15*AP15*AR15*AS15)/1000000),IF(AL15=2,((AI15*AK15*AP15*AR15*AS15)/1000000),((AI15*30000*AP15*AR15*AS15)/1000000))))"
                                                                        value="<?= isset($data['survey']["AT$svbd"])?$data['survey']["AT$svbd"]:'' ?>">
                                                                        <input type="text" class="input-sm form-control text-center yellow" id="AV<?= $svbd ?>" name="AV<?= $svbd ?>" data-cell="AV<?= $svbd ?>" data-formula="IF(AS15=0,' ',(Z15-AT15))"
                                                                        value="<?= isset($data['survey']["AV$svbd"])?$data['survey']["AV$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-150 product34" id="AA<?= $svbd ?>" name="AA<?= $svbd ?>" data-cell="AA<?= $svbd ?>" 
                                                                      value="<?= isset($data['survey']["AA$svbd"])?$data['survey']["AA$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-50" id="AI<?= $svbd ?>" name="AI<?= $svbd ?>" data-cell="AI<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["AI$svbd"])?$data['survey']["AI$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow" id="AK<?= $svbd ?>" name="AK<?= $svbd ?>" data-cell="AK<?= $svbd ?>" data-format="0,0[.]"
                                                                        value="<?= isset($data['survey']["AK$svbd"])?$data['survey']["AK$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow width-250" name="AM<?= $svbd ?>" id="AM<?= $svbd ?>" data-cell="AM<?= $svbd ?>">
                                                                            <option value="0" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0?'selected':'' ?>></option>
                                                                            <option value="1" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==1?'selected':'' ?>>No control system</option>
                                                                            <option value="0.7" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.7?'selected':'' ?>>Occupancy sensor</option>
                                                                            <option value="0.7" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.7?'selected':'' ?>>Day-light linked control</option>
                                                                            <option value="0.85" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.85?'selected':'' ?>>Programmable dimming</option>
                                                                            <option value="0.9" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.9?'selected':'' ?>>Manual dimming</option>
                                                                            <option value="0.744" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.744?'selected':'' ?>>VRU</option>
                                                                            <option value="0.6" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.6?'selected':'' ?>>Occupancy & Daylight Control</option>
                                                                            <option value="0.6" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.6?'selected':'' ?>>Occupancy & Program. Dimmer</option>
                                                                            <option value="0.63" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.63?'selected':'' ?>>Occupancy & Manual Dimmer</option>
                                                                            <option value="0.6" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.6?'selected':'' ?>>Occupancy & VRU</option>
                                                                            <option value="0.6" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.6?'selected':'' ?>>Daylight Control & Program. Dimmer</option>
                                                                            <option value="0.63" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.63?'selected':'' ?>>Daylight Control & Manual Dimmer</option>
                                                                            <option value="0.6" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.6?'selected':'' ?>>Daylight Control & VRU</option>
                                                                            <option value="0.765" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.765?'selected':'' ?>>Program. Dimmer & Manual Dimmer</option>
                                                                            <option value="0.6324" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.6324?'selected':'' ?>>Program. Dimmer & VRU</option>
                                                                            <option value="0.6696" <?= isset($data['survey']["AM$svbd"])&&$data['survey']["AM$svbd"]==0.6696?'selected':'' ?>>Manual Dimmer & VRU</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow" name="AQ<?= $svbd ?>" id="AQ<?= $svbd ?>" data-cell="AQ<?= $svbd ?>">
                                                                            <option value="0" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0?'selected':'' ?>></option>
                                                                            <option value="1" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==1?'selected':'' ?>>No</option>
                                                                            <option value="1.05" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==1.05?'selected':'' ?>>Yes</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-50" id="AS<?= $svbd ?>" name="AS<?= $svbd ?>" data-cell="AS<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["AS$svbd"])?$data['survey']["AS$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow" id="AU<?= $svbd ?>" name="AU<?= $svbd ?>" data-cell="AU<?= $svbd ?>" data-formula="IF(AS15=0,0,3000)"
                                                                        value="<?= isset($data['survey']["AU$svbd"])?$data['survey']["AU$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow" id="AW<?= $svbd ?>" name="AW<?= $svbd ?>" data-cell="AW<?= $svbd ?>" data-formula="IFERROR(IF(AV15>0,IF(AW9=0,(AV15*BS106)*0.963,' '),' '),' ')" data-format="0.0"
                                                                        value="<?= isset($data['survey']["AW$svbd"])?$data['survey']["AW$svbd"]:'' ?>">
                                                                    </td>
                                                                </tr>
                                                                <?php $svbd++; endwhile; ?>
                                                                
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <td colspan="6" class="text-right"><label for="" class="text-right">Total existing lamp</label></td>
                                                                    <td>
                                                                        <input type="text" class="input-sm form-control text-center" id="W50" name="W50" data-cell="W50" data-formula="SUM(W15:W25)"
                                                                        value="<?= isset($data['survey']['W50'])?$data['survey']['W50']:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center" id="Z50" name="Z50" data-cell="Z50" data-formula="SUM(Z15:Z25)" data-format="0.00"
                                                                        value="<?= isset($data['survey']['Z50'])?$data['survey']['Z50']:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center" id="AT50" name="AT50" data-cell="AT50" data-formula="SUM(AT15:AT25)" data-format="0.00"
                                                                        value="<?= isset($data['survey']['AT50'])?$data['survey']['AT50']:'' ?>">
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td colspan="5" class="text-right"><label for="" class="text-right">Total upgrade lamp</label></td>
                                                                    <td>
                                                                      <input type="text" class="input-sm form-control text-center" id="AS50" name="AS50" data-cell="AS50" data-formula="SUM(AS15:AS25)"
                                                                        value="<?= isset($data['survey']['AS50'])?$data['survey']['AS50']:'' ?>">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="15" class="text-right"><label for="">Total estimate VEECs</label></td>
                                                                    <td >
                                                                      <input type="text" class="input-sm form-control text-center" id="AV51" name="AV51" data-cell="AV51" data-formula="SUM(AW15:AW25)" data-format="0.0"
                                                                        value="<?= isset($data['survey']['AV51'])?$data['survey']['AV51']:'' ?>">
                                                                    </td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 text-right">
                                                            <button type="button" class="btn btn-sm btn-primary" id="btn-add-survey">add row</button>
                                                        </div>

                                                <div class="col-sm-12 text-center" style="margin-top:20px">
                                                    <a href="<?= site_url('maincontroller/subform') ?>" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
                                                    <a href="#quote-page" data-toggle="tab" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>