<!DOCTYPE html>
<html lang="en"  class="body-error"><head>
        <meta charset="utf-8">
        <title>Get Veet21C - Sign In</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="<?= base_url('asset/css/login.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->


    </head>

    <body>

        <div id="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="<?= site_url('authcontroller/login') ?>" class="form-login" method="post" style="margin-top:30px"> 

                <div class="header">
                    <a href="" class="link-head-left"></a>
                    <span>Sign in</span>
                    <a href="javascript:void(0);" tabindex="1" class="link-head-right main-item"></a>
<!--                    <div class="config-box"> 
                        <div class="colors">
                            <div class="clear"></div> 
                        </div>
                        <div class="style-bg">
                            <a href="/demo/loginform/black/index.html">Black</a>
                            <a href="/demo/loginform/white/index.html" class="active">White</a> 
                        </div> 
                    </div>-->
                </div>

                <div class="text-center"><img src="<?= base_url('asset/images/avatar.png') ?>" alt="" style="width:250px;margin-top:5px;text-align:center;margin-left:45px;margin-bottom:0px;"></div>

                <div class="inputs" style="padding-bottom:20px;">
                    <input name="email" type="text" required placeholder="Email" />
                    <input name="password" type="password" required placeholder="Password" />


                    <div class="link-1">
                        <input type="checkbox" id="c2" name="cc" checked="checked" />
                        <label for="c2"><span></span> Remember me</label>
                    </div>
                    <div class="link-2"><a href="<?= site_url('authcontroller/forgot_password') ?>">Forgot Password?</a></div>
                    <div class="clear"></div>

                    <div class="button-login"><input type="submit" value="Sign in"></div>
                </div>


            </form>
                    </div>
                </div>
            </div>


            <div class="clear"></div>  
            <div class="link-to-page" style="padding:0px"> 
                &nbsp;
            </div>
        </div>
    </body>
</html>

