<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>assignment form</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <link rel="stylesheet" type="text/css" href="<?= base_url('asset/css/jquery.signaturepad.css') ?>" />
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">

    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center" id="content">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-sm-6 col-sm-offset-3">
                                <ul class="nav nav-pills text-center">
                                        <li style="width:200px">
                                            <a href="<?= $link21 ?>" class="btn 34form-menu">
                                                <img 
                                                src="<?= isset($icon21) ? $icon21 : base_url('asset/images/new_form.png') ?>"
                                                class="dashboard-icon" alt=""> <br>
                                                <span class="bold"><?= isset($text21)?$text21:'21 Activity' ?></span>
                                            </a>
                                        </li>

                                        <li style="width:200px">
                                            <a href="<?= $link34 ?>" class="btn 34form-menu">
                                                <img src="<?= base_url('asset/images/new_form.png') ?>" class="dashboard-icon" alt=""> <br>
                                                <span class="bold"><?= isset($text34)?$text34:'34 Activity' ?></span>
                                            </a>
                                        </li>
                                    </ul>
                            </div>
                        </div><!-- close panel-body -->
                    </div><!-- close panel -->
                </div>
            </div><!-- close content -->   
        </div><!-- close container -->

        <!-- script references -->
          <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js') ?>"></script>-->
        <script src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/daniel.js') ?>" type="text/javascript"></script>
        
    </body>
</html>