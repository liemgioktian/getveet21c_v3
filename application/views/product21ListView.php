<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Product 21 List</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="box_label text-center">
                            <input type="hidden" name="action" id="action">
                            <input name="action" id="action" type="hidden">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>PRODUCT 21 LIST</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <!--page user-->
                            <div class="page" id="">
                                <div class="row">
                                    <div class="col-sm-4 hidden-xs">
                                        <img src="<?= $logo ?>" style="height:80px" alt="">
                                    </div>
                                    <div class="col-sm-4">
                                        <h2 class="text-center bold">VEEC 21</h2>
                                        <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                    </div>
                                    <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                    <div class="col-sm-3 hidden-xs">
                                        <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 box_desc">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="table-responsive" style="overflow-x:auto;max-width:100%">
                                                    <table id="tcodes_tbl" cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
                                                        <thead>
                                                            <tr class="skyblue">
                                                                <td class="center" rowspan="2">BRAND</td>
                                                                <td class="center" rowspan="2">MODEL</td>
                                                                <td class="center">Output</td>
                                                                <td class="center">Efficacy</td>
                                                                <td class="center" rowspan="2">Rated Lifetime</td>
                                                                <td class="center" rowspan="2">Power Factor</td>
                                                                <td class="center" rowspan="2">Abatement Factor</td>
                                                                <td class="center" rowspan="2">Combined Factor</td>
                                                                <td class="center" colspan="2">Number of VEECs per lamp</td>
                                                                <td class="center" rowspan="2">Activity</td>
                                                                <td class="center" colspan="2">Tranformer compatibility</td>
                                                                <td class="center" rowspan="2">Action</td>
                                                            </tr>
                                                            <tr class="skyblue">
                                                                <td class="center">(Lumens)</td>
                                                                <td class="center">(Lumens/Watt)</td>
                                                                
                                                                <td class="center">When installed in regional Victoria</td>
                                                                <td class="center">Metro Melbourne</td>
                                                                
                                                                <td class="center">Electronic</td>
                                                                <td class="center">Magnetic</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($items as $item): ?>
                                                                <tr>
                                                                    <td class="text-center"><?= $item->brand ?></td>
                                                                    <td class="text-center"><?= $item->id ?></td>
                                                                    <td class="text-center"><?= $item->output ?></td>
                                                                    <td class="text-center"><?= $item->efficacy ?></td>
                                                                    <td class="text-center"><?= $item->lifetime ?></td>
                                                                    <td class="text-center"><?= $item->power ?></td>
                                                                    <td class="text-center"><?= $item->abatement ?></td>
                                                                    <td class="text-center"><?= $item->factors ?></td>
                                                                    <td class="text-center"><?= $item->veec_vic ?></td>
                                                                    <td class="text-center"><?= $item->veec_metro ?></td>
                                                                    <td class="text-center"><?= $item->type ?></td>
                                                                    <td class="text-center"><?= $item->electronic ?></td>
                                                                    <td class="text-center"><?= $item->magnetic ?></td>
                                                                    <td class="text-center">
                                                                      <a href="<?= site_url("product21controller/edit/$item->item_id") ?>" class="btn btn-xs btn-warning"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                                                      <a href="<?= site_url("product21controller/delete/$item->item_id/confirm") ?>" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                        <tfoot>
                                                          <tr>
                                                            <th colspan="13" class="text-right">
                                                              <a href="<?= site_url('product21controller/create') ?>" class="btn btn-sm btn-primary">+ ADD NEW PRODUCT 21</a>
                                                            </th>
                                                          </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--close user-->
                        </div>
                    </div>
                        
                        
                </div>
            </div>      
        </div>


        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
    </body>
</html>