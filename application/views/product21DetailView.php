<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Product 21 Form</title>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
    <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
    <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
  </head>
  <body>
    
    <div class="container">

      <div class="row">
        <div class="col-sm-12">
          <?php include APPPATH.'/views/menuView.php'; ?>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12" id="content">
          <div class="panel">
            <div class="panel-body">
              
              <form enctype="multipart/form-data" action="" class="form-horizontal" method="post" role="form" id="product21">
                <div class="page" id="page1">
                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">Brand</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="brand" value="<?= isset($brand)?$brand:'' ?>" />
                      </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">Model</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="id" value="<?= isset($id)?$id:'' ?>" />
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">Lumens Output</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="output" value="<?= isset($output)?$output:'' ?>" />
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">EFFICACY (lm/w)</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="efficacy" value="<?= isset($efficacy)?$efficacy:'' ?>" />
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">LIFETIME</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="lifetime" value="<?= isset($lifetime)?$lifetime:'' ?>" />
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">Power Factor</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="power" value="<?= isset($power)?$power:'' ?>" />
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">Abatement Factor</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="abatement" value="<?= isset($abatement)?$abatement:'' ?>" />
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">Combined Factor</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="factors" value="<?= isset($factors)?$factors:'' ?>" />
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">VEECs in Victoria</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="veec_vic" value="<?= isset($veec_vic)?$veec_vic:'' ?>" />
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">VEECs in Metro Melbourne</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="veec_metro" value="<?= isset($veec_metro)?$veec_metro:'' ?>" />
                      </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">Activity</label>
                      <div class="col-sm-3 col-xs-7">
                        <select name="type">
                          <option value="21C" <?= isset($type) && $type == '21C'?'selected':'' ?>>21C</option>
                          <option value="21D" <?= isset($type) && $type == '21D'?'selected':'' ?>>21D</option>
                        </select>
                        <!--<input type="text" class="form-control input-sm" name="type" value="<?= isset($type)?$type:'' ?>" />-->
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">Electonic Tranformer Compatibility</label>
                      <div class="col-sm-3 col-xs-7">
                        <select name="electronic">
                          <option value="YES" <?= isset($electronic) && $electronic == 'YES'?'selected':'' ?>>YES</option>
                          <option value="NO" <?= isset($electronic) && $electronic == 'NO'?'selected':'' ?>>NO</option>
                          <option value="N/A" <?= isset($electronic) && $electronic == 'N/A'?'selected':'' ?>>N/A</option>
                        </select>
                        <!--<input type="text" class="form-control input-sm" name="type" value="<?= isset($type)?$type:'' ?>" />-->
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">Magnetic Tranformer Compatibility</label>
                      <div class="col-sm-3 col-xs-7">
                        <select name="magnetic">
                          <option value="YES" <?= isset($magnetic) && $magnetic == 'YES'?'selected':'' ?>>YES</option>
                          <option value="NO" <?= isset($magnetic) && $magnetic == 'NO'?'selected':'' ?>>NO</option>
                          <option value="N/A" <?= isset($magnetic) && $magnetic == 'N/A'?'selected':'' ?>>N/A</option>
                        </select>
                        <!--<input type="text" class="form-control input-sm" name="type" value="<?= isset($type)?$type:'' ?>" />-->
                      </div>
                    </div>
                  </div>
                  
                  
                  <div class="row hidden">
                    <div class="form-group">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5">VALUE</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="value" value="<?= isset($value)?$value:'' ?>" />
                      </div>
                    </div>
                  </div>
                
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  <div class="row">
                    <div class="form-group text-right">
                      <label for="" class="col-sm-3 col-sm-offset-3 col-xs-5"></label>
                      <div class="col-sm-3 col-xs-7">
                        <a href="<?= site_url('product21controller') ?>" class="btn btn-sm">CANCEL</a>
                        <input type="submit" class="btn btn-sm" value="SAVE" />
                      </div>
                    </div>
                  </div>
                  
                  
                </div>
              </form>
              
            </div>
          </div>
        </div>
      </div>
      
    </div>

    </body>
</html>