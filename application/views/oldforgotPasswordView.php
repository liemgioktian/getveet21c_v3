<!DOCTYPE html>
<html lang="en"  class="body-error"><head>
        <meta charset="utf-8">
        <title>Night Login Form - Sign In</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="<?= base_url('asset/css/login.css') ?>" rel="stylesheet">

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->


    </head>

    <body>

        <div id="wrapper">

            <form action="" class="form-login" method="post"> 

                <div class="header">
                    <a href="" class="link-head-left"></a>
                    <span>reset Password</span>
                    <a href="javascript:void(0);" tabindex="1" class="link-head-right main-item"></a>
<!--                    <div class="config-box"> 
                        <div class="colors">
                            <div class="clear"></div> 
                        </div>
                        <div class="style-bg">
                            <a href="/demo/loginform/black/index.html">Black</a>
                            <a href="/demo/loginform/white/index.html" class="active">White</a> 
                        </div> 
                    </div>-->
                </div>

                <div class="avatar"><img src="<?= base_url('asset/images/avatar.png') ?>" alt=""></div>

                <div class="inputs">
                    <input name="email" type="text" required placeholder="Email" />
                    <div class="clear"></div>
                    <div class="button-login"><input type="submit" value="Send email"></div>
                </div>

                <div class="footer-login">
<!--                    <ul class="social-links">
                        <li class="twitter"><a href="#"><span>twitter</span></a></li>
                        <li class="google-plus"><a href="#"><span>google</span></a></li>
                        <li class="facebook"><a href="#"><span>facebook</span></a></li>
                    </ul>-->
                </div>
            </form>
            <div class="clear"></div>  
            <div class="link-to-page">Already  have an account? <a href="<?= base_url() ?>">Sign in now!</a></div>      
        </div>
    </body>
</html>

