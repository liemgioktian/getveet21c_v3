<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Landing page</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
    </head>
    <body>

        <div class="container">
            <div class="row main">
                <div class="col-md-4">
                    <img src="<?php echo base_url('asset/css/images/' . $settings['logo']) ?>" alt="" style="width:100%">
                </div>
                <div class="col-md-8 text-right">
                    <h2> <?php echo $settings['electricians_company_name'] ?></h2>
                    <h3><i class="glyphicon glyphicon-phone"></i> <?php echo $settings['telephone_number'] ?></h3>
                    <h4>ABN <?php echo $settings['abn_number'] ?></h4>
                    <h4>REC <?php echo $settings['rec_number'] ?></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php if (isset($settings['landing_header_image']) && $settings['landing_header_image']): ?>
                    <img style="width:100%" src="<?php echo base_url('asset/css/images/' . $settings['landing_header_image']) ?>" alt="">
                    <?php endif ?>
                </div>
            </div>

            <div class="row" style="margin-top:20px">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if (isset($settings['landing_text']) && $settings['landing_text']): ?>
                                <?php echo $settings['landing_text'] ?>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if (isset($settings['landing_footer_image']) && $settings['landing_footer_image']): ?>
                            <img style="width:100%" src="<?php echo base_url('asset/css/images/' . $settings['landing_footer_image']) ?>" alt="">
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4"><iframe style="width:100%; border:none; min-height: 700px" src="<?php echo $settings['lead_form_url'] ?>"></iframe></div>
            </div>
        </div>

        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
    </body>
</html>