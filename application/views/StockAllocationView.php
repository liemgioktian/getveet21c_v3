<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>assignment list</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row main">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="box_label text-center">
                            <input type="hidden" name="action" id="action">
                            <input name="action" id="action" type="hidden">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>STOCK ALLOCATION</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <!--page 2-->
                            <div class="page" id="page2">
                                <div class="row">
                                    <div class="col-sm-4 hidden-xs">
                                        <img src="<?= $logo ?>" style="height:80px" alt="">
                                    </div>
                                    <div class="col-sm-4">
                                        <h2 class="text-center bold">VEEC 21</h2>
                                        <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                    </div>
                                    <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                    <div class="col-sm-3 hidden-xs">
                                        <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">--?
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 box_desc">
                                        <div class="row">
                                            
                                            <form method="POST">
                                            	<!-- company -->
                                            	<div class="col-xs-12 col-md-6 col-md-offset-2">
                                            		<div class="form-group">
                                            			<label>Company</label>
                                            			<input type="text" class="form-control input-sm"
                                            			disabled="disabled" 
                                            			value="<?= $installer['electricians_company_name'] ?>" />
                                            		</div>
                                            	</div>
                                            	<!-- company -->
                                            	<!-- date -->
                                            	<div class="col-xs-12 col-md-2">
                                            		<div class="form-group">
                                            			<label>Date</label>
                                            			<input type="text" disabled="disabled" 
                                            			value="<?= date("d/m/Y") ?>" 
                                            			class="form-control input-sm" />
                                            		</div>
                                            	</div>
                                            	<!-- date -->
                                            	<!-- installer -->
                                            	<div class="col-xs-12 col-md-3 col-md-offset-2">
                                            		<div class="form-group">
                                            			<label>Installer</label>
                                            			<input type="text" disabled="disabled" 
                                            			value="<?= $installer['first_name'].' '.$installer['last_name'] ?>" 
                                            			class="form-control input-sm" />
                                            		</div>
                                            	</div>
                                            	<!-- installer -->
                                            	<!-- items -->
                                            	<div class="col-xs-12 col-md-5">
                                            		<div class="form-group">
                                            			<label></label>
	                                            		<div class="table-responsive" style="overflow-x:auto;max-width:100%">
	                                                    	<table cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
	                                                        	<thead>
	                                                            	<tr class="skyblue">
	                                                            		<th>Item</th>
	                                                            		<th>Quantity</th>
	                                                            	</tr>
	                                                            </thead>
	                                                            <tbody>
	                                                            	<?php foreach($items as $item): ?>
	                                                            		<tr>
	                                                            			<td><?= $item->id ?></td>
	                                                            			<td>
	                                                            				<input type="text" name="<?= $item->item_id ?>" />
	                                                            			</td>
	                                                            		</tr>
	                                                            	<?php endforeach; ?>
	                                                            </tbody>
	                                                        </table>
	                                                    </div>
	                                                </div>
                                            	</div>
                                            	<!-- items -->
                                            	<!-- submit -->
                                            	<div class="col-xs-12 col-md-offset-2">
                                            		<div class="form-group">
                                            			<input type="submit" value="Save Changes" class="btn btn-warning" />
                                            		</div>
                                            	</div>
                                            	<!-- submit -->
                                            </form>
                                            
                                            <div class="col-xs-12">
                                                <div class="table-responsive" style="overflow-x:auto;max-width:100%">
                                                    <table id="tcodes_tbl" cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
                                                        <thead>
                                                            <tr class="skyblue">
                                                            	<th>ALLOCATION DATE</th>
                                                            	<th>ITEM ID</th>
                                                            	<th>ALLOCATED QUANTITY</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        	<?php foreach($logs as $log): ?>
                                                        		<tr>
                                                        			<td><?= date('d/m/Y',$log->time) ?></td>
                                                        			<td><?= $log->id ?></td>
                                                        			<td class="text-right"><?= $log->stock ?></td>
                                                        		</tr>
                                                        	<?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--close page2-->
                        </div>
                    </div>
                </div>
            </div>      
        </div>

        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('asset/js/jquery-ui.min.js') ?>"></script>
    </body>
</html>