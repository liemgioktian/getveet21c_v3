<!DOCTYPE html>
<html lang="en"  class="body-error"><head>
        <meta charset="utf-8">
        <title>Get Veet21C Sign Up</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="<?= base_url('asset/css/login.css') ?>" rel="stylesheet">

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->


    </head>

    <body>

        <div id="wrapper">

            <form action="<?= site_url('authcontroller/register') ?>" class="form-login" method="post"> 

                <div class="header">
                    <a href="" class="link-head-left"></a>
                    <span>Sign up</span>
                    <a href="javascript:void(0);" tabindex="1" class="link-head-right main-item"></a>
                </div>

                <div class="avatar"><img src="<?= base_url('asset/images/avatar.png') ?>" alt=""></div>

                <div class="inputs">
                	<select name="cid">
                		<?php foreach($companies as $company): ?>
                			<option value="<?= $company->cid ?>"><?= $company->electricians_company_name ?></option>
                		<?php endforeach; ?>
                	</select>
                    <input name="email" type="text" required placeholder="Email" />
                    <input name="password" type="password" required placeholder="Password" />
                    <input name="password2" type="password" required placeholder="Password" />
                    <div class="clear"></div>
                    <div class="button-login"><input type="submit" value="Sign up"></div>
                </div>

                <div class="footer-login">
                </div>
            </form>
            <div class="clear"></div>  
            <div class="link-to-page">Already  have an account? <a href="<?= base_url() ?>">Sign in now!</a></div>      
        </div>
    </body>
</html>

