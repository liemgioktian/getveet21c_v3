<!DOCTYPE html>
<html lang="en"  class="body-error"><head>
        <meta charset="utf-8">
        <title>Get Veet21C Sign Up</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="<?= base_url('asset/css/login.css') ?>" rel="stylesheet">

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">



    </head>

    <body>

        <div class="container">
            <form action="<?= site_url('authcontroller/register') ?>" class="form-login" method="post"> 
                <div class="row box_desc">
                    <div class="col-sm-12 text-center">
                        <h3 class="bold text-center">Create Adminsrator log in</h3>
                    </div>
                </div>
                <div class="row box_desc">
                    <div class="col-sm-12 text-center">
                        <img src="<?= base_url('asset/images/avatar.png') ?>" class="text-center" style="width:150px" alt="">
                    </div>
                </div>
                <div class="row box_desc">
                    <div class="col-sm-12">
                        <!--
                        <select name="cid">
                            <?php foreach($companies as $company): ?>
                                <option value="<?= $company->cid ?>"><?= $company->electricians_company_name ?></option>
                            <?php endforeach; ?>
                        </select>
                        -->
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-4">
                                <input class="form-control input-md" name="email" type="text" required placeholder="Email" />
                                <input class="form-control input-md" name="password" type="password" required placeholder="Password" />
                                <input class="form-control input-md" name="password2" type="password" required placeholder="Password" />
                                <div class="clear"></div>
                                <div class="button-login"><input type="submit" value="Sign up"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row space">
                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-sm btn-primary back" id="back4" name="back4" data-back="4">BACK</button>
                        <button type="button" class="btn btn-sm btn-primary next" id="next6" name="next6" data-next="6">NEXT</button>
                    </div>
                </div>
            </form>
        </div>

        <script src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery.ui.touch-punch.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
    </body>
</html>

