<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Stock Register</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="box_label text-center">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>STOCK RUNNING SHEET</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <!--page 2-->
                            <div class="page" id="">
                                <div class="row">
                                    <div class="col-sm-4 hidden-xs">
                                        <img src="<?= $logo ?>" style="height:80px" alt="">
                                    </div>
                                    <div class="col-sm-4">
                                        <h2 class="text-center bold">VEEC 21</h2>
                                        <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                    </div>
                                    <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                    <div class="col-sm-3 hidden-xs">
                                        <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 box_desc">
                                        <div class="row space">
                                            <div class="col-sm-6 text-center">
                                            	<h3 class="space">ADMINSTRATOR'S INSTALLERS</h3>
                                                <!-- Split button -->
                                                <div class="btn-group">
                                                  <button type="button" class="btn btn-success">Choose Your Installer...</button>
                                                  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                  </button>
                                                  <ul class="dropdown-menu">
                                                    <?php foreach($installers as $installer): ?>
                                                    <li><a 
                                                    href="<?= site_url("stockcontroller/installer/$installer->iid/installer") ?>" 
                                                    data-cid="<?= $installer->cid ?>"
                                                    class="btn btn-sm btn-primary btn-installer" 
                                                    style="margin: 10px;">
                                                        <?= "$installer->first_name $installer->last_name" ?>
                                                    </a></li>
                                                    <?php endforeach; ?>
                                                  </ul>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 text-center">
                                                <h3 class="space">COMPANIES</h3>
                                                <div class="btn-group">
                                                  <button type="button" class="btn btn-info">Choose The Company...</button>
                                                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                  </button>
                                                  <ul class="dropdown-menu">
                                                    <?php foreach($companies as $company): ?>
                                                        <li><a 
                                                        href="<?= site_url("stockcontroller/installer/$company->cid/company") ?>" 
                                                        data-cid="<?= $company->cid ?>"
                                                        class="btn btn-sm btn-primary btn-installer" 
                                                        style="margin: 10px;">
                                                            <?= "$company->electricians_company_name" ?>
                                                        </a></li>
                                                    <?php endforeach; ?>
                                                  </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--close page2-->
                        </div>
                    </div>
                </div>
                <!-- close running sheet -->
            </div>      
        </div>
    </body>
</html>