<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>My Electrician Online Login</title>
  <link rel="stylesheet" href="<?= base_url('asset/css/new-login.css') ?>">
  <link rel="stylesheet" href="<?= base_url('asset/css/bootstrap.min.css') ?>">
  <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-sm-offset-3 col-sm-6">
        <form action="" class="" method="post"> 
          <div class="text-center" style="text-align:center">
            <img src="<?= base_url('asset/images/logo-grey.png') ?>" alt="" style="width:250px;margin-top:50px;margin-bottom:30px;">
          </div>
          <input type="password" name="password" class="form-control input-md password" required placeholder="Password">
          <input type="password" name="confirm" class="form-control input-md password" required placeholder="Re-type Password">
          <input type="submit" value="Set New Password" class="login-submit">
        </form>
      </div>
    </div>
  
  </div>

</body>
</html>