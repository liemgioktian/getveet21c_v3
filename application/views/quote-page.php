<form enctype="multipart/form-data" action="" class="form-horizontal" method="post" role="form" id="quote">
                                        <input type="hidden" id="action1" name="action1">
                                        <div class="page">
                                            <div class="row">
                                                <div class="col-sm-4 hidden-xs">
                                                    <img src="<?= $logo ?>" style="height:80px" alt="">
                                                </div>
                                                <div class="col-sm-4">
                                                    <h2 class="text-center bold">CL VEEC</h2>
                                                    <h4 class="text-center bold" style="margin-bottom:10px">Installation Details</h4>
                                                </div>
                                                <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                                <div class="col-sm-3 hidden-xs">
                                                    <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="quote_up1" class="col-xs-1 box_label quote_up hidden"><button type="button" id="quote_up_button1" class="btn btn-danger btn-xs quote_up_button" data-button="1"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="quote_down1" class="col-xs-1 box_label quote_down"><button type="button" id="quote_down_button1" class="btn btn-success btn-xs quote_down_button" data-button="1"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Energy Consumer's details</h5>
                                                </div>
                                            </div>
                                            <div class="row box_desc quote_class1">
                                                <div class="col-sm-12">
                                                    <div class="form-group small_space">
                                                        <div class="col-sm-6">
                                                            <label for="C5">Company name</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C5" name="C5" data-cell="C5" data-formula="IF(#survey!C6=0,' ',#survey!C6)"
                                                            value="<?= isset($data['quote']['C5'])?$data['quote']['C5']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label for="O5">Contact person</label>
                                                            <input type="text" class="form-control input-sm yellow" id="O5" name="O5" data-cell="O5" data-formula="IF(#survey!U6=0,' ',#survey!U6)"
                                                            value="<?= isset($data['quote']['O5'])?$data['quote']['O5']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label for="U5">Contact phone</label>
                                                            <input type="text" class="form-control input-sm yellow" id="U5" name="U5" data-cell="U5" data-formula="IF(#survey!AA6=0,' ',#survey!AA6)"
                                                            value="<?= isset($data['quote']['U5'])?$data['quote']['U5']:'' ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group small_space">
                                                        <div class="col-sm-6">
                                                            <label for="C8">Installation street address</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C8" name="C8" data-cell="C8" data-formula="IF(#survey!C9=0,' ',#survey!C9&', '&#survey!M9&' '&#survey!W9&' '&#survey!Y9)"
                                                            value="<?= isset($data['quote']['C8'])?$data['quote']['C8']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="O8">Email</label>
                                                            <input type="text" class="form-control input-sm yellow" id="O8" name="O8" data-cell="O8" data-formula="IF(#survey!AI6=0,' ',#survey!AI6)"
                                                            value="<?= isset($data['quote']['O8'])?$data['quote']['O8']:'' ?>">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                            </div>

                                            <div class="row">
                                                <div id="quote_up2" class="col-xs-1 box_label quote_up hidden"><button type="button" id="quote_up_button2" class="btn btn-danger btn-xs quote_up_button" data-button="2"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="quote_down2" class="col-xs-1 box_label quote_down"><button type="button" id="quote_down_button2" class="btn btn-success btn-xs quote_down_button" data-button="2"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Item costing</h5>
                                                </div>
                                            </div>
                                            <div class="row quote_class2">
                                                <div class="col-sm-12 box_desc">
                                                    <div class="table-responsive">
                                                        <table id="quote_tbl" class="table table-bordered table-condesed table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th class="small center"><label for=""></label></th>
                                                                    <th class="small center"><label for="">Room/rea</label></th>
                                                                    <th class="small center"><label for="">Lamp type & ballast</label></th>
                                                                    <th class="small center"><label for="">No. Lamps</label></th>
                                                                    <th class="small center"><label for="">Lamp unit price</label></th>
                                                                    <th class="small center"><label for="">Install cost per unit</label></th>
                                                                    <th class="small center"><label for="">Recyc. levy per unit</label></th>
                                                                    <th class="small center"><label for="">Other cost per unit</label></th>
                                                                    <th class="small center"><label for="">Total room cost</label></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="quote_body">
                                                                <tr>
                                                                    <td class="text-center">
                                                                        <label for="" id="B13" name="B13" data-cell="B13" data-formula="C13"></label>
                                                                        <input type="hidden" class="input-sm form-control text-center" id="C13" name="C13" data-cell="C13" value="1">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center width-250" id="D13" name="D13" data-cell="D13" data-formula="#survey!C15"
                                                                      value="<?= isset($data['quote']['D13'])?$data['quote']['D13']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center width-250" id="G13" name="G13" data-cell="G13" data-formula="#survey!AA15"
                                                                      value="<?= isset($data['quote']['G13'])?$data['quote']['G13']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center" id="M13" name="M13" data-cell="M13" data-formula="#survey!AS15"
                                                                      value="<?= isset($data['quote']['M13'])?$data['quote']['M13']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                        <input type="text" class="input-sm form-control text-center yellow" data-format="$ 0,0[.]00" id="O13" name="O13" data-cell="O13"
                                                                      value="<?= isset($data['quote']['O13'])?$data['quote']['O13']:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center" id="N13" name="N13" data-cell="N13" data-formula="O13*M13"
                                                                        value="<?= isset($data['quote']['N13'])?$data['quote']['N13']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                        <input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="Q13" name="Q13" data-cell="Q13"
                                                                        value="<?= isset($data['quote']['Q13'])?$data['quote']['Q13']:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center" id="P13" name="P13" data-cell="P13" data-formula="Q13*M13"
                                                                        value="<?= isset($data['quote']['P13'])?$data['quote']['P13']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                        <input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="S13" name="S13" data-cell="S13"
                                                                        value="<?= isset($data['quote']['S13'])?$data['quote']['S13']:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center" id="R13" name="R13" data-cell="R13" data-formula="S13*M13"
                                                                        value="<?= isset($data['quote']['R13'])?$data['quote']['R13']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                        <input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="U13" name="U13" data-cell="U13"
                                                                        value="<?= isset($data['quote']['U13'])?$data['quote']['U13']:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center" id="T13" name="T13" data-cell="T13" data-formula="U13*M13"
                                                                        value="<?= isset($data['quote']['T13'])?$data['quote']['T13']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="W13" name="W13" data-cell="W13" data-formula="IF(((O13+Q13+S13+U13)*M13)>0,((O13+Q13+S13+U13)*M13),' ')"
                                                                      value="<?= isset($data['quote']['W13'])?$data['quote']['W13']:'' ?>">
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                            <tfoot id="quote_foot">
                                                                <tr>
                                                                    <th class="text-right" colspan="3"><label for="">Sub totals :</label></th>
                                                                    <th class="text-right">&nbsp;</th>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="O49" name="O49" data-cell="O49" data-formula="SUM(N13:N23)"
                                                                      value="<?= isset($data['quote']['O49'])?$data['quote']['O49']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="Q49" name="Q49" data-cell="Q49" data-formula="SUM(P13:P23)"
                                                                      value="<?= isset($data['quote']['Q49'])?$data['quote']['Q49']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="S49" name="S49" data-cell="S49" data-formula="SUM(R13:R23)"
                                                                      value="<?= isset($data['quote']['S49'])?$data['quote']['S49']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="U49" name="U49" data-cell="U49" data-formula="SUM(T13:T23)"
                                                                      value="<?= isset($data['quote']['U49'])?$data['quote']['U49']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="W49" name="W49" data-cell="W49" data-formula="SUM(W13:W23)"
                                                                      value="<?= isset($data['quote']['W49'])?$data['quote']['W49']:'' ?>">
                                                                    </td>
                                                                </tr>
                                                            </tfoot>
                                                            
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="quote_up3" class="col-xs-1 box_label quote_up hidden"><button type="button" id="quote_up_button3" class="btn btn-danger btn-xs quote_up_button" data-button="3"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="quote_down3" class="col-xs-1 box_label quote_down"><button type="button" id="quote_down_button3" class="btn btn-success btn-xs quote_down_button" data-button="3"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Additional costing</h5>
                                                </div>
                                            </div>
                                            <div class="row quote_class3 box_desc">
                                                <div class="col-sm-12 ">
                                                    <div class="table-responsive">
                                                        <table id="quote_tbl2" class="table table-bordered table-condesed table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th class="small center width-50"><label for=""></label></th>
                                                                    <th class="small center"><label for="">Description</label></th>
                                                                    <th class="small center width-150"><label for="">Amount</label></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="quote_body2">
                                                                <?php $qbd=54; while($qbd==54||isset($data['quote']["D$qbd"])): ?>
                                                                <tr>
                                                                    <td class="text-center">
                                                                        <label class="width-50" for="" id="C<?= $qbd ?>" name="C<?= $qbd ?>" data-cell="C<?= $qbd ?>" data-formula="B<?= $qbd ?>"></label>
                                                                        <input type="hidden" class="input-sm form-control text-center" id="B<?= $qbd ?>" name="B<?= $qbd ?>" data-cell="B<?= $qbd ?>" value="<?= $qbd-53 ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center" id="D<?= $qbd ?>" name="D<?= $qbd ?>" data-cell="D<?= $qbd ?>"
                                                                      value="<?= isset($data['quote']["D$qbd"])?$data['quote']["D$qbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center width-150" data-format="$ 0,0[.]00" id="W<?= $qbd ?>" name="W<?= $qbd ?>" data-cell="W<?= $qbd ?>"
                                                                      value="<?= isset($data['quote']["W$qbd"])?$data['quote']["W$qbd"]:'' ?>">
                                                                    </td>
                                                                </tr>
                                                                <?php $qbd++; endwhile; ?>
                                                            </tbody>
                                                            <tfoot id="quote_foot2">
                                                                <tr>
                                                                    <th class="text-right">&nbsp;</th>
                                                                    <th class=""><button class="btn btn-sm" id="btn-add-cost">add row</button></th>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="W62" name="W62" data-cell="W62" data-formula="SUM(W54:W60)"
                                                                      value="<?= isset($data['quote']['W62'])?$data['quote']['W62']:'' ?>">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-right" colspan="2"><label for="">Total job cost (ex GST): </label></td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="W64" name="W64" data-cell="W64" data-formula="W62+W49"
                                                                      value="<?= isset($data['quote']['W64'])?$data['quote']['W64']:'' ?>">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-right" colspan="2"><label for="">less Estimated certificate value: </label></td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="W66" name="W66" data-cell="W66" data-formula="H66*#survey!AV51"
                                                                      value="<?= isset($data['quote']['W66'])?$data['quote']['W66']:'' ?>">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-right" colspan="2"><label for="">Total Cost after Incentive: </label></td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="W68" name="W68" data-cell="W68" data-formula="W64-W66"
                                                                      value="<?= isset($data['quote']['W68'])?$data['quote']['W68']:'' ?>">
                                                                    </td>
                                                                </tr>
                                                            </tfoot>
                                                            
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <label for="">Current certificate price*:</label>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control yellow input-sm" id="H66" name="H66" data-cell="H66"
                                                                value="<?= isset($data['quote']['H66'])?$data['quote']['H66']:'' ?>">
                                                                <div class="input-group-addon">/ VEEC</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <p class="text-justify">*This document illustrates only the costs savings in energy usage derived from the upgraded lighting products and the eligible environmental certificate value.  In addition, many energy efficient lighting solutions offer significantly increased service life or longer service intervals which may further reduce the operational cost of the upgraded lighting system.  </p>
                                                            <p class="text-justify">PLEASE NOTE: This assessment has been prepared based on information supplied and indicates only the possible eligibility of the energy saving initiative to create environmental certificates.  Green Energy Trading takes no responsibility for the accuracy of this estimate until such time as all correct and complete documentation has been received.</p>
                                                </div>
                                            </div>

                                            <h4 style="margin-top:20px;margin-bottom:10px">Project Payback Analysis</h4>
                                            <div class="row">
                                                <div id="quote_up4" class="col-xs-1 box_label quote_up hidden"><button type="button" id="quote_up_button4" class="btn btn-danger btn-xs quote_up_button" data-button="4"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="quote_down4" class="col-xs-1 box_label quote_down"><button type="button" id="quote_down_button4" class="btn btn-success btn-xs quote_down_button" data-button="4"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Energy saving / C02 analysis</h5>
                                                </div>
                                            </div>
                                            <div class="row box_desc quote_class4">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="K84" class="col-sm-6">Current annual energy usage (lighting):</label>
                                                        <div class="col-sm-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-sm" id="K84" name="K84" data-cell="K84" data-formula="#survey!Z50/10" data-format="0.00"
                                                                value="<?= isset($data['quote']['K84'])?$data['quote']['K84']:'' ?>">
                                                                <div class="input-group-addon input-sm">MWh</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="K86" class="col-sm-6">Est. annual energy usage after upgrade:</label>
                                                        <div class="col-sm-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-sm" id="K86" name="K86" data-cell="K86" data-formula="#survey!AT50/10" data-format="0,0.00"
                                                                value="<?= isset($data['quote']['K86'])?$data['quote']['K86']:'' ?>">
                                                                <div class="input-group-addon input-sm">MWh</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="K88" class="col-sm-6">Annual energy savings (lighting activities):</label>
                                                        <div class="col-sm-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-sm" id="K88" name="K88" data-cell="K88" data-formula="K84-K86" data-format="0,0.00"
                                                                value="<?= isset($data['quote']['K88'])?$data['quote']['K88']:'' ?>">
                                                                <div class="input-group-addon input-sm">MWh</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="K90" class="col-sm-6">Annual energy savings (lighting activities):</label>
                                                        <div class="col-sm-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-sm" id="K90" name="K90" data-cell="K90" data-formula="#survey!AV51" data-format="0,0.00"
                                                                value="<?= isset($data['quote']['K90'])?$data['quote']['K90']:'' ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="K92" class="col-sm-6">Annual energy savings (lighting activities):</label>
                                                        <div class="col-sm-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-sm" id="K92" name="K92" data-cell="K92" data-formula="K90/10" data-format="0,0.00"
                                                                value="<?= isset($data['quote']['K92'])?$data['quote']['K92']:'' ?>">
                                                                <div class="input-group-addon input-sm">t</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="U84" class="col-sm-6">Current electricity supply rate:</label>
                                                        <div class="col-sm-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-sm yellow" id="U84" name="U84" data-cell="U84" data-format="0,0.00"
                                                                value="<?= isset($data['quote']['U84'])?$data['quote']['U84']:'' ?>">
                                                                <div class="input-group-addon input-sm">/KWh</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="U86" class="col-sm-6">Estimated annual energy savings:</label>
                                                        <div class="col-sm-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-sm" id="U86" name="U86" data-cell="U86" data-formula="U84*1000*K88" data-format="0,0.00"
                                                                value="<?= isset($data['quote']['U86'])?$data['quote']['U86']:'' ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="U88" class="col-sm-6">Current VEEC buyback rate:</label>
                                                        <div class="col-sm-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-sm" id="U88" name="U88" data-cell="U88" data-formula="H66"
                                                                value="<?= isset($data['quote']['U88'])?$data['quote']['U88']:'' ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="U90" class="col-sm-6">Estimated value of VEECs:</label>
                                                        <div class="col-sm-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-sm" id="U90" name="U90" data-cell="U90" data-formula="U88*K90" data-format="0,0.00"
                                                                value="<?= isset($data['quote']['U90'])?$data['quote']['U90']:'' ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="quote_up5" class="col-xs-1 box_label quote_up hidden"><button type="button" id="quote_up_button5" class="btn btn-danger btn-xs quote_up_button" data-button="5"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="quote_down5" class="col-xs-1 box_label quote_down"><button type="button" id="quote_down_button5" class="btn btn-success btn-xs quote_down_button" data-button="5"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Investment payback analysis</h5>
                                                </div>
                                            </div>
                                            <div class="row box_desc quote_class5">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="K96" class="col-sm-6">Cost of lighting upgrade - products:</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control input-sm" id="K96" name="K96" data-cell="K96" data-formula="O49" data-format="$ 0,0[.]00"
                                                            value="<?= isset($data['quote']['K96'])?$data['quote']['K96']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="K98" class="col-sm-6">Cost of lighting upgrade - installation:</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control input-sm" id="K98" name="K98" data-cell="K98" data-formula="Q49" data-format="$ 0,0[.]00"
                                                            value="<?= isset($data['quote']['K98'])?$data['quote']['K98']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="K100" class="col-sm-6">Environmental levy (lamp recycling):</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control input-sm" id="K100" name="K100" data-cell="K100" data-formula="S49" data-format="$ 0,0[.]00"
                                                            value="<?= isset($data['quote']['K100'])?$data['quote']['K100']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="K102" class="col-sm-6">Other costs:</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control input-sm" id="K102" name="K102" data-cell="K102" data-formula="U49+W62" data-format="$ 0,0[.]00"
                                                            value="<?= isset($data['quote']['K102'])?$data['quote']['K102']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="K104" class="col-sm-6">less estimated value of VEECs:</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control input-sm" id="K104" name="K104" data-cell="K104" data-formula="U90" data-format="$ 0,0[.]00"
                                                            value="<?= isset($data['quote']['K104'])?$data['quote']['K104']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="K106" class="col-sm-6">Total cost of lighting upgrade:</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control input-sm" id="K106" name="K106" data-cell="K106" data-formula="K96+K98+K100+K102-K104" data-format="$ 0,0[.]00"
                                                            value="<?= isset($data['quote']['K106'])?$data['quote']['K106']:'' ?>">
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-sm-6"></div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="K108" class="col-sm-6">Simple payback period:</label>
                                                        <div class="col-sm-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-sm" id="K108" name="K108" data-cell="K108" data-formula="K106/U86" data-format="0.00"
                                                                value="<?= isset($data['quote']['K10'])?$data['quote']['K108']:'' ?>">
                                                                <div class="input-group-addon input-sm">Yrs</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="K110" class="col-sm-6">Net present value:</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control input-sm" id="K110" name="K110" data-cell="K110" data-formula="NPV(U110,AM110,AM111,AM112,AM113,AM114,AM115,AM116,AM117,AM118,AM119,AM120)" data-format="$ 0,0[.]"
                                                            value="<?= isset($data['quote']['K110'])?$data['quote']['K110']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="K112" class="col-sm-6">Net present value:</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control input-sm" id="K112" name="K112" data-cell="K112" data-formula="IRR(AM110:AM120)" data-format="0[.]0 %"
                                                            value="<?= isset($data['quote']['K112'])?$data['quote']['K112']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="" class="text-center"><u><i>Assumptions</i></u></label>
                                                    <div class="form-group">
                                                        <label for="U108" class="col-sm-6"><i>Investment period:</i></label>
                                                        <div class="col-sm-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control input-sm" id="U108" name="U108" data-cell="U108"
                                                                value="<?= isset($data['quote']['U108'])?$data['quote']['U108']:'' ?>">
                                                                <div class="input-group-addon input-sm">Yrs</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="U110" class="col-sm-6"><i>Discount rate: </i></label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control input-sm" id="U110" name="U110" data-cell="U110" data-format=" 0[.]00 %"
                                                            value="<?= isset($data['quote']['U110'])?$data['quote']['U110']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="U112" class="col-sm-6"><i>Annual electricity price increase: </i></label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control input-sm" id="U112" name="U112" data-cell="U112" data-format=" 0[.]00 %"
                                                            value="<?= isset($data['quote']['U112'])?$data['quote']['U112']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row box_desc quote_class5">
                                                <div class="hidden">
                                                    <div class="col-sm-2">
                                                        <input type="text" id="AL110" name="AL110" data-cell="AL110"
                                                        value="<?= isset($data['quote']['AL110'])?$data['quote']['AL110']:'' ?>">
                                                        <input type="text" id="AL111" name="AL111" data-cell="AL111" value="1">
                                                        <input type="text" id="AL112" name="AL112" data-cell="AL112" data-formula="AL111+1"
                                                        value="<?= isset($data['quote']['AL112'])?$data['quote']['AL112']:'' ?>">
                                                        <input type="text" id="AL113" name="AL113" data-cell="AL113" data-formula="AL112+1"
                                                        value="<?= isset($data['quote']['AL113'])?$data['quote']['AL113']:'' ?>">
                                                        <input type="text" id="AL114" name="AL114" data-cell="AL114" data-formula="AL113+1"
                                                        value="<?= isset($data['quote']['AL114'])?$data['quote']['AL114']:'' ?>">
                                                        <input type="text" id="AL115" name="AL115" data-cell="AL115" data-formula="AL114+1"
                                                        value="<?= isset($data['quote']['AL115'])?$data['quote']['AL115']:'' ?>">
                                                        <input type="text" id="AL116" name="AL116" data-cell="AL116" data-formula="AL115+1"
                                                        value="<?= isset($data['quote']['AL116'])?$data['quote']['AL116']:'' ?>">
                                                        <input type="text" id="AL117" name="AL117" data-cell="AL117" data-formula="AL116+1"
                                                        value="<?= isset($data['quote']['AL117'])?$data['quote']['AL117']:'' ?>">
                                                        <input type="text" id="AL118" name="AL118" data-cell="AL118" data-formula="AL117+1"
                                                        value="<?= isset($data['quote']['AL118'])?$data['quote']['AL118']:'' ?>">
                                                        <input type="text" id="AL119" name="AL119" data-cell="AL119" data-formula="AL118+1"
                                                        value="<?= isset($data['quote']['AL119'])?$data['quote']['AL119']:'' ?>">
                                                        <input type="text" id="AL120" name="AL120" data-cell="AL120" data-formula="AL119+1"
                                                        value="<?= isset($data['quote']['AL120'])?$data['quote']['AL120']:'' ?>">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" id="AM110" name="AM110" data-cell="AM110" data-format="0,0" data-formula="(-1*K106)"
                                                        value="<?= isset($data['quote']['AM110'])?$data['quote']['AM110']:'' ?>">
                                                        <input type="text" id="AM111" name="AM111" data-cell="AM111" data-format="0,0" data-formula="K88*1000*U84"
                                                        value="<?= isset($data['quote']['AM111'])?$data['quote']['AM111']:'' ?>">
                                                        <input type="text" id="AM112" name="AM112" data-cell="AM112" data-format="0,0" data-formula="AM111*(1+U112)"
                                                        value="<?= isset($data['quote']['AM112'])?$data['quote']['AM112']:'' ?>">
                                                        <input type="text" id="AM113" name="AM113" data-cell="AM113" data-format="0,0" data-formula="AM112*(1+U112)"
                                                        value="<?= isset($data['quote']['AM113'])?$data['quote']['AM113']:'' ?>">
                                                        <input type="text" id="AM114" name="AM114" data-cell="AM114" data-format="0,0" data-formula="AM113*(1+U112)"
                                                        value="<?= isset($data['quote']['AM114'])?$data['quote']['AM114']:'' ?>">
                                                        <input type="text" id="AM115" name="AM115" data-cell="AM115" data-format="0,0" data-formula="AM114*(1+U112)"
                                                        value="<?= isset($data['quote']['AM115'])?$data['quote']['AM115']:'' ?>">
                                                        <input type="text" id="AM116" name="AM116" data-cell="AM116" data-format="0,0" data-formula="AM115*(1+U112)"
                                                        value="<?= isset($data['quote']['AM116'])?$data['quote']['AM116']:'' ?>">
                                                        <input type="text" id="AM117" name="AM117" data-cell="AM117" data-format="0,0" data-formula="AM116*(1+U112)"
                                                        value="<?= isset($data['quote']['AM117'])?$data['quote']['AM117']:'' ?>">
                                                        <input type="text" id="AM118" name="AM118" data-cell="AM118" data-format="0,0" data-formula="AM117*(1+U112)"
                                                        value="<?= isset($data['quote']['AM118'])?$data['quote']['AM118']:'' ?>">
                                                        <input type="text" id="AM119" name="AM119" data-cell="AM119" data-format="0,0" data-formula="AM118*(1+U112)"
                                                        value="<?= isset($data['quote']['AM119'])?$data['quote']['AM119']:'' ?>">
                                                        <input type="text" id="AM120" name="AM120" data-cell="AM120" data-format="0,0" data-formula="AM119*(1+U112)"
                                                        value="<?= isset($data['quote']['AM120'])?$data['quote']['AM120']:'' ?>">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" id="AN110" name="AN110" data-cell="AN110" value="year"
                                                        value="<?= isset($data['quote']['AN110'])?$data['quote']['AN110']:'' ?>">
                                                        <input type="text" id="AN111" name="AN111" data-cell="AN111" value="1">
                                                        <input type="text" id="AN112" name="AN112" data-cell="AN112" data-formula="AN111+1"
                                                        value="<?= isset($data['quote']['AN112'])?$data['quote']['AN112']:'' ?>">
                                                        <input type="text" id="AN113" name="AN113" data-cell="AN113" data-formula="AN112+1"
                                                        value="<?= isset($data['quote']['AN113'])?$data['quote']['AN113']:'' ?>">
                                                        <input type="text" id="AN114" name="AN114" data-cell="AN114" data-formula="AN113+1"
                                                        value="<?= isset($data['quote']['AN114'])?$data['quote']['AN114']:'' ?>">
                                                        <input type="text" id="AN115" name="AN115" data-cell="AN115" data-formula="AN114+1"
                                                        value="<?= isset($data['quote']['AN115'])?$data['quote']['AN115']:'' ?>">
                                                        <input type="text" id="AN116" name="AN116" data-cell="AN116" data-formula="AN115+1"
                                                        value="<?= isset($data['quote']['AN116'])?$data['quote']['AN116']:'' ?>">
                                                        <input type="text" id="AN117" name="AN117" data-cell="AN117" data-formula="AN116+1"
                                                        value="<?= isset($data['quote']['AN117'])?$data['quote']['AN117']:'' ?>">
                                                        <input type="text" id="AN118" name="AN118" data-cell="AN118" data-formula="AN117+1"
                                                        value="<?= isset($data['quote']['AN118'])?$data['quote']['AN118']:'' ?>">
                                                        <input type="text" id="AN119" name="AN119" data-cell="AN119" data-formula="AN118+1"
                                                        value="<?= isset($data['quote']['AN119'])?$data['quote']['AN119']:'' ?>">
                                                        <input type="text" id="AN120" name="AN120" data-cell="AN120" data-formula="AN119+1"
                                                        value="<?= isset($data['quote']['AN120'])?$data['quote']['AN120']:'' ?>">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" id="AO110" name="AO110" data-cell="AO110" data-formula="upgrade lighting"
                                                        value="<?= isset($data['quote']['AO110'])?$data['quote']['AO110']:'' ?>">
                                                        <input type="text" id="AO111" name="AO111" data-cell="AO111" data-formula="(K86*U84*1000)+K106"
                                                        value="<?= isset($data['quote']['AO111'])?$data['quote']['AO111']:'' ?>">
                                                        <input type="text" id="AO112" name="AO112" data-cell="AO112" data-formula="(K86*U84*1000)*(1+U112)"
                                                        value="<?= isset($data['quote']['AO112'])?$data['quote']['AO112']:'' ?>">
                                                        <input type="text" id="AO113" name="AO113" data-cell="AO113" data-formula="AO112*(1+U112)"
                                                        value="<?= isset($data['quote']['AO113'])?$data['quote']['AO113']:'' ?>">
                                                        <input type="text" id="AO114" name="AO114" data-cell="AO114" data-formula="AO113*(1+U112)"
                                                        value="<?= isset($data['quote']['AO114'])?$data['quote']['AO114']:'' ?>">
                                                        <input type="text" id="AO115" name="AO115" data-cell="AO115" data-formula="AO114*(1+U112)"
                                                        value="<?= isset($data['quote']['AO115'])?$data['quote']['AO115']:'' ?>">
                                                        <input type="text" id="AO116" name="AO116" data-cell="AO116" data-formula="AO115*(1+U112)"
                                                        value="<?= isset($data['quote']['AO116'])?$data['quote']['AO116']:'' ?>">
                                                        <input type="text" id="AO117" name="AO117" data-cell="AO117" data-formula="AO116*(1+U112)"
                                                        value="<?= isset($data['quote']['AO117'])?$data['quote']['AO117']:'' ?>">
                                                        <input type="text" id="AO118" name="AO118" data-cell="AO118" data-formula="AO117*(1+U112)"
                                                        value="<?= isset($data['quote']['AO118'])?$data['quote']['AO118']:'' ?>">
                                                        <input type="text" id="AO119" name="AO119" data-cell="AO119" data-formula="AO118*(1+U112)"
                                                        value="<?= isset($data['quote']['AO119'])?$data['quote']['AO119']:'' ?>">
                                                        <input type="text" id="AO120" name="AO120" data-cell="AO120" data-formula="AO119*(1+U112)"
                                                        value="<?= isset($data['quote']['AO120'])?$data['quote']['AO120']:'' ?>">
                                                        <input type="text" id="AO121" name="AO121" data-cell="AO121" data-formula="SUM(AO111:AO120)"
                                                        value="<?= isset($data['quote']['AO121'])?$data['quote']['AO121']:'' ?>">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" id="AP110" name="AP110" data-cell="AP110" data-formula="Existing Lighting"
                                                        value="<?= isset($data['quote']['AP110'])?$data['quote']['AP110']:'' ?>">
                                                        <input type="text" id="AP111" name="AP111" data-cell="AP111" data-formula="K84*U84*1000"
                                                        value="<?= isset($data['quote']['AP111'])?$data['quote']['AP111']:'' ?>">
                                                        <input type="text" id="AP112" name="AP112" data-cell="AP112" data-formula="AP111*(1+U112)"
                                                        value="<?= isset($data['quote']['AP112'])?$data['quote']['AP112']:'' ?>">
                                                        <input type="text" id="AP113" name="AP113" data-cell="AP113" data-formula="AP112*(1+U112)"
                                                        value="<?= isset($data['quote']['AP113'])?$data['quote']['AP113']:'' ?>">
                                                        <input type="text" id="AP114" name="AP114" data-cell="AP114" data-formula="AP113*(1+U112)"
                                                        value="<?= isset($data['quote']['AP114'])?$data['quote']['AP114']:'' ?>">
                                                        <input type="text" id="AP115" name="AP115" data-cell="AP115" data-formula="AP114*(1+U112)"
                                                        value="<?= isset($data['quote']['AP115'])?$data['quote']['AP115']:'' ?>">
                                                        <input type="text" id="AP116" name="AP116" data-cell="AP116" data-formula="AP115*(1+U112)"
                                                        value="<?= isset($data['quote']['AP116'])?$data['quote']['AP116']:'' ?>">
                                                        <input type="text" id="AP117" name="AP117" data-cell="AP117" data-formula="AP116*(1+U112)"
                                                        value="<?= isset($data['quote']['AP117'])?$data['quote']['AP117']:'' ?>">
                                                        <input type="text" id="AP118" name="AP118" data-cell="AP118" data-formula="AP117*(1+U112)"
                                                        value="<?= isset($data['quote']['AP118'])?$data['quote']['AP118']:'' ?>">
                                                        <input type="text" id="AP119" name="AP119" data-cell="AP119" data-formula="AP118*(1+U112)"
                                                        value="<?= isset($data['quote']['AP119'])?$data['quote']['AP119']:'' ?>">
                                                        <input type="text" id="AP120" name="AP120" data-cell="AP120" data-formula="AP119*(1+U112)"
                                                        value="<?= isset($data['quote']['AP120'])?$data['quote']['AP120']:'' ?>">
                                                        <input type="text" id="AP121" name="AP121" data-cell="AP121" data-formula="SUM(AP111:AP120)"
                                                        value="<?= isset($data['quote']['AP121'])?$data['quote']['AP121']:'' ?>">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" id="AQ110" name="AQ110" data-cell="AQ110" data-formula=""
                                                        value="<?= isset($data['quote']['AQ110'])?$data['quote']['AQ110']:'' ?>">
                                                        <input type="text" id="AQ111" name="AQ111" data-cell="AQ111" data-formula="AP111-AO111"
                                                        value="<?= isset($data['quote']['AQ111'])?$data['quote']['AQ111']:'' ?>">
                                                        <input type="text" id="AQ112" name="AQ112" data-cell="AQ112" data-formula="AP112-AO112"
                                                        value="<?= isset($data['quote']['AQ112'])?$data['quote']['AQ112']:'' ?>">
                                                        <input type="text" id="AQ113" name="AQ113" data-cell="AQ113" data-formula="AP113-AO113"
                                                        value="<?= isset($data['quote']['AQ113'])?$data['quote']['AQ113']:'' ?>">
                                                        <input type="text" id="AQ114" name="AQ114" data-cell="AQ114" data-formula="AP114-AO114"
                                                        value="<?= isset($data['quote']['AQ114'])?$data['quote']['AQ114']:'' ?>">
                                                        <input type="text" id="AQ115" name="AQ115" data-cell="AQ115" data-formula="AP115-AO115"
                                                        value="<?= isset($data['quote']['AQ115'])?$data['quote']['AQ115']:'' ?>">
                                                        <input type="text" id="AQ116" name="AQ116" data-cell="AQ116" data-formula="AP116-AO116"
                                                        value="<?= isset($data['quote']['AQ116'])?$data['quote']['AQ116']:'' ?>">
                                                        <input type="text" id="AQ117" name="AQ117" data-cell="AQ117" data-formula="AP117-AO117"
                                                        value="<?= isset($data['quote']['AQ117'])?$data['quote']['AQ117']:'' ?>">
                                                        <input type="text" id="AQ118" name="AQ118" data-cell="AQ118" data-formula="AP118-AO118"
                                                        value="<?= isset($data['quote']['AQ118'])?$data['quote']['AQ118']:'' ?>">
                                                        <input type="text" id="AQ119" name="AQ119" data-cell="AQ119" data-formula="AP119-AO119"
                                                        value="<?= isset($data['quote']['AQ119'])?$data['quote']['AQ119']:'' ?>">
                                                        <input type="text" id="AQ120" name="AQ120" data-cell="AQ120" data-formula="AP120-AO120"
                                                        value="<?= isset($data['quote']['AQ120'])?$data['quote']['AQ120']:'' ?>">
                                                        <input type="text" id="AQ121" name="AQ121" data-cell="AQ121" data-formula="AP121-AO121"
                                                        value="<?= isset($data['quote']['AQ121'])?$data['quote']['AQ121']:'' ?>">
                                                    </div>
                                                </div>
                                                <div class="hidden">
                                                    <div class="col-sm-3">
                                                        <input type="text" id="AN124" name="AN124" data-cell="AN124" data-format="0[.]" data-formula="'Years'"
                                                        value="<?= isset($data['quote']['AN124'])?$data['quote']['AN124']:'' ?>">
                                                        <input type="text" id="AN125" name="AN125" data-cell="AN125" data-format="0[.]" data-formula="YEAR(TODAY())"
                                                        value="<?= isset($data['quote']['AN125'])?$data['quote']['AN125']:'' ?>">
                                                        <input type="text" id="AN126" name="AN126" data-cell="AN126" data-format="0[.]" data-formula="AN125+1"
                                                        value="<?= isset($data['quote']['AN126'])?$data['quote']['AN126']:'' ?>">
                                                        <input type="text" id="AN127" name="AN127" data-cell="AN127" data-format="0[.]" data-formula="AN126+1"
                                                        value="<?= isset($data['quote']['AN127'])?$data['quote']['AN127']:'' ?>">
                                                        <input type="text" id="AN128" name="AN128" data-cell="AN128" data-format="0[.]" data-formula="AN127+1"
                                                        value="<?= isset($data['quote']['AN128'])?$data['quote']['AN128']:'' ?>">
                                                        <input type="text" id="AN129" name="AN129" data-cell="AN129" data-format="0[.]" data-formula="AN128+1"
                                                        value="<?= isset($data['quote']['AN129'])?$data['quote']['AN129']:'' ?>">
                                                        <input type="text" id="AN130" name="AN130" data-cell="AN130" data-format="0[.]" data-formula="AN129+1"
                                                        value="<?= isset($data['quote']['AN130'])?$data['quote']['AN130']:'' ?>">
                                                        <input type="text" id="AN131" name="AN131" data-cell="AN131" data-format="0[.]" data-formula="AN130+1"
                                                        value="<?= isset($data['quote']['AN131'])?$data['quote']['AN131']:'' ?>">
                                                        <input type="text" id="AN132" name="AN132" data-cell="AN132" data-format="0[.]" data-formula="AN131+1"
                                                        value="<?= isset($data['quote']['AN132'])?$data['quote']['AN132']:'' ?>">
                                                        <input type="text" id="AN133" name="AN133" data-cell="AN133" data-format="0[.]" data-formula="AN132+1"
                                                        value="<?= isset($data['quote']['AN133'])?$data['quote']['AN133']:'' ?>">
                                                        <input type="text" id="AN134" name="AN134" data-cell="AN134" data-format="0[.]" data-formula="AN133+1"
                                                        value="<?= isset($data['quote']['AN134'])?$data['quote']['AN134']:'' ?>">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="text" id="AO124" name="AO124" data-cell="AO124" data-format="$ 0,0.00" data-formula="'Upgraded Lighting'"
                                                        value="<?= isset($data['quote']['AO124'])?$data['quote']['AO124']:'' ?>">
                                                        <input type="text" id="AO125" name="AO125" data-cell="AO125" data-format="$ 0,0.00" data-formula="AO111"
                                                        value="<?= isset($data['quote']['AO125'])?$data['quote']['AO125']:'' ?>">
                                                        <input type="text" id="AO126" name="AO126" data-cell="AO126" data-format="$ 0,0.00" data-formula="AO125+AO112"
                                                        value="<?= isset($data['quote']['AO126'])?$data['quote']['AO126']:'' ?>">
                                                        <input type="text" id="AO127" name="AO127" data-cell="AO127" data-format="$ 0,0.00" data-formula="AO126+AO113"
                                                        value="<?= isset($data['quote']['AO127'])?$data['quote']['AO127']:'' ?>">
                                                        <input type="text" id="AO128" name="AO128" data-cell="AO128" data-format="$ 0,0.00" data-formula="AO127+AO114"
                                                        value="<?= isset($data['quote']['AO128'])?$data['quote']['AO128']:'' ?>">
                                                        <input type="text" id="AO129" name="AO129" data-cell="AO129" data-format="$ 0,0.00" data-formula="AO128+AO115"
                                                        value="<?= isset($data['quote']['AO129'])?$data['quote']['AO129']:'' ?>">
                                                        <input type="text" id="AO130" name="AO130" data-cell="AO130" data-format="$ 0,0.00" data-formula="AO129+AO116"
                                                        value="<?= isset($data['quote']['AO130'])?$data['quote']['AO130']:'' ?>">
                                                        <input type="text" id="AO131" name="AO131" data-cell="AO131" data-format="$ 0,0.00" data-formula="AO130+AO117"
                                                        value="<?= isset($data['quote']['AO131'])?$data['quote']['AO131']:'' ?>">
                                                        <input type="text" id="AO132" name="AO132" data-cell="AO132" data-format="$ 0,0.00" data-formula="AO131+AO118"
                                                        value="<?= isset($data['quote']['AO132'])?$data['quote']['AO132']:'' ?>">
                                                        <input type="text" id="AO133" name="AO133" data-cell="AO133" data-format="$ 0,0.00" data-formula="AO132+AO119"
                                                        value="<?= isset($data['quote']['AO133'])?$data['quote']['AO133']:'' ?>">
                                                        <input type="text" id="AO134" name="AO134" data-cell="AO134" data-format="$ 0,0.00" data-formula="AO133+AO120"
                                                        value="<?= isset($data['quote']['AO134'])?$data['quote']['AO134']:'' ?>">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="text" id="AP124" name="AP124" data-cell="AP124" data-format="$ 0,0.00" data-formula="'Existing Lighting'"
                                                        value="<?= isset($data['quote']['AP124'])?$data['quote']['AP124']:'' ?>">
                                                        <input type="text" id="AP125" name="AP125" data-cell="AP125" data-format="$ 0,0.00" data-formula="AP111"
                                                        value="<?= isset($data['quote']['AP125'])?$data['quote']['AP125']:'' ?>">
                                                        <input type="text" id="AP126" name="AP126" data-cell="AP126" data-format="$ 0,0.00" data-formula="AP125+AP112"
                                                        value="<?= isset($data['quote']['AP126'])?$data['quote']['AP126']:'' ?>">
                                                        <input type="text" id="AP127" name="AP127" data-cell="AP127" data-format="$ 0,0.00" data-formula="AP126+AP113"
                                                        value="<?= isset($data['quote']['AP127'])?$data['quote']['AP127']:'' ?>">
                                                        <input type="text" id="AP128" name="AP128" data-cell="AP128" data-format="$ 0,0.00" data-formula="AP127+AP114"
                                                        value="<?= isset($data['quote']['AP128'])?$data['quote']['AP128']:'' ?>">
                                                        <input type="text" id="AP129" name="AP129" data-cell="AP129" data-format="$ 0,0.00" data-formula="AP128+AP115"
                                                        value="<?= isset($data['quote']['AP129'])?$data['quote']['AP129']:'' ?>">
                                                        <input type="text" id="AP130" name="AP130" data-cell="AP130" data-format="$ 0,0.00" data-formula="AP129+AP116"
                                                        value="<?= isset($data['quote']['AP130'])?$data['quote']['AP130']:'' ?>">
                                                        <input type="text" id="AP131" name="AP131" data-cell="AP131" data-format="$ 0,0.00" data-formula="AP130+AP117"
                                                        value="<?= isset($data['quote']['AP131'])?$data['quote']['AP131']:'' ?>">
                                                        <input type="text" id="AP132" name="AP132" data-cell="AP132" data-format="$ 0,0.00" data-formula="AP131+AP118"
                                                        value="<?= isset($data['quote']['AP132'])?$data['quote']['AP132']:'' ?>">
                                                        <input type="text" id="AP133" name="AP133" data-cell="AP133" data-format="$ 0,0.00" data-formula="AP132+AP119"
                                                        value="<?= isset($data['quote']['AP133'])?$data['quote']['AP133']:'' ?>">
                                                        <input type="text" id="AP134" name="AP134" data-cell="AP134" data-format="$ 0,0.00" data-formula="AP133+AP120"
                                                        value="<?= isset($data['quote']['AP134'])?$data['quote']['AP134']:'' ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="box_graph">
                                                        <div class="row">
                                                            <div class="col-sm-12 text-center">
                                                                <label for="ewd" class="big">Cumulative Lighting Cost</label>
                                                            </div>
                                                        </div>
                                                        <!--
                                                        <div class="row">
                                                            <div class="col-sm-12 text-center">
                                                                <button type="button" class="btn btn-success btn-xs" id="graph_refresh">Graphic Refresh</button>
                                                            </div>
                                                        </div>
                                                    -->
                                                        <div class="row">
                                                            <div class="col-sm-10">
                                                                <div class="box_graphic">
                                                                    <div id="saving_graph" style="width:100%;height:475px;float:right;overflow-x:auto;" data-formula="GRAPH(AO125:AP134, ['legend=AO124:AP124', 'label=AN125:AN134', 'orientation=vertical'])"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label for="" class="red"><i>Total Savings</i></label><br>
                                                                <label for="" class="red" id="AP137" name="AP137" data-cell="AP137" data-formula="AP136" data-format="$ 0,0.00"></label>
                                                                <input type="hidden" id="AP136" name="AP136" data-cell="AP136" data-formula="AP134-AO134"
                                                                value="<?= isset($data['quote']['AP136'])?$data['quote']['AP136']:'' ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12 text-center">
                                                    <a href="#lighting-survey" data-toggle="tab" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span></a>
                                                    <a href="<?= site_url('maincontroller/subform') ?>" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
                                                    <a href="#assignment-form" data-toggle="tab" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>



