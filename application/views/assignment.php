<?php 
//foreach ($_POST as $key => $value) {
//    $$key = $value;
//}
?>
 
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="author" content="4121" />
    <title>Assignment Form</title>
    <style>
        body{
            padding:0;
            margin:0;
            font-family: Tahoma, Arial, Helvetica, Tahoma, serif;
            color:#000;
        }
        @page{
            margin-top:0;
            margin-left: 0.5cm;
            margin-bottom:0;
            margin-right:0.5cm;
        }
        .page{
            /*height:27.9cm;*/
            width:19cm;
            margin:0 auto;
            page-break-after:auto;
            overflow:auto;
            font-size:90%;
            background:white;
            padding:10px;
        }
        
        .watermark{ 
            background-position: bottom right; 
            background-repeat: no-repeat; 
            color: #d0d0d0;
          -webkit-transform: rotate(-20deg);
          -moz-transform: rotate(-20deg);
          margin: 0;
          z-index: -1;
        }

       
        .pagebreak{
            page-break-after:always;
            
        }
        .center{
            text-align:center;
        }
        .bold{
            font-weight: bold;
        }
        .italic{
            font-style: italic;
        }
        
        p{
            text-align:justify;
            margin:2px 0;
        }
        .title{
            font-weight:bold;
            font-size:14px;
        }
        div.content{
            padding:10px;
        }
        .box_page{
            border: solid 1px #000; 
        }
        table{
            border-collapse:collapse;
        }
        td.box{
            border:solid 1px #000;
        }
        td.grid{
            width: <?php echo 100/12 ?>%;
            padding: 2px;

        }
        td.grid-small{
            width: <?php echo 100/12 ?>%;
            padding-top: 2px;

        }
        td,p,body,li{
            font-size:10px;
        }
        .label{
            font-size: 12px;
        }
        .small{
            font-size: 8px;
        }
        .linespace{
            line-height: 20px;
            text-align:justify;
        }
        td.underline{
            border-bottom: solid .5px black;
        }
        td.text-line{
           
            text-decoration:underline;
        }
        td.dotted{
           border-bottom: dotted .5px black;
            text-decoration:none;
        }
        .tops{
            margin-top: 10px;
        }
        .top{
            margin-top: 20px;
        }
        .right{
            text-align:right;
        }
        .bottom{
            margin-bottom: 10px;
        }
        .grey{
            color:grey;
        }
        .justify{
            text-align: justify;
        }
        .border{
            border: solid 1px grey;
        }
        .border-bottom{
            border-bottom: solid 1px grey;
        }
        .border-top{
            border-top: solid 1px grey;
        }
        .border-right{
            border-right: solid 1px grey;
        }
        .border-left{
            border-left: solid 1px grey;
        }
        .zero{
            height: 0px;
        }
        .box_desc{
          background: #fff;
        }
        .box_label{
          background: #00C57D;
          color: white;
          border: solid 1px #000;
        }
        .skyblue{
          background-color: #00B1E0 !important;
          color: white;
        }
        .warm{
          background-color: #D7C100 !important;
          color: white;
        }
        .pad{
            height: 10px;
        }
        .box_grey{
            background: #dedede;
            color: #000;
        }

        .footer { position: fixed; bottom: 20px; text-align: right !important; }
    </style>
</head>
<body>
<div class="page">
    <!--page 1-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid" colspan="3"><img src="<?= $logo ?>" alt="" style="width:160px"></td>
                <td class="grid center zero" colspan="6" style="vertical-align:top">
                    <h2 class=""><strong>VEEC 21</h2>
                    <h3 style="vertical-align:bottom"> 12V Halogen downlight replacement</h3>
                </td>
                <td class="grid left" colspan="3"><img src="asset/css/images/logo.png" alt="" style="width:130px"></td>
            </tr>
            <tr>
                <td class="grid bold center zero" colspan="12">
                    <h4 class="zero" style="font-weight:normal">&nbsp;<?php echo $input_A3 ?></h4>
                </td>
            </tr>
            <tr>
                <td class="grid" colspan="2">Job Reference :</td>
                <td class="grid" colspan="2"><p class="border center" style="vertical-align:middle"><?php echo $H4 ?></p></td>
                <td class="grid" colspan="2">Client number :</td>
                <td class="grid" colspan="2"><p class="border center" style="vertical-align:middle"><?php echo $client ?></p></td>
                <td class="grid">&nbsp;</td>
                <td class="grid justify" colspan="3">
                    <p>2 Domville Avenue, Hawthorn VIC 3122</p>
                    <p>T:1300 077 784 F:03 9815 1066</p>
                    <p style="color:green">Forms@greenenergytrading.com.au</p>
                    <p>ABN 21 128 476 406</p>
                </td>

            </tr>
            <tr>
                <td class="grid bold zero box_label" colspan="12">
                    <label for="">Form of benefit provided to energy consumer</label>
                </td>
            </tr>
            <tr>
                <td class="grid box_desc" colspan="12">
                    <p class="justify">
                        Consumers in respect of whom a prescribed activity is undertaken can create Victorian Energy Efficiency Certificates (VEECs) under the Victorian Energy Efficiency Target Act 2007. One VEEC represents one tonne of carbon dioxide equivalent (CO2-e) to be reduced by the prescribed activity undertaken by the consumer. Consumers or their authorised signatories are able to assign their right to create VEECs to an Accredited Person. In assigning their right to an Accredited Person, the Accredited Person will be entitled to create and own certificates in respect of the prescribed activity undertaken by the consumer. In return, the Accredited Person should provide consumers with an identifiable benefit for the assignation, such as a price reduction on a product, free installation or a cash-back arrangement. Consumers should be aware that it is their responsibility to negotiate satisfactory terms with the Accredited Person in return for assigning their right to create VEECs. 
                    </p>
                </td>
            </tr>
            <tr>
                <td class="grid box_desc bold center" colspan="12">
                    <p class="center" style="color:red">
                        Yellow fields are mandatory. <br>
                        Depending on answers, some fields may change
                    </p>
                </td>
            </tr>
            <tr>
                <td class="grid bold zero box_label" colspan="12">
                    <label for="">Energy consumer details (i.e.for whom the installation was performed)</label>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="7">Was the installation carried out in a residential or commercial premises?</td>
                <td class="grid" colspan="4"><p class="border">&nbsp;<?php echo (isset($AE16) && $AE16 == '0') ? '&nbsp;' : $AE16; ?></p></td>
                <td class="grid">&nbsp;</p></td>
            </tr>
            <?php if ($AE16 == 'Residential'):?>
            <tr class="box_desc">
                <td class="grid" colspan="8">Is the lighting system owned by a business for business use or an individual for private use?</td>
                <td class="grid" colspan="3"><p class="border">&nbsp;<?php echo (isset($AE18) && $AE18 == '0') ? '&nbsp;' : $AE18; ?></p></td>
                <td class="grid">&nbsp;</p></td>
            </tr>
            <?php else:?>
            &nbsp;
            <?php endif; ?>

            <tr class="box_desc">
                <td class="grid" colspan="4">
                    <p class="">&nbsp;<?php echo $input_C21 ?></p>
                    <p class="border">&nbsp;<?php echo $C22 ?></p>
                </td>
                <td class="grid" colspan="4">
                    <label for="">Last Name</label>
                    <p class="border">&nbsp;<?php echo $C23 ?></p>
                </td>

                <?php if($AE16 == 'Residential') : ?>
                <td class="grid" colspan="4">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                </td>
                <?php else: ?>
                <td class="grid" colspan="4">
                    <p class="">&nbsp;<?php echo $input_Y21 ?></p>
                    <p class="border">&nbsp;<?php echo $Y22 ?></p>
                </td>
                <?php endif; ?>

            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="3">
                    <p class="">Tenant/Landlord/Owner?</p>
                    <p class="border">&nbsp;<?php echo $C25 ?></p>
                </td>
                <?php if($AE16 == 'Residential') : ?>
                <td class="grid" colspan="3">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                </td>
                <?php else: ?>
                <td class="grid" colspan="3">
                    <p class="">&nbsp;<?php echo $input_M24 ?></p>
                    <p class="border">&nbsp;<?php echo $M25 ?></p>
                </td>
                <?php endif; ?>

                <?php if($AE16 == 'Residential') : ?>
                <td class="grid" colspan="2">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                </td>
                <?php else: ?>
                <td class="grid" colspan="2">
                    <p class="">&nbsp;<?php echo $input_Y24 ?></p>
                    <p class="border">&nbsp;<?php echo $Y25 ?></p>
                </td>
                <?php endif; ?>

                <?php if($AE16 == 'Residential') : ?>
                <td class="grid" colspan="4">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                </td>
                <?php else: ?>
                <td class="grid" colspan="4">
                    <p class="">&nbsp;<?php echo $input_AI24 ?></p>
                    <p class="border">&nbsp;<?php echo $AI25 ?></p>
                </td>
                <?php endif; ?>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="4">
                    <p class="">Phone</p>
                    <p class="border">&nbsp;<?php echo $C28 ?></p>
                </td>
                <td class="grid" colspan="8">
                    <p class="">Postal address (if different from installation address below)</p>
                    <p class="border">&nbsp;<?php echo $R28 ?></p>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="4">
                    <p class="">Email</p>
                    <p class="border">&nbsp;<?php echo $C31?></p>
                </td>
                <?php if($AE16 == 'Residential') : ?>
                <td class="grid" colspan="6">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                </td>
                <td class="grid" colspan="2">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                </td>
                <?php else: ?>
                <td class="grid" colspan="6">
                    <p class="">&nbsp;<?php echo $input_Y30 ?></p>
                    <p class="border">&nbsp;<?php echo $Y31 ?></p>
                </td>
                <td class="grid" colspan="2">
                    <p class="">&nbsp;</p>
                    <p class="border">&nbsp;<?php echo $AS31?></p>
                </td>
                <?php endif; ?>
                
            </tr>
            <tr>
                <td class="grid bold zero box_label" colspan="12">
                    <label for="">&nbsp;<?php echo $input_O33 ?></label>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="2">
                    <p class="">Unit type </p>
                    <p class="border">&nbsp;<?php echo $C36?></p>
                </td>
                <td class="grid" colspan="2">
                    <p class="">Unit number</p>
                    <p class="border">&nbsp;<?php echo $I36?></p>
                </td>
                <td class="grid" colspan="2">
                    <p class="">Street number</p>
                    <p class="border">&nbsp;<?php echo $P36?></p>
                </td>
                <td class="grid" colspan="4">
                    <p class="">Street name</p>
                    <p class="border">&nbsp;<?php echo $W36?></p>
                </td>
                <td class="grid" colspan="2">
                    <p class="">Street type</p>
                    <p class="border">&nbsp;<?php echo $AO36?></p>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="4">
                    <p class="">Town/suburb</p>
                    <p class="border">&nbsp;<?php echo $C39?></p>
                </td>
                <td class="grid" colspan="2">
                    <p class="">State</p>
                    <p class="border">&nbsp;<?php echo $U39?></p>
                </td>
                <td class="grid" colspan="2">
                    <p class="">Postcode</p>
                    <p class="border">&nbsp;<?php echo $Z39?></p>
                </td>
                <?php if($AE16 == 'Residential') : ?>
                <td class="grid" colspan="2">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                </td>
                <?php else: ?>
                <td class="grid" colspan="2">
                    <p class="">&nbsp;<?php echo $input_AE38 ?></p>
                    <p class="border">&nbsp;<?php echo $AE39 ?></p>
                </td>
                <td class="grid" colspan="2">
                    <p class="">&nbsp;<?php echo $input_AM38 ?></p>
                    <p class="border">&nbsp;<?php echo $AM39 ?></p>
                </td>
                <?php endif; ?>
                
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="7">
                    <p class="">Has there previously been a VEEC lighting installation done in the past at this address?</p>
                </td>
                <td class="grid" colspan="5">
                    <p class="">&nbsp;</p>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="2">
                    <p class="border">&nbsp;<?php echo $C42?></p>
                </td>
                <td class="grid" colspan="10">
                    <p class="">&nbsp;</p>
                </td>
            </tr>
            <tr class="box_desc">
                <?php if($C42 == 'Yes') : ?>
                <td class="grid" colspan="4">
                    <p class="">Please give reason for additional visit</p>
                </td>
                <td class="grid" colspan="8">
                    <p class="border">&nbsp;<?php echo $L43 ?></p>
                </td>
                <?php else: ?>
                <td class="grid" colspan="12">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                </td>
                <?php endif; ?>
            </tr>
            <tr>
                <td class="grid bold zero box_label" colspan="12">
                    <label for="">VEEC benefit calculation</label>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="2">
                    <p class="">Activity 21C</p>
                    <p class="">Number of VEECs</p>
                    <p class="border">&nbsp;<?php echo $C51?></p>
                    <p class="">Activity 21D</p>
                    <p class="">Number of VEECs</p>
                    <p class="border">&nbsp;<?php echo $C55?></p>
                </td>
                <td class="grid center" colspan="1">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                    <p class="bold center">X</p>
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                    <p class="bold center">X</p>
                </td>
                <td class="grid" colspan="2">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                    <p class="border">&nbsp;<?php echo $M51?></p>
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                    <p class="border">&nbsp;<?php echo $M55?></p>
                </td>
                <td class="grid center" colspan="1">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                    <p class="bold center">=</p>
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                    <p class="bold center">=</p>
                </td>
                <td class="grid" colspan="2">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                    <p class="border">&nbsp;<?php echo $W51?></p>
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                    <p class="border">&nbsp;<?php echo $W55?></p>
                </td>
                <td class="grid center" colspan="1">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                    <p class="bold center">=</p>
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                </td>
                <td class="grid" colspan="3">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                    <p class="">Total amount payable:</p>
                    <p class="border bold">&nbsp;<?php echo $AI53?></p>
                    <p class="">excl GST</p>
                    <p class="">&nbsp;</p>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="text-justify">Notes :</p>
                      <p class="text-justify">
                        (i). Certificate prices can fluctuate and are valid on the date this assignment from is received at Green Energy Trading, subject to installed product approval. <br>
                        (ii). VEECs can only be claimed within 6 months after the end of the year in which the activity was completed. GET will only accept form if they are received complete and correct, one month prior to this date
                      </p>
                </td>
            </tr>
           
            <div class="footer">
                <p>&copy; <small>Green Energy Trading Pty Ltd 2014</small></p>
            </div>
            
        </table>
    </div>
    <div class="pagebreak"></div>
    <!-- close page 1-->
    
    <!--page 2-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid">&nbsp;</td>
                <td class="grid" colspan="2"><img src="<?= $logo ?>" alt="" style="width:160px"></td>
                <td class="grid center zero" colspan="5" style="vertical-align:top">
                    <h2 class=""><strong>VEEC 21</h2>
                    <h3 style="vertical-align:bottom"> 12V Halogen downlight replacement</h3>
                </td>
                <td class="grid left" colspan="3"><img src="asset/css/images/logo.png" alt="" style="width:130px"></td>
            </tr>
            <tr>
                <td class="grid bold center zero" colspan="12">
                    <h4 class="zero" style="font-weight:normal">&nbsp;<?php echo $input_A3 ?></h4>
                </td>
            </tr>
            <tr>
                <td class="grid bold zero box_label" colspan="12">
                    <label for="">Installation details</label>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="4">
                    <label for="">Installation date</label>
                    <p class="border">&nbsp;<?php echo $C68 ?></p>
                </td>
                <td class="grid" colspan="8">
                    <p class="">&nbsp;<?php echo $O67 ?></p>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <label for="">Please complete the following table with details of the removed and installed lighting</label>
                </td>
            </tr>
            <tr class="box_desc">
                <td class=""></td>
                <td class=" border center skyblue" colspan="4">
                    <label for="">What has been removed</label>
                </td>
                <td class=" border center warm" colspan="7">
                    <label for="">What has been installed</label>
                </td>
                
            </tr>
            <tr class="box_desc">
                <td class=" " style="width:50px"></td>
                <td class=" border center skyblue" style="width:125px">
                    <label for="">Select existing product</label>
                </td>
                <td class=" border skyblue center"><label for="">Transformer type</label></td>
                <td class=" border skyblue center" style="width:50px"><label for=""># of lamps</label></td>
                <td class=" border skyblue center"><label for="">Activity</label></td>
                <td class=" border warm center" colspan="2" style="width:150px"><label for="">Lamp brand and model number</label></td>
                <td class=" border warm center" style="width:50px"><label for=""># of lamps</label></td>
                <td class=" border warm center" style="width:50px"><label for="">Compatible with existing transfomer</label></td>
                <td class=" border warm center"><label for="">Output</label></td>
                <td class=" border warm center"><label for="">Efficacy</label></td>
                <td class=" border warm center" style="width:50px"><label for="">Lifetime</label></td>
            </tr>
            <tr class="box_desc">
                <td class=" center"><label for="">1.</label></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $C74 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $L74 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $O74 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $R74 ?></p></td>
                <?php if($C74 == '12V Halogen lamp only') : ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T74 ?></p></td>
                <?php elseif($C74 == '12V Halogen lamp & transfomer'): ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T73 ?></p></td>
                <?php else: ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;</p></td>
                <?php endif; ?>
                
                <td class=" border center"><p class="small">&nbsp;<?php echo $AI74 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AL74 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AN74 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AP74 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AS74 ?></p></td>
            </tr>
            <tr class="box_desc">
                <td class=" center"><label for="">2.</label></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $C76 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $L76 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $O76 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $R76 ?></p></td>
                <?php if($C76 == '12V Halogen lamp only') : ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T76 ?></p></td>
                <?php elseif($C76 == '12V Halogen lamp & transfomer'): ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T75 ?></p></td>
                <?php else: ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;</td>
                <?php endif; ?>
                
                <td class=" border center"><p class="small">&nbsp;<?php echo $AI76 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AL76 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AN76 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AP76 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AS76 ?></p></td>
            </tr>
            <tr class="box_desc">
                <td class=" center"><label for="">3.</label></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $C78 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $L78 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $O78 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $R78 ?></p></td>
                <?php if($C78 == '12V Halogen lamp only') : ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T78 ?></p></td>
                <?php elseif($C78 == '12V Halogen lamp & transfomer'): ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T77 ?></p></td>
                <?php else: ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;</td>
                <?php endif; ?>
                
                <td class=" border center"><p class="small">&nbsp;<?php echo $AI78 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AL78 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AN78 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AP78 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AS78 ?></p></td>
            </tr>
            <tr class="box_desc">
                <td class=" center"><label for="">4.</label></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $C80 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $L80 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $O80 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $R80 ?></p></td>
                <?php if($C80 == '12V Halogen lamp only') : ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T80 ?></p></td>
                <?php elseif($C80 == '12V Halogen lamp & transfomer'): ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T79 ?></p></td>
                <?php else: ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;</td>
                <?php endif; ?>
                
                <td class=" border center"><p class="small">&nbsp;<?php echo $AI80 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AL80 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AN80 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AP80 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AS80 ?></p></td>
            </tr>
            <!--
            <tr class="box_desc">
                <td class=" center"><label for="">5.</label></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $C82 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $L82 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $O82 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $R82 ?></p></td>
                <?php if($C82 == '12V Halogen lamp only') : ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T82 ?></p></td>
                <?php elseif($C82 == '12V Halogen lamp & transfomer'): ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T81 ?></p></td>
                <?php else: ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;</td>
                <?php endif; ?>
                
                <td class=" border center"><p class="small">&nbsp;<?php echo $AI82 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AL82 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AN82 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AP82 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AS82 ?></p></td>
            </tr>
            <tr class="box_desc">
                <td class=" center"><label for="">6.</label></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $C84 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $L84 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $O84 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $R84 ?></p></td>
                <?php if($C84 == '12V Halogen lamp only') : ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T84 ?></p></td>
                <?php elseif($C84 == '12V Halogen lamp & transfomer'): ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T83 ?></p></td>
                <?php else: ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;</td>
                <?php endif; ?>
                
                <td class=" border center"><p class="small">&nbsp;<?php echo $AI84 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AL84 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AN84 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AP84 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AS84 ?></p></td>
            </tr>
            <tr class="box_desc">
                <td class=" center"><label for="">7.</label></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $C86 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $L86 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $O86 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $R86 ?></p></td>
                <?php if($C86 == '12V Halogen lamp only') : ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T86 ?></p></td>
                <?php elseif($C86 == '12V Halogen lamp & transfomer'): ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T85 ?></p></td>
                <?php else: ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;</td>
                <?php endif; ?>
                
                <td class=" border center"><p class="small">&nbsp;<?php echo $AI86 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AL86 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AN86 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AP86 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AS86 ?></p></td>
            </tr>
            <tr class="box_desc">
                <td class=" center"><label for="">8.</label></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $C88 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $L88 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $O88 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $R88 ?></p></td>
                <?php if($C88 == '12V Halogen lamp only') : ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T88 ?></p></td>
                <?php elseif($C88 == '12V Halogen lamp & transfomer'): ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T87 ?></p></td>
                <?php else: ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;</td>
                <?php endif; ?>
                
                <td class=" border center"><p class="small">&nbsp;<?php echo $AI88 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AL88 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AN88 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AP88 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AS88 ?></p></td>
            </tr>
            <tr class="box_desc">
                <td class=" center"><label for="">9.</label></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $C90 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $L90 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $O90 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $R90 ?></p></td>
                <?php if($C90 == '12V Halogen lamp only') : ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T90 ?></p></td>
                <?php elseif($C90 == '12V Halogen lamp & transfomer'): ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T89 ?></p></td>
                <?php else: ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;</td>
                <?php endif; ?>
                
                <td class=" border center"><p class="small">&nbsp;<?php echo $AI90 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AL90 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AN90 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AP90 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AS90 ?></p></td>
            </tr>
            <tr class="box_desc">
                <td class=" center"><label for="">10.</label></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $C92 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $L92 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $O92 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $R92 ?></p></td>
                <?php if($C92 == '12V Halogen lamp only') : ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T92 ?></p></td>
                <?php elseif($C92 == '12V Halogen lamp & transfomer'): ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;<?php echo $T91 ?></p></td>
                <?php else: ?>
                <td class=" border center" colspan="2"><p class="small">&nbsp;</td>
                <?php endif; ?>
                
                <td class=" border center"><p class="small">&nbsp;<?php echo $AI92 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AL92 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AN92 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AP92 ?></p></td>
                <td class=" border center"><p class="small">&nbsp;<?php echo $AS92 ?></p></td>
            </tr>
            -->
            <tr class="box_desc">
                <td class=" center">&nbsp;</td>
                <td class=" center" colspan="6"><p class="">Maximum of 5 different energy efficient lighting products can be installed for each activity</td>
                <td class=" center" colspan="3"><p class="">Number of VEECs for 21C</td>
                <td class=" border center" colspan="2"><p class="">&nbsp;<?php echo $AS94 ?></p></td>
            </tr>
            <tr class="box_desc">
                <td class=" center">&nbsp;</td>
                <td class=" center" colspan="6">&nbsp;</td>
                <td class=" center" colspan="3"><p class="">Number of VEECs for 21D</td>
                <td class=" border center" colspan="2"><p class="">&nbsp;<?php echo $AS96 ?></p></td>
            </tr>
            <?php if($R74="21D" || $R76="21D" || $R78="21D" || $R80="21D" || $R82="21D" || $R84="21D" || $R86="21D" || $R88="21D" || $R90="21D" || $R92="21D") : ?>
            <tr class="box_desc tops">
                <td class=" center" colspan="12"><p class="">* Schedule 21D activities require replacement of a 12 volt fitting. The (pre-existing) halogen transformer(s) must not be used as part of the new fitting installation.</p></td>
            </tr>
            <?php else: ?>
            &nbsp;
            <?php endif; ?>

            <tr>
                <td class="grid bold zero box_label" colspan="12">
                    <label for="">Pre-installation inspection completed by electrician</label>
                </td>
            </tr>
            <tr class="box_desc">
                <td class=" center tops" colspan="12"><p class="">I have performed the pre-installation assessment and physically inspected the existing equipment:</p></td>
            </tr>
            <tr class="box_desc">
                <td class="">&nbsp;</td>
                <td class="" colspan="11">
                    <img src="asset/images/<?php echo (isset($D101) && $D101 == '1') ? '1' : '0' ?>.gif" alt=""> &nbsp; Any safety hazards identified have been resolved and the installation meets all relevant standards
                </td>
            </tr>
            <tr class="box_desc">
                <td class="">&nbsp;</td>
                <td class="" colspan="11">
                    <img src="asset/images/<?php echo (isset($D102) && $D102 == '1') ? '1' : '0' ?>.gif" alt=""> &nbsp; The existing transformers are compatible with the new lamps to which they will be connected 
                </td>
            </tr>
            <tr class="box_desc">
                <td class="">&nbsp;</td>
                <td class=" center" colspan="11"><p class="">Please note, where safety and compatibility requirements are not met the installation is not eligible for VEEC creation.</p></td>
            </tr>
            <tr class="box_desc">
                <td class="">&nbsp;</td>
                <td class=" center" colspan="11"><p class="">Give the brand and model number of the existing transformers:</p></td>
            </tr>
            <tr class="box_desc">
                <td class=" center">&nbsp;</td>
                <td class=" border skyblue" colspan="3"><p class="center">Brand</p></td>
                <td class=" border skyblue" colspan="3"><p class="center">Model</p></td>
                <td class=" border skyblue" colspan="3"><p class="center">Transformer type</p></td>
                <td class=" center" colspan="2">&nbsp;</td>
            </tr>
            <tr class="box_desc">
                <td class=" center">&nbsp;</td>
                <td class=" center" colspan="3"><p class="border">&nbsp;<?php echo $D108 ?></p></td>
                <td class=" center" colspan="3"><p class="border">&nbsp;<?php echo $R108 ?></p></td>
                <td class=" center" colspan="3"><p class="border">&nbsp;<?php echo $AI108 ?></p></td>
                <td class=" center" colspan="2">&nbsp;</td>
            </tr>
            <tr class="box_desc">
                <td class=" center">&nbsp;</td>
                <td class=" center" colspan="3"><p class="border">&nbsp;<?php echo $D110 ?></p></td>
                <td class=" center" colspan="3"><p class="border">&nbsp;<?php echo $R110 ?></p></td>
                <td class=" center" colspan="3"><p class="border">&nbsp;<?php echo $AI110 ?></p></td>
                <td class=" center" colspan="2">&nbsp;</td>
            </tr>
            <tr class="box_desc">
                <td class=" center">&nbsp;</td>
                <td class=" center" colspan="3"><p class="border">&nbsp;<?php echo $D112 ?></p></td>
                <td class=" center" colspan="3"><p class="border">&nbsp;<?php echo $R112 ?></p></td>
                <td class=" center" colspan="3"><p class="border">&nbsp;<?php echo $AI112 ?></p></td>
                <td class=" center" colspan="2">&nbsp;</td>
            </tr>
            <tr class="box_desc">
                <td class="" colspan="4"><p class="">Were any lamps installed on dimmable circuits?</p></td>
                <td class="" colspan="2"><p class="border">&nbsp;<?php echo $V115 ?></p></td>
                <?php if($V115 == 'Yes') : ?>
                <td class=" center" colspan="3">
                    <p class="">If yes, how many ?</p>
                </td>
                <td class=" center" colspan="3">
                    <p class="border">&nbsp;<?php echo $AJ115 ?></p>
                </td>
                <?php else: ?>
                <td class=" center" colspan="6"><p class="">&nbsp;</p></td>
                <?php endif; ?>
            </tr>
            <tr class="box_desc">
                <td class="" colspan="4"><p class="">Was wiring work required as part of this upgrade?</p></td>
                <td class="" colspan="2"><p class="border">&nbsp;<?php echo $V117 ?></p></td>
                <?php if($V117 == 'Yes') : ?>
                <td class=" center" colspan="3">
                    <p class="">Electrical Safety Certificate number:</p>
                </td>
                <td class=" center" colspan="3">
                    <p class="border">&nbsp;<?php echo $AM117 ?></p>
                </td>
                <?php else: ?>
                <td class=" center" colspan="6"><p class="">&nbsp;</p></td>
                <?php endif; ?>
            </tr>
            <tr class="box_desc">
                <td class="" colspan="8"><p class="">Were there any 240V halogen downlights replaced for the purposes of the VEET during this installation?</p></td>
                <td class="" colspan="3"><p class="border">&nbsp;<?php echo $V115 ?></p></td>
                <td class="" colspan="1"><p class="">&nbsp;</p></td>
            </tr>
            <tr class="box_desc">
                <?php if($AO119 == 'Yes') : ?>
                <td class="center" colspan="12">
                    <p class="" style="color:red">240V products are not eligible to be replaced under this activity</p>
                </td>
                <?php else: ?>
                <td class="center" colspan="12">
                    &nbsp;
                </td>
                
                <?php endif; ?>
            </tr>
            <tr>
                <td class="bold box_label" colspan="12">
                    <label for="">Electrician details</label>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="" colspan="5"><p class="">Electrician's company name</p></td>
                <td class="" colspan="7"><p class="">Company address</p></td>
            </tr>
            <tr class="box_desc">
                <td class="" colspan="5"><p class="border">&nbsp;<?php echo $C124 ?></p></td>
                <td class="" colspan="7"><p class="border">&nbsp;<?php echo $U124 ?></p></td>
            </tr>
            <tr class="box_desc">
                <td class="" colspan="3"><p class="">Electrician's name</p></td>
                <td class="" colspan="3"><p class="">Phone number</p></td>
                <td class="" colspan="6"><p class="">Email</p></td>
            </tr>
            <tr class="box_desc">
                <td class="" colspan="3"><p class="border">&nbsp;<?php echo $C127 ?></p></td>
                <td class="" colspan="3"><p class="border">&nbsp;<?php echo $O127 ?></p></td>
                <td class="" colspan="6"><p class="border">&nbsp;<?php echo $Z127 ?></p></td>
            </tr>
            <tr class="box_desc">
                <td class="" colspan="3"><p class="">Licence type</p></td>
                <td class="" colspan="3"><p class="">Licence number</p></td>
                <td class="" colspan="3"><p class="">Issue/renewal date</p></td>
                <td class="" colspan="3"><p class="">Date of expiry</p></td>
            </tr>
            <tr class="box_desc">
                <td class="" colspan="3"><p class="border">&nbsp;<?php echo $C130 ?></p></td>
                <td class="" colspan="3"><p class="border">&nbsp;<?php echo $K130 ?></p></td>
                <td class="" colspan="3"><p class="border">&nbsp;<?php echo $V130 ?></p></td>
                <td class="" colspan="3"><p class="border">&nbsp;<?php echo $AI130 ?></p></td>
            </tr>
           
            <div class="footer">
                <p>&copy; <small>Green Energy Trading Pty Ltd 2014</small></p>
            </div>
            
        </table>
    </div>
    <div class="pagebreak"></div>
    <!-- close page 2-->

    <!--page 3-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid" colspan="3"><img src="<?= $logo ?>" alt="" style="width:160px"></td>
                <td class="grid center zero" colspan="6" style="vertical-align:top">
                    <h2 class=""><strong>VEEC 21</h2>
                    <h3 style="vertical-align:bottom"> 12V Halogen downlight replacement</h3>
                </td>
                <td class="grid left" colspan="3"><img src="asset/css/images/logo.png" alt="" style="width:130px"></td>
            </tr>
            <tr>
                <td class="grid bold center zero" colspan="12">
                    <h4 class="zero" style="font-weight:normal">&nbsp;<?php echo $input_A3 ?></h4>
                </td>
            </tr>
            <tr>
                <td class="grid bold zero box_label" colspan="12">
                    <label for="">Declaration by electrician</label>
                </td>
            </tr>
            <?php if($C127 == '') : ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="">&nbsp;I, ______________________________, hereby declare that:</p>
                </td>
            </tr>
            <?php else: ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="">&nbsp;<?php echo 'I, ' .$C127. ' , hereby declare that:' ?></p>
                </td>
            </tr>
            <?php endif; ?>

            <?php if($AE16 == 'Residential') : ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    &nbsp;
                </td>
            </tr>
            <?php else: ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="">*    I confirm that the above installation was not undertaken in a premises compulsorily registered on the EREPs register      under the Environment and Resource Efficiency Plans Program administered by the Environment Protection Authority.</p>
                </td>
            </tr>
            <?php endif; ?>

            <?php if($AE16 == 'Residential') : ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="">*    The above product(s) have been installed in a residential premises.</p>
                </td>
            </tr>
            <?php else: ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="">*    The above product(s) have been installed at the stated premises.</p>
                </td>
            </tr>
            <?php endif; ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="">*  Where applicable, the consumer has been informed that a Certificate of Electrical Safety is required for the work undertaken and will be provided a copy of the relevant certificate within five working days of installation.</p>
                    <p class="">*  The lamps which were removed from the premises will be decommissioned through recycling.</p>
                    <p class="">*  Where any product was installed in a dimmable circuit, the product is approved by the manufacturer as suitable for such a circuit.</p>
                    <p class="">*  No damage, fault or hazard was identified during the mandatory compatibility & safety inspection, and I determined that it was safe to proceed with the installation.</p>
                    <p class="">*  I am a licensed electrician and the installation meets all mandatory industry standards.</p>
                    <p class="">*  In the case of Schedule 21C, lamps installed are compatible with pre-existing transformers.</p>
                    <p class="">*  The information provided is complete and accurate and I am aware that penalties can be applied for providing misleading information in this form under the Victorian Energy Efficiency Target Act 2007.</p>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid">&nbsp;</td>
                <td class="grid" colspan="3">Signature</td> 
                <td class="grid">&nbsp;</td>
                <td class="grid" colspan="3">&nbsp;</td>
                <td class="grid">&nbsp;</td>
                <td class="grid" colspan="2">Date</td>
                <td class="grid">&nbsp;</td>

            </tr>
            <tr class="box_desc">
                <td class="grid center" colspan="3" style="padding-top:5px;"><img src="<?php echo $E153 ?>" style="height: 60px;" /></td>                
                <td class="grid center" style="padding-top:5px;">&nbsp;</td>
                <td class="grid center" colspan="3" style="padding-top:5px;">
                    &nbsp;
                </td>
                <td class="grid center" colspan="2" style="padding-top:5px;">&nbsp;</td>
                <td class="grid " colspan="2" style="padding-top:5px;"><p class=""><?php echo $Z153 ?> </p></td> 
                <td class="grid center" >&nbsp;</td>               
            </tr>
            <tr class="box_desc">
                <td class="" colspan="4"><p class="">Name</p></td>
                <td class="" colspan="4"><p class="">Company name</p></td>
                <td class="" colspan="4"><p class="">Phone number</p></td>
            </tr>
            <tr class="box_desc">
                <td class="" colspan="4"><p class="border">&nbsp;<?php echo $C156 ?></p></td>
                <td class="" colspan="4"><p class="border">&nbsp;<?php echo $R156 ?></p></td>
                <td class="" colspan="4"><p class="border">&nbsp;<?php echo $AI156 ?></p></td>
            </tr>
            <!--
            <tr >
                <td class="grid bold zero box_label" colspan="12">
                    <label for="" class="tops">Document checklist</label>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="12">Please attach the following required documentation upon submission of this form:</td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="6">
                    &nbsp;<img src="asset/images/<?php echo (isset($D160) && $D160 == '1') ? '1' : '0' ?>.gif" alt=""> &nbsp; 
                    A1. Completed and signed VEEC Assignment Form <br>
                    <p class="justify">Ensure all yellow fields have been completed.</p> <br>
                    &nbsp;<img src="asset/images/<?php echo (isset($D163) && $D163 == '1') ? '1' : '0' ?>.gif" alt=""> &nbsp; 
                    A2. Proof of purchase (customer's tax invoice) This must detail : <br>
                    <ul>
                        <li>Energy consumer's name and installation address</li>
                        <li>Model and quantity of each lighting product supplied</li>
                        <li>Value of any VEEC benefit provided</li>
                    </ul>
                    &nbsp;<img src="asset/images/<?php echo (isset($D168) && $D168 == '1') ? '1' : '0' ?>.gif" alt=""> &nbsp; 
                    A3. Itemised recycling receipt <br>
                    <p class="justify">A receipt issued by a recycler or collect responsible for the disposal of the original lamps. The receipt must show the installation address and the type and quantity of each removed product. A recycling receipt showing only "weight" is not accepted.</p> <br>
                    &nbsp;<img src="asset/images/<?php echo (isset($D174) && $D174 == '1') ? '1' : '0' ?>.gif" alt=""> &nbsp; 
                    A4. Energy consumer's electricity bill This must clearly name the energy consumer as the party responsible for paying the bill. <br>

                </td>
                <td class="grid" colspan="6">
                    &nbsp;<img src="asset/images/<?php echo (isset($AB160) && $AB160 == '1') ? '1' : '0' ?>.gif" alt=""> &nbsp; 
                    A5. Installation photographs <br>
                    <p>&nbsp;</p>
                    <ul>
                        <li>One of each type of halogen lamp, showing wattage</li>
                        <li>One of each type of existing transformer, showing model</li>
                        <li>One of each type of installed energy efficient lamp, showing model</li>
                        <li>One of each type of installed LED driver (for Activity 21D)</li>
                        <li>Photo(s) of the space where lighting was upgraded</li>
                        <li>Removed lamps laid out in a grid, showing quantity</li>
                    </ul>
                    &nbsp;<img src="asset/images/<?php echo (isset($AB167) && $AB167 == '1') ? '1' : '0' ?>.gif" alt=""> &nbsp; 
                    A6. Certificate of Electrical Safety - not required <br> <br>
                    &nbsp;<img src="asset/images/<?php echo (isset($AB172) && $AB172 == '1') ? '1' : '0' ?>.gif" alt=""> &nbsp; 
                    A7. Tax invoice to Green Energy Trading - not required
                </td>
            </tr>
            -->
            <tr>
                <td class="grid bold zero box_label" colspan="12">
                    <label for="">How to submit this form</label>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid center bold" colspan="12">Please email the completed VEEC 21 Assigment Form and accompanying required documentation to :</td>
            </tr>
            <tr class="box_desc">
                <td class="grid center bold" style="color:blue" colspan="12">forms@greenenegytrading.com.au</td>
            </tr>
            <tr class="box_desc">
                <td class="grid center bold" style="color:green" colspan="12">
                    <p class="center">With all enquiries:</p>
                    <h1>1300 077 784</h1>
                </td>
            </tr>


            <div class="footer">
                <p>&copy; <small>Green Energy Trading Pty Ltd 2014</small></p>
            </div>
        </table>
    </div>
    <div class="pagebreak"></div>
    <!-- close page 3-->

    <!--page 4-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid" colspan="3"><img src="<?= $logo ?>" alt="" style="width:160px"></td>
                <td class="grid center zero" colspan="6" style="vertical-align:top">
                    <h2 class=""><strong>VEEC 21</h2>
                    <h3 style="vertical-align:bottom"> 12V Halogen downlight replacement</h3>
                </td>
                <td class="grid left" colspan="3"><img src="asset/css/images/logo.png" alt="" style="width:130px"></td>
            </tr>
            <tr>
                <td class="grid bold center zero" colspan="12">
                    <h4 class="zero" style="font-weight:normal">&nbsp;<?php echo $input_A3 ?></h4>
                </td>
            </tr>
            <tr>
                <td class="grid bold zero box_label" colspan="12">
                    <label for="">Form of benefit provided to energy consumer</label>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <label for="">The following benefit has been provided to the energy consumer:</label>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="3">
                    <img src="asset/images/<?php echo (isset($benefit) && $benefit == '1') ? '1' : '0' ?>.gif" alt=""> &nbsp; Upfront cash
                </td>
                <td class="grid" colspan="3">
                    <img src="asset/images/<?php echo (isset($benefit) && $benefit == '3') ? '1' : '0' ?>.gif" alt=""> &nbsp; Delayed cash
                </td>
                <td class="grid" colspan="3">
                    <img src="asset/images/<?php echo (isset($benefit) && $benefit == '5') ? '1' : '0' ?>.gif" alt=""> &nbsp; other (please describe)
                </td>
                <td class="grid" colspan="3">
                    <p class="border">&nbsp;<?php echo $AG190 ?>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="3">
                    <img src="asset/images/<?php echo (isset($benefit) && $benefit == '2') ? '1' : '0' ?>.gif" alt=""> &nbsp; Price reduction
                </td>
                <td class="grid" colspan="3">
                    <img src="asset/images/<?php echo (isset($benefit) && $benefit == '4') ? '1' : '0' ?>.gif" alt=""> &nbsp; Free installation 
                </td>
                <td class="grid" colspan="3">
                    <p>$ amount of benefit provided :</p>
                </td>
                <td class="grid" colspan="3">
                    <p class="border">&nbsp;<?php echo $AJ192 ?>
                </td>
            </tr>
            <tr>
                <td class="grid bold zero box_label" colspan="12">
                    <label for="">Payment details</label>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="6">
                    <p class="">Make payment to: </p>
                    <p class="border">&nbsp;<?php echo $C196 ?></p>
                </td>

                <?php if($C196 == 'Other - please describe arrangement in adjacent field') : ?>
                <td class="grid" colspan="6">
                    <p class="">Other arrangement:</p>
                    <p class="">&nbsp;<?php echo $AA196 ?></p>
                </td>
                <?php elseif($C196 == 'Lighting solution provider - energy consumer has received VEEC benefit'): ?>
                <td class="grid" colspan="6">
                    <p class="">Who is the lighting solution provider?</p>
                    <p class="border">&nbsp;<?php echo $AA196 ?></p>
                </td>
                <?php else : ?>
                <td class="grid" colspan="6">
                    <p class="">&nbsp;</p>
                    <p class="">&nbsp;</p>
                </td>
                <?php endif; ?>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="3">
                    <p class="">Account name</p>
                    <p class="border">&nbsp;<?php echo $C198 ?></p>
                </td>
                <td class="grid" colspan="3">
                    <p class="">BSB</p>
                    <p class="border">&nbsp;<?php echo $M198 ?></p>
                </td>
                <td class="grid" colspan="3">
                    <p class="">Account number</p>
                    <p class="border">&nbsp;<?php echo $U198 ?></p>
                </td>
                <td class="grid" colspan="3">
                    <p class="">Email address for remittance advice</p>
                    <p class="border">&nbsp;<?php echo $AC198 ?></p>
                </td>
            </tr>
            <tr>
                <td class="grid bold zero box_label" colspan="12">
                    <label for="">Declaration by energy consumer</label>
                </td>
            </tr>

            <?php if($C22 == '') : ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="">&nbsp;I, ______________________________, hereby declare that:</p>
                </td>
            </tr>
            <?php else: ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="">&nbsp;<?php echo 'I, ' .$C202. ' , hereby declare that:' ?></p>
                </td>
            </tr>
            <?php endif; ?>

            <?php if($AW19 == '0' || $AW19 == '2') : ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="justify"><?php echo '*    I am authorised to sign on behalf of ' .$Y22 ?></p>
                </td>
            </tr>
            <?php else: ?>
            &nbsp;
            <?php endif; ?>

            <?php if($AW19 == '0' || $AW19 == '2') : ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="justify"><?php echo '*    '. $Y22 .' is the' . $C25 .' of the premises at the above installation address.' ?></p>
                </td>
            </tr>
            <?php else: ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="justify"><?php echo '*    I am the '. $C25 .' of the residence at the above installation address.' ?></p>
                </td>
            </tr>
            <?php endif; ?>

            <?php if($AW19 == '0' || $AW19 == '2') : ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="justify"><?php echo '*    '. $Y22 .'has received or will receive an identifiable benefit from '.$C196.' in exchange for assigning the rights to create the VEECs for the above system.' ?></p>
                </td>
            </tr>
            <?php else: ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="justify"><?php echo '*    I have received, or will receive an identifiable benefit from '.$AA196.' in exchange for assigning my rights to create the certificates for the above installation.' ?></ps>
                </td>
            </tr>
            <?php endif; ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="">*  The replaced lamps have been removed from the premises.</p>
                    <p class="">*  The information provided by the installer on page 3 of the assignment form is correct and complete.</p>
                    <p class="">*  I understand that by signing this form I am assigning the right to create VEECs for the installation to Green Energy Trading Pty Ltd.</p>
                    <p class="">*  The Essential Services Commission has the right to inspect the installation with reasonable notice.</p>
                    <p class="">*  Green Energy Trading has the right to inspect the installation with reasonable notice and to conduct a phone audit of the installation if necessary.</p>
                    <p class="">*  I understand that information on this form will be disclosed to the Essential Services Commission for the purpose of creating VEECs under the Victorian Energy Efficiency Target Act 2007 and for related verification, audit and scheme monitoring purposes.</p>
                    <p class="">*  I understand that this form cannot be signed before the latter of decommissioning or installation date, as required by the Activity undertaken by the Installer.</p>
                    <p class="">*  I am aware that penalties can be applied for providing misleading information in this form under the Victorian Energy Efficiency Target Act 2007.</p>
                </td>
            </tr>
            <?php if($AE16 =='Commercial') : ?>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="justify"><?php echo '*    I confirm that the above installation was not undertaken in a premises compulsorily registered on the EREPs register     under the Environment and Resource Efficiency Plans Program administered by the Environment Protection Authority.'?></p>
                </td>
            </tr>
            <?php else: ?>
            &nbsp;
            <?php endif; ?>
            <tr class="box_desc">
                <td class="grid" colspan="10">
                    <p class="">I have been advised whether a Certificate of Electrical Safety is required for the work undertaken and, if one is required, that I will be provided a copy of the relevant certificate</p>
                </td>
                <td class="grid" colspan="2">
                    <p class="border center" style="">&nbsp;<?php echo $AP220 ?></p>
                </td>
               
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="10">
                    <p class="">I declare the above named electrician physically attended the premises and left a form of identification at the completion of the upgrade.</p>
                </td>
                <td class="grid" colspan="2" >
                    <p class="border center" style="">&nbsp;<?php echo $AP222 ?></p>
                </td>
            </tr>

            <?php if(isset($installerphoto2) && file_exists($installerphoto2)) : ?>
            <tr class="box_desc">
                <td class="grid">&nbsp;</td>
                <td class="grid" colspan="5">
                  <img style="width:3cm; height:4cm" src="<?= $installerphoto2 ?>" class="text-center">
                  <br> <p>This photo depicts the installer who undertook the replacement of halogen down lights in the above address.</p>
                </td>
            </tr>
            <?php endif; ?>

            <tr class="box_desc">
                <td class="grid">&nbsp;</td>
                <td class="grid" colspan="5">Signature</td>                
                <td class="grid">&nbsp;</td>
                <td class="grid" colspan="5">Date</td>                
            </tr>
            <tr class="box_desc">
                <td class="grid center" colspan="5" style="padding-top:5px;"><img src="<?php echo $E225 ?>" style="height: 60px;" /></td>                
                <td class="grid" colspan="5" style="padding-top:5px;"><p class="center"><?php echo $Z225 ?> </p></td>                
                <td class="grid center" style="padding-top:5px;" colspan="2">&nbsp;</td>
            </tr>
            
            <tr class="box_desc">
                <td class="grid" colspan="4">
                    <p class=""><?php echo ($AW19 == '0' || $AW19 == '2') ? 'Authorised signatory name' : 'Energy consumers name'; ?></p>
                    <p class="border">&nbsp;<?php echo $C228 ?></p>
                </td>
                
                <td class="grid" colspan="4">
                    <p class=""><?php echo ($AW19 == '0' || $AW19 == '2') ? 'Position' : '&nbsp;'; ?></p>
                    <?php if($AW19 == '0' || $AW19 == '2') : ?>
                    <p class="border">&nbsp;<?php echo $V228 ?></p>
                    <?php else: ?>
                    &nbsp;
                    <?php endif; ?>
                </td>
                <td class="grid" colspan="4">
                    <p class="">Phone number</p>
                    <p class="border">&nbsp;<?php echo $AI228 ?></p>
                </td>
            </tr>
            <tr>
                <td class="grid bold zero box_label" colspan="12">
                    <label for="">GST implications of this assignment</label>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="">For the purposes of the Goods and Services Tax (GST), this transaction is characterised as the supply of the right to create Environmental Certificates from the energy consumer to Green Energy Trading. </p>
                    <p class="">Where the energy consumer is registered for GST and has acquired the lighting products for business purposes, the supply will attract GST, and the Parties agree that the energy consumer will issue a tax invoice to Green Energy Trading in respect of this supply.</p>
                </td>
                
            </tr>
            <div class="footer">
                <p>&copy; <small>Green Energy Trading Pty Ltd 2014</small></p>
            </div>
            <div class="pagebreak"></div>
            
        </table>
    </div>
    <!-- close page 4-->

    <!--page 5-->
    
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid" colspan="6"><img src="<?= $logo ?>" alt="" style="width:160px"></td>
                
                <td class="grid left" colspan="6">
                    <!--<img src="asset/css/images/tax.png" alt="" style="width:180px">-->
                    <h1 class="right bold" style="margin-bottom:0px">Proof of Purchase</h1>
                </td>
            </tr>
            <tr>
                <td class="grid" colspan="6">
                    <p class="label bold">&nbsp;<?php echo isset($SP1) ? $SP1 : 'Eminent Service Group PL T/A My Electrician' ?></p>
                    <p class="label bold">&nbsp;<?php echo isset($SP2) ? $SP2 : 'PO Box 332, Webb Street ' ?></p>
                    <p class="label bold">&nbsp;<?php echo (isset($SP4) && isset($SP4)) ? $SP3.' '.$SP4 : 'Narre Warren 3805' ?></p>
                </td>
                <td class="grid right" colspan="4">
                    <p class="label right bold">Invoice No. :</p>
                    <p class="label right bold">Date :</p>
                    <p class="label right bold">Term :</p>
                </td>
                <td class="grid" colspan="2">
                    <p class="label bold">&nbsp;<?php echo $H4 ?></p>
                    <p class="label bold">&nbsp;<?php echo $ZL6 ?></p>
                    <p class="label bold">14 Days</p>
                </td>
            </tr>
            <tr>
                <td class="grid" colspan="12">
                    <!--<p class="label bold">&nbsp;<?php echo $SP4 ?></p>-->
                    <p class="label bold">&nbsp;<?php echo isset($SP5) ? $SP5 : '' ?></p>
                    <p class="label bold">&nbsp;<?php echo isset($SP6) ? $SP6 : '95 149 996 958' ?></p>
                    <p class="label bold">&nbsp;<?php echo isset($SP7) ? $SP7 : '1300 660 042' ?></p>
                    <p class="label bold">&nbsp;<?php echo isset($SP8) ? $SP8 : 'www.myelec.net.au' ?></p>
                    <p class="label bold">&nbsp;<?php echo isset($SP9) ? $SP9 : 'reception@myelec.net.au' ?></p>
                    <p class="label bold">Installing Electrician :&nbsp;<?php echo $C127 ?></p>
                </td>
            </tr>
            <tr>
                <td class="grid border-bottom" colspan="12">&nbsp;</td>
            </tr>
           
            <tr>
                <td class="grid" style="vertical-align:top;"><p class="label grey bold tops" style="vertical-align:top;">Bill To :</p></td>
                <td class="grid" colspan="11">
                    <p class="tops">&nbsp;<?php echo $C202. ' ' . $P36. ' ' . $W36 . ' ' . $AO36 ?></p>
                    <p class="">&nbsp;<?php echo $C39.' '.$Z39 ?></p>
                    <p class="">&nbsp;<?php echo $U39 ?></p>
                    <p class="">&nbsp;<?php echo $C28 ?></p>
                    <p class="">&nbsp;<?php echo $C31 ?></p>
                </td>
            </tr>
            <tr>
                <td class="grid" colspan="12" style="border-bottom:solid 1px #dedede"><p class="tops">&nbsp;</p></td>
            </tr>
            <tr>
                <td class="grid border box_grey" colspan="7"><p class="label bold">Description</p></td>
                <td class="grid border box_grey" colspan="1"><p class="label bold center">Quantity</p></td>
                <td class="grid border box_grey" colspan="1"><p class="label bold center">Veecs</p></td>
                <td class="grid border box_grey" colspan="1"><p class="label bold center">Rate</p></td>
                <td class="grid border box_grey" colspan="2"><p class="label bold center">Amount Incl GST</p></td>
            </tr>
            <?php for($i=23; $i<=32; $i++): 
                if ($i!=27) {
                    

                $ZA = "ZA$i";
                //$ZAA = (isset($$ZA) && $$ZA == '') ? ' ' : $ZA;
                 $ZI = "ZI$i";
                 $ZG = "ZG$i";
                 $ZJ = "ZJ$i";
                 $ZK = "ZK$i";
                 $ZL = "ZL$i";
                 
                    echo '<tr>
                            <td class="grid border" colspan="7"><p class="pad">'.$$ZA.'</p></td>
                            <td class="grid border" colspan="1"><p class="pad right">'.$$ZI.'</p></td>
                            <td class="grid border" colspan="1"><p class="pad right">'.$$ZG.'</p></td>
                            <td class="grid border" colspan="1"><p class="pad right">'.$$ZJ.'</p></td>
                            <td class="grid border" colspan="2"><p class="pad right">'.$$ZK.'</p></td>
                        </tr>';
                }
                
                endfor; ?>
            <tr>
                <td class="grid" colspan="12"><p class="" style="margin-top:30px">&nbsp;</p></td>
            </tr>
            <tr>
                <td class="grid tops center" colspan="7" rowspan="5"><img src="asset/css/images/paid.png" alt="" style="width:250px;padding-top:30px" class="center tops"></td>
                <td class="grid tops right border-top border-left" colspan="3"><p class="label bold grey right tops">Subtotal</p></td>
                <td class="grid tops right border-top border-right" colspan="2"><p class="label bold grey right tops"><?php echo $ZL36 ?></p></td>
            </tr>
            <tr>
                <td class="grid right border-left" colspan="3"><p class="label bold grey right ">GST (10%)</p></td>
                <td class="grid right border-right" colspan="2"><p class="label bold grey right "><?php echo $ZL38 ?></p></td>
            </tr>
            
            <tr>
                <td class="grid right border-left" colspan="3"><p class="label bold grey right">Total</p></td>
                <td class="grid right border-right" colspan="2"><p class="label bold grey right "><?php echo $ZL40 ?></p></td>
            </tr>
            <tr>
                <td class="grid right border-left" colspan="3"><p class="label bold grey right">VEEC Incentive</p></td>
                <td class="grid right border-right" colspan="2"><p class="label bold grey right"><?php echo $ZL42 ?></p></td>
            </tr>
            <tr>
                <td class="grid right border-left border-bottom" colspan="3"><h1 class="bold">Balance Due</h1></td>
                <td class="grid right border-right border-bottom" colspan="2"><h1 class="bold"><?php echo $ZL46 ?></h1></td>
            </tr>
        
        </table>
        <div class="footer">
            <p>&copy; <small>Green Energy Trading Pty Ltd 2014</small></p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!--close page 5-->

    <!-- page 6-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid" colspan="12">
                    <p class="" style="margin-bottom:10px">GPS Location</p>
                </td>
            </tr>


            <tr class="">
                <td class="grid text-center" colspan="12">
                    <img style="width:530px;height:400px" class="text-center" src="<?= isset($mapsrc)?$mapsrc:'' ?>"/>
                </td>
            </tr>
            <tr class="">
                <td class="grid" colspan="12">
                    <p class="" style="margin-bottom:10px">Certificate of electrical safety (if applicable)</p>
                </td>
            </tr>
            <tr class="">
                <td class="grid text-center" colspan="12">
                    <?php if(isset($safety) && file_exists($safety)){ ?>
                    <img style="width:530px;height:400px;" src="<?= $safety ?>" class="text-center">
                    <?php }else echo '<p style="width:530px;height:400px">&nbsp;</p>' ?>
                </td>
            </tr>
        </table>
        <div class="footer">
            <p>&copy; <small>Green Energy Trading Pty Ltd 2014</small></p>
        </div>
    </div>
    <div class="pagebreak"></div>
    <!-- close page 6-->

    <!-- page 7-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid" colspan="12">
                    <p class="" style="margin-bottom:10px">Electricity Bill Front</p>
                </td>
            </tr>
            <tr class="">
                <td class="grid text-center" colspan="12">
                    <?php if(isset($bill_front) && file_exists($bill_front)){ ?>
                    <img style="width:530px;height:400px;" src="<?= $bill_front ?>" class="text-center">
                    <?php }else echo '<p style="width:530px;height:400px">&nbsp;</p>' ?>
                </td>
            </tr>
            <tr class="box_desc">
                <td class="grid" colspan="12">
                    <p class="" style="margin-bottom:10px">Installer Photo</p>
                </td>
            </tr>
            
            <tr class="">
                <td class="grid text-center" colspan="12">
                    <?php if(isset($installer) && file_exists($installer)){ ?>
                    <img style="height:400px;" src="<?= $installer ?>" class="text-center">
                    <?php }else echo '<p style="width:530px;height:400px">&nbsp;</p>' ?>
                </td>
            </tr>
        </table>
        <div class="footer">
            <p>&copy; <small>Green Energy Trading Pty Ltd 2014</small></p>
        </div>
    </div>
    <div class="pagebreak"></div>
    <!-- close page 7-->

    <!-- page 8-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid" colspan="12">
                    <p class="" style="margin-bottom:10px">Lamp Photo</p>
                </td>
            </tr>

            <tr class="">
                <td class="grid text-center" colspan="12">
                    <?php if(isset($evidence1) && file_exists($evidence1)){ ?>
                    <img style="width:530px;height:400px;" src="<?= $evidence1 ?>" class="text-center">
                    <?php }else echo '<p style="width:530px;height:400px">&nbsp;</p>' ?>
                </td>
            </tr>
            <tr class="">
                <td class="grid" colspan="12">
                    <p class="" style="margin-bottom:10px">Transformer Photos (21d only)</p>
                </td>
            </tr>
            <tr class="">
                <td class="grid text-center" colspan="12">
                    <?php if(isset($evidence2) && file_exists($evidence2)){ ?>
                    <img style="width:530px;height:400px;" src="<?= $evidence2 ?>" class="text-center">
                    <?php }else echo '<p style="width:530px;height:400px">&nbsp;</p>' ?>
                </td>
            </tr>
        </table>
        <div class="footer">
            <p>&copy; <small>Green Energy Trading Pty Ltd 2014</small></p>
        </div>
    </div>
    <div class="pagebreak"></div>
    <!-- close page 8-->

    <!-- page 9-->
    <!--
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid" colspan="12">
                    <p class="" style="margin-bottom:10px">Installation Evidence Photo 3</p>
                </td>
            </tr>
            
            <tr class="">
                <td class="grid text-center" colspan="12">
                    <?php if(isset($evidence3) && file_exists($evidence3)){ ?>
                    <img style="width:530px;height:400px;" src="<?= $evidence3 ?>" class="text-center">
                    <?php }else echo '<p style="width:530px;height:400px">&nbsp;</p>' ?>
                </td>
            </tr>
            <tr class="">
                <td class="grid" colspan="12">
                    <p class="" style="margin-bottom:10px">Installation Evidence Photo 4</p>
                </td>
            </tr>
            <tr class="">
                <td class="grid text-center" colspan="12">
                    <?php if(isset($evidence4) && file_exists($evidence4)){ ?>
                    <img style="width:530px;height:400px;" src="<?= $evidence4 ?>" class="text-center">
                    <?php }else echo '<p style="width:530px;height:400px">&nbsp;</p>' ?>
                </td>
            </tr>
        </table>
        <div class="footer">
            <p>&copy; <small>Green Energy Trading Pty Ltd 2014</small></p>
        </div>
    </div>
    <!-- close page 9-->

    <!-- page 10-->
    <!--
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid" colspan="12">
                    <p class="" style="margin-bottom:10px">Installation Evidence Photo 5</p>
                </td>
            </tr>

            <tr class="">
                <td class="grid text-center" colspan="12">
                    <?php if(isset($evidence5) && file_exists($evidence5)){ ?>
                    <img style="width:530px;height:400px;" src="<?= $evidence5 ?>" class="text-center">
                    <?php }else echo '<p style="width:530px;height:400px">&nbsp;</p>' ?>
                </td>
            </tr>
            <tr class="">
                <td class="grid" colspan="12">
                    <p class="" style="margin-bottom:10px">Installation Evidence Photo 6</p>
                </td>
            </tr>
            <tr class=" text-center">
                <td class="grid text-center" colspan="12">
                    <?php if(isset($evidence6) && file_exists($evidence6)){ ?>
                    <img style="width:530px;height:400px;" src="<?= $evidence6 ?>" class="text-center">
                    <?php }else echo '<p style="width:530px;height:400px">&nbsp;</p>' ?>
                </td>
            </tr>
            

        </table>
        <div class="footer">
            <p>&copy; <small>Green Energy Trading Pty Ltd 2014</small></p>
        </div>
    </div>
    <div class="pagebreak"></div>
    <!-- close page 10-->

    <!-- page 11-->
    <!--
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid" colspan="12">
                    <p class="" style="margin-bottom:10px">Installation Evidence Photo 7</p>
                </td>
            </tr>

            <tr class="">
                <td class="grid text-center" colspan="12">
                    <?php if(isset($evidence7) && file_exists($evidence7)){ ?>
                    <img style="width:530px;height:400px;" src="<?= $evidence7 ?>" class="text-center">
                    <?php }else echo '<p style="width:530px;height:400px">&nbsp;</p>' ?>
                </td>
            </tr>
            <tr class="">
                <td class="grid" colspan="12">
                    <p class="" style="">Installation Evidence Photo 8</p>
                </td>
            </tr>
            <tr class="">
                <td class="grid text-center" colspan="12">
                    <?php if(isset($evidence8) && file_exists($evidence8)){ ?>
                    <img style="width:530px;height:400px;" src="<?= $evidence8 ?>" class="text-center">
                    <?php }else echo '<p style="width:530px;height:400px">&nbsp;</p>' ?>
                </td>
            </tr>
            <tr class="pagebreak">
                <td class="grid" colspan="12">&nbsp;</td>
            </tr>

        </table>
        <div class="footer">
            <p>&nbsp;</p>
        </div>
    </div>
    <!-- close page 11-->



</div>
    

</body>
</html>