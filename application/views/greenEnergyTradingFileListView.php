<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Saved Forms</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="navbar" role="navigation" class="shadow" style="margin-bottom:0px">
                    <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle glyphicon glyphicon-list" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#" style="padding:10px"><img src="<?= base_url('asset/images/logo.png') ?>" class="img-responsive" style="width:100px" alt=""></a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-right navbar-nav">
                              <li class="dropdown">
                                <a href="<?= site_url('authcontroller/logout') ?>" data-toggle="tooltip" data-placement="left" title="logout"><i class="glyphicon glyphicon-log-out" style="padding:5px 10px"></i></a>
                              </li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="box_label text-center">
                            <input type="hidden" name="action" id="action">
                            <input name="action" id="action" type="hidden">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>SAVED FORMS</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <!--page 2-->
                            <div class="page" id="page2">
                                <div class="row">
                                    <div class="col-sm-4 hidden-xs">
                                        <img src="<?= $logo ?>" style="height:80px" alt="">
                                    </div>
                                    <div class="col-sm-4">
                                        <h2 class="text-center bold">VEEC 21</h2>
                                        <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                    </div>
                                    <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                    <div class="col-sm-3 hidden-xs">
                                        <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 box_desc">
                                        <div class="row">
                                            
                                            <form method="POST">
                                            <div class="col-xs-12 col-sm-4 col-sm-offset-1" style="margin-top:10px">
                                                <label>From</label>
                                                <input type="text" name="since" class="filter-date text-right filter-input pull-right" value="<?= isset($since)?$since:'' ?>" />
                                            </div>
                                            <div class="col-xs-12 col-sm-4" style="margin-top:10px">
                                                <label>To</label>
                                                <input type="text" name="until" class="filter-date text-right filter-input pull-right" value="<?= isset($until)?$until:'' ?>" />
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-sm-offset-1" style="margin-top:5px">
                                                <label>FILE NAME CONTAINING</label>
                                                <input type="text" class="filter-input pull-right" name="namelike" value="<?= isset($namelike)?$namelike:'' ?>" />
                                            </div>
                                            <div class="clearfix"></div>
                                            
                                            <?php if(isset($companies)): ?>
                                            <div class="col-xs-12 col-sm-4 col-sm-offset-1" style="margin-top:5px">
                                            	<label>SHOW FORM OF</label>
                                            	<select name="cid">
                                            		<option value="all">All Companies</option>
                                            		<?php foreach($companies as $company): ?>
                                            			<option value="<?= $company->cid ?>"
                                            				<?php if(isset($selected_company)) if($company->cid==$selected_company) echo 'SELECTED="SELECTED"' ?>
                                            				><?= $company->electricians_company_name ?></option>
                                            		<?php endforeach; ?>
                                            	</select>
                                            </div>
                                            <?php endif; ?>
                                            <div class="clearfix"></div>
                                            
                                            <div class="col-xs-12 col-sm-2 col-sm-offset-1" style="margin-top:5px">
                                                <input type="submit" class="btn btn-sm btn-warning" value="SEARCH" />
                                                <input type="reset" class="btn btn-sm btn-danger" value="RESET" onclick="jQuery('.filter-input').val('').parent().parent('form').submit()" />
                                            </div>
                                            </form>

                                            <div class="col-xs-12 col-sm-offset-1" style="margin-top:10px;margin-bottom:5px;">
                                                <a href="<?= site_url('maincontroller/csv') ?>" class="btn btn-info btn-sm">CSV ALL</a>
                                                <a href="javascript:download_csv_selected();" class="btn btn-info btn-sm">CSV SELECTED</a>
                                            </div>
                                            
                                            <div class="col-xs-12">
                                                <div class="table-responsive" style="overflow-x:auto;max-width:100%">
                                                    <table id="tcodes_tbl" cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
                                                        <thead>
                                                            <tr class="skyblue">
                                                                <th class="text-center">CSV</th>
                                                                <th class="text-center">AUDIT</th>
                                                                
                                                                <th class="text-center">COMPLETION STATUS</th>
                                                                <th class="text-center">JOB REFERENCE</th>
                                                                <th class="text-center">FILE NAME</th>
                                                                <th class="text-center">INSTALLATION DATE</th>
                                                                <th class="text-center">USER</th>
                                                                <th class="text-center">COMPANY</th>
                                                                <th class="text-center">ACTION</th>
                                                                <!--<th class="text-center"></th>-->
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($items as $item): if ($item->complete != 1) continue; ?>
                                                                <tr>
                                                                    <td class="text-center"><input type="checkbox" value="<?= $item->fid ?>" /></td>
                                                                    <td 
                                                                    	style="text-align:center;cursor:pointer;background: url(<?= $item->redflag==1?base_url('asset/images/flag-red.ico'):base_url('asset/images/flag-white.ico') ?>) no-repeat"
                                                                    	onclick="$.get('<?= site_url('maincontroller/redflag/' . $item->fid) ?>', function () {window.location=window.location.href})">
                                                                    </td>
                                                                    
                                                                    <td class="text-center" width="20%">
                                                                        
                                                                    	<?= $item->complete == 1 ? 
                                                                          "<p class='bg-success'>COMPLETED</p>":
                                                                          "<p class='bg-warning'>INCOMPLETE</p><p class='text-left'>$item->incompletion_msg</p>";
                                                                          
                                                                        
                                                                            if ($current_user['is_admin']) {
                                                                            echo $item->complete == 1 && $item->completed_date > 0 ? date('d.m.Y h:i:sa', $item->completed_date) : '';
                                                                          }
                                                                        ?>
																		<form action="" method="POST">
																			<?php switch ($item->admin3_status) : case null: ?>
																				
        																		<textarea id="admin3_msg" name="admin3_msg" placeholder="Info Request Details"><?= $item->admin3_msg ?></textarea><br/>
        																		<input type="submit" name="set_admin3_msg" value="Info Request" class="btn btn-xs btn-warning" />
        																	<?php break; case 'incomplete': ?>
                                                                                <p class="bg-danger">Information Requested</p>
                                                                                <?php echo $item->request_date > 0 ? date('d.m.Y h:i:sa', $item->request_date) : ''; ?>
                                                                                <?php if($item->info_submitted==1) :?>
                                                                                    <p class="bg-danger">Information Submitted</p>
                                                                                    <?php echo $item->submitted_date > 0 ? date('d.m.Y h:i:sa', $item->submitted_date) : ''; ?>
                                                                                    
                                                                                        <textarea id="admin3_msg" name="admin3_msg" placeholder="Info Request Details"><?= $item->admin3_msg ?></textarea><br/>
                                                                                        <input type="submit" name="set_admin3_msg" value="Info Request" class="btn btn-xs btn-warning" />
                                                                                    

                                                                                <?php else:?>

                                                                                    <textarea id="admin3_msg" name="admin3_msg" placeholder="Info Request Details"><?= $item->admin3_msg ?></textarea><br/>

                                                                                <?php endif;?>
        																		
                                                                            <?php break; case 'confirmed': ?>
                                                                                <p class="bg-info">Form Checked and Clean</p>
                                                                                <?php echo $item->checked_date > 0 ? date('d.m.Y h:i:sa', $item->checked_date) : ''; ?>
        																	<?php endswitch ?>
        																			
    																		<?php if( (('confirmed'!=$item->admin3_status) && ('incomplete'!=$item->admin3_status)) || ($item->info_submitted==1) ): ?>
    																				<input type="hidden" name="fid" value="<?= $item->fid ?>" />
    	                                                                            <input type="submit" name="set_admin3_complete" value="Checked Clean" class="btn btn-xs btn-warning" />
                                                                            <?php endif; ?>

																		</form>

                                                                        <!-- process button-->
                                                                        <?php if(null!=$item->admin3_status && $item->admin3_status=='confirmed'): ?>
                                                                            <?php if ($item->processed==1) : 
                                                                            echo '<p class="bg-warning">Processed</p>' ;
                                                                            echo $item->processed_date > 0 ? date('d.m.Y h:i:sa', $item->processed_date) : '';
                                                                            else : ?>
                                                                            <button class="btn btn-xs" onclick="$.get('<?= site_url('maincontroller/processed/' . $item->fid) ?>', function () {window.location=window.location.href})">
                                                                                Process
                                                                            </button>
                                                                            <?php endif; ?>
                                                                        <?php endif; ?>
                                                                        <!--end process button -->
                                                                    </td>
                                                                    <td><?= $item->clientNumber ?></td>
                                                                    <td><?= $item->name ?></td>
                                                                    <td class="text-center"><?php
                                                                    $data = json_decode($item->data, true);
                                                                    echo isset($data['C68']) && !empty($data['C68']) ? $data['C68'] : '';
                                                                     ?></td>
                                                                    <td><?= $item->email ?></td>
                                                                    <td><?= $item->electricians_company_name ?></td>
                                                                    <td>
                                                                    	<?php if($current_user['is_admin'] == 1 || $item->complete != 1): ?>
                                                                        <a href="<?= site_url("maincontroller/edit/$item->fid") ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Load/Edit"><i class="glyphicon glyphicon-edit"></i></a>
                                                                        <a href="<?= site_url("maincontroller/edit/$item->fid/upload") ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Upload"><i class="glyphicon glyphicon-camera"></i></a>
                                                                        <?php endif; ?>

                                                                        <a href="<?= site_url("maincontroller/pdf/$item->fid") ?>" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Print PDF"><i class="glyphicon glyphicon-print"></i></a>

                                                                        <?php if($this->session->userdata('is_admin')==1): ?>
                                                                        <a href="<?= site_url("maincontroller/delete/$item->fid/confirm") ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
                                                                        
                                                                        <?php endif; ?>
                                                                    </td>
                                                                    <!--
                                                                    <td>
                                                                    	<?php if ($item->lodge_draft !== 'LODGE FORM'): ?>
	                                                                    	<form action="" method="POST">
	                                                                    		<input type="hidden" name="fid" value="<?= $item->fid ?>" />
	                                                                    		<input type="submit" name="lodge_draft" class="btn btn-sm btn-primary"
	                                                                    		value="LODGE FORM" />
	                                                                    	</form>
                                                                    	<?php endif; ?>
                                                                    </td>
                                                                    -->
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--close page2-->
                        </div>
                    </div>
                </div>
            </div>      
        </div>

        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('asset/js/jquery-ui.min.js') ?>"></script>
        <script type="text/javascript">
        	jQuery('.filter-date').datepicker();
        	function download_csv_selected(){
        		var download_csv_url = '<?= site_url('maincontroller/csv?ids=') ?>';
        		jQuery('table#tcodes_tbl tbody tr td :checkbox:checked').each(function(){
        			download_csv_url += jQuery(this).val()+'|';
        		});
        		download_csv_url = download_csv_url.slice(0,-1);
        		window.location = download_csv_url;
        	}
        </script>
    </body>
</html>