<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>assignment form</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">

    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center" id="content">
                    <div class="panel">
                      
                        <div class="box_label text-center">
                            <input type="hidden" name="action" id="action">
                            <input name="action" id="action" type="hidden">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>EVIDENCE 34</strong></h4>
                                </div>
                            </div>
                        </div>
                        
                        <div class="panel-body">
                          
                          <div class="row">
                            <div class="col-sm-4 hidden-xs">
                              <img src="<?= $logo ?>" style="height:80px" alt="">
                            </div>
                            <div class="col-sm-4">
                              <h2 class="text-center bold">VEEC 21</h2>
                              <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                            </div>
                            <div class="col-sm-1 hidden-xs">&nbsp;</div>
                            <div class="col-sm-3 hidden-xs">
                              <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                            </div>
                          </div>
                          
                          <?php if (isset($self)): ?>
                            <div class="row">
                              <div class="col-sm-12 box_desc text-center">
                              <h4><?= $self['name'] ?></h4>
                              </div>
                            </div>
                          <?php endif ?>
                          
                          <div class="row control box_desc">
                            <div class="col-md-1">
                              <a class="btn" href="
                                <?= isset($self)?
                                    site_url('evidence34controller/open/' . $self['fid34'] . '/' . $self['parent']):
                                    site_url('form34controller/') ?>"
                              >GO UP</a>
                            </div>
                            <div class="col-md-4">
                              <?php if (isset($self) && $self['type']=='folder'): ?>
                                <form action="" enctype="multipart/form-data" class="form-inline" method="POST">
                                  <div class="input-group">
                                    <input type="file" class="form-control" name="name">
                                    <input type="hidden" name="type" value="file" />
                                    <span class="input-group-btn">
                                      <input type="submit" name="upload" value="UPLOAD FILE" class="btn" />
                                    </span>
                                  </div>
                                </form>
                              <?php endif ?>
                            </div>
                            <div class="col-md-3">
                              <?php if(!isset($self) || (isset($self)&&$self['type']!='file')): ?>
                              <form action="" class="form-inline" method="POST">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="name">
                                  <input type="hidden" name="type" value="folder" />
                                  <span class="input-group-btn">
                                    <input type="submit" name="create" value="CREATE FOLDER" class="btn" />
                                  </span>
                                </div>
                              </form>
                              <?php elseif(file_exists(EVIDENCE34_DIR . $self['name'])): ?>
                                <a href="<?= site_url('evidence34controller/download/' . $self['eid']) ?>" class="btn">DOWNLOAD</a>
                              <?php endif ?>
                            </div>
                            <div class="col-md-3">
                              <?php if (isset($self) && $self['type']=='folder'): ?>
                              <form action="" class="form-inline" method="POST">
                                <div class="input-group">
                                  <input type="text" class="form-control" name="name">
                                  <input type="hidden" name="type" value="folder" />
                                  <span class="input-group-btn">
                                    <input type="submit" name="rename" value="RENAME FOLDER" class="btn" />
                                  </span>
                                </div>
                              </form>
                              <?php endif; ?>
                            </div>
                            <div class="col-md-1">
                              <?php if (isset($self)): ?><a href="<?= site_url('evidence34controller/delete/confirm/' . $self['eid']) ?>" class="btn">DELETE</a><?php endif ?>
                            </div>
                          </div>
                          
                          <div class="row">
                            <div class="col-sm-12 box_desc" style="padding: 20px 20px">
                              
                              <?php if (isset($childs)): ?>
                              <ul class="nav nav-pills text-center">
                                
                                <?php foreach ($childs as $child): ?>
                                <li>
                                  <a href="<?= site_url("evidence34controller/open/$child->fid34/$child->eid") ?>" class="btn 34form-menu">
                                    <img src="<?= base_url('asset/images/folder_open.png') ?>" class="dashboard-icon" alt=""> <br>
                                    <span class="bold"><?= $child->name ?></span>
                                  </a>
                                </li>
                                <?php endforeach ?>
                                
                              </ul>
                              <?php endif ?>

                            </div>
                          </div>
                          
                        </div><!-- close panel-body -->
                    </div><!-- close panel -->
                </div>
            </div><!-- close content -->   
        </div><!-- close container -->

        <!-- script references -->
          <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js') ?>"></script>-->
        <script src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        
    </body>
</html>