<?php 
//foreach ($_POST as $key => $value) {
//    $$key = $value;
//}
?>
 
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="author" content="4121" />
    <title>Assignment Form</title>
    <style>
        body{
            padding:0;
            margin:0;
            font-family: Tahoma, Arial, Helvetica, Tahoma, serif;
            color:#000;
        }
        @page{
            margin-top:0;
            margin-left: 0.5cm;
            margin-bottom:0;
            margin-right:0.5cm;
        }
        .page{
            /*height:27.9cm;*/
            width:19cm;
            margin:0 auto;
            page-break-after:auto;
            overflow:auto;
            font-size:90%;
            background:white;
            padding:10px;
        }
        
        .watermark{ 
            background-position: bottom right; 
            background-repeat: no-repeat; 
            color: #d0d0d0;
          -webkit-transform: rotate(-20deg);
          -moz-transform: rotate(-20deg);
          margin: 0;
          z-index: -1;
        }

       
        .pagebreak{
            page-break-after:always;
            
        }
        .center{
            text-align:center;
        }
        .bold{
            font-weight: bold;
        }
        .italic{
            font-style: italic;
        }
        
        li{
            text-align:justify;
            margin:15px 5px;
        }
        .title{
            font-weight:bold;
            font-size:14px;
        }
        div.content{
            padding:10px;
        }
        .box_page{
            border: solid 1px #000; 
        }
        table{
            border-collapse:collapse;
        }
        td.box{
            border:solid 1px #000;
        }
        td.grid{
            width: <?php echo 100/12 ?>%;
            padding: 10px;

        }
        td.grid-small{
            width: <?php echo 100/12 ?>%;
            padding-top: 2px;

        }
        td,p,body,li,ol,ul{
            font-size:14px;
        }
        .label{
            font-size: 14px;
        }
        .small{
            font-size: 8px;
        }
        .linespace{
            line-height: 20px;
            text-align:justify;
        }
        td.underline{
            border-bottom: solid .5px black;
        }
        td.text-line{
           
            text-decoration:underline;
        }
        td.dotted{
           border-bottom: dotted .5px black;
            text-decoration:none;
        }
        .tops{
            margin-top: 10px;
        }
        .top{
            margin-top: 20px;
        }
        .right{
            text-align:right;
        }
        .bottom{
            margin-bottom: 10px;
        }
        .grey{
            color:grey;
        }
        .justify{
            text-align: justify;
        }
        .border{
            border: solid 1px grey;
        }
        .border-bottom{
            border-bottom: solid 1px grey;
        }
        .border-top{
            border-top: solid 1px grey;
        }
        .border-right{
            border-right: solid 1px grey;
        }
        .border-left{
            border-left: solid 1px grey;
        }
        .zero{
            height: 0px;
        }
        .box_desc{
          background: #fff;
        }
        .box_label{
          background: #00C57D;
          color: white;
          border: solid 1px #000;
        }
        .skyblue{
          background-color: #00B1E0 !important;
          color: white;
        }
        .warm{
          background-color: #D7C100 !important;
          color: white;
        }
        .pad{
            height: 10px;
        }
        .box_grey{
            background: #dedede;
            color: #000;
        }
        ol {list-style:none;}
        .top-line{
            vertical-align:top
        }
        .footer {
            clear: both;
            position: fixed;
            bottom: 25px;
            height: 2em;
            margin-top: -2em;
            font-size: 10px !important;
            text-align: right !important; }
    </style>
</head>
<body>
<div class="page">
    <!--page 1-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid center" colspan="12">
                    <h3 class="bold center">
                        IN TERMS OF AN AGREEMENT <br>
                        <small>
                            <?php 
                            $t=date('d-m-Y');
                            echo 'made this '.date("d").' of '.date("M").' '.date("Y");
                        ?>
                        </small>
                    </h3>
                    <p class="center bold">BETWEEN</p>
                    <p class="center">
                        Eminent Services Group PL <br>
                        Trading as <br>
                        ‘My Electrician’ <br>
                        ABN 95 149 996 958 <br>
                        of 25 Neill Street Berwick 3806 <br>
                        (the “Solution Provider”) <br>
                    </p>
                    <p class="center bold">AND</p>
                    <p class="center">
                        <?php echo $author ?><br>
                        <?php echo $electricians_company_name ?><br>
                        ABN/ACN <?php echo $abn_number ?> <br>
                        of <br>
                        <?php echo $company_address. ' ' . $suburb . ' ' . $postal_code ?> <br>
                        (the “Installation Company”)
                    </p>
                </td>
            </tr>
            <tr>
                <td class="grid" colspan="12">
                    <h3 class="bold center">TERMS AND CONDITIONS</h3>
                    <h4 class="bold">INTRODUCTION:</h4>
                    <ol style="list-style-type:upper-alpha">
                        <li class="">
                            The Certificate Creation Partner (“CCP”) provides a range of services to participants in environmental markets, including product accreditation and Environmental Certificate Creation and monetisation and is to be nominated from time to time by and in the absolute and sole discretion of My Electrician.
                        </li>
                        <li>
                            My Electrician provides a range of services to participants in environmental markets, including product supply, Software systems for Environmental Certificate creation and monetisation.
                        </li>
                        <li>
                            The Installation Company operates a business that is involved in the supply and/or installation of Systems for System Owners.
                        </li>
                        <li>
                            The Installation Company wishes to work with My Electrician on an ongoing basis in respect of a range of business activities including but not limited to transactions with System Owners.
                        </li>
                        <li>
                            From time to time the Installation Company may:
                            <ol>
                                <li><span>1.</span> offer System Owners a discount in return for the System Owner entering into an assignment agreement with My Electrician or its nominated Certificate Creation Partner;</li>
                                <li><span>2.</span> utilise My Electrician’s online transaction portal (“MOP”).</li>
                            </ol>
                        </li>
                        
                    </ol>
                </td>
            </tr>

        </table>
        <div class="footer">
            <p>Page 1 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 1-->

    <!--page 2-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid" colspan="12">
                    <h4 class="bold">TERMS :</h4>
                </td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    1.
                </td>
                <td class="grid" colspan="11">
                    Definitions and Interpretation
                </td>
            </tr>
            <tr>
                <td class="grid center" colspan="2">1.1</td>
                <td class="grid" colspan="10">Definitions: For the purposes of this Agreement:</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.1</td>
                <td class="grid justify" colspan="9">
                    “<strong><i>Accept</i></strong>” means, in respect of:
                    <ol style="list-style-type:lower-roman">
                        <li>a Submitted Assignment Agreement, the process by which My Electrician becomes bound by that Assignment Agreement; and</li>
                        <li>this Agreement the acceptance by My Electrician of any Purchase Orders effective immediately upon My Electrician transmitting electronically its acceptance of any Purchaser Order to any such Electronic address as it may have received from the person who requests such Purchase Order (“the purchaser”).</li>
                    </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.2</td>
                <td class="grid justify" colspan="9">“Administrator” includes a liquidator, receiver or receiver and manager (whether provisional or otherwise).</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.3</td>
                <td class="grid justify" colspan="9">“Agreement” means this Agreement and governs all future dealings between My Electrician and the Installation Company for the purposes of Introduction paragraph B.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.4</td>
                <td class="grid justify" colspan="9">“Assignment Agreement” means an agreement between My Electrician and a System Owner, in which the System Owner assigns its rights to create a Parcel of Environmental Certificates to My Electrician.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.5</td>
                <td class="grid justify" colspan="9">“CER” means the Clean Energy Regulator.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.6</td>
                <td class="grid justify" colspan="9">“Certificate Creation Partner (“CCP”)” means an accredited person under VEET who provides the services described in paragraph A of the Introduction.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.7</td>
                <td class="grid justify" colspan="9">“Code of Conduct” means any set of rules or other obligations that are a condition of membership of an industry association to which a Party belongs.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.8</td>
                <td class="grid justify" colspan="9">“Confidential Information” means the confidential information described in Clause 11, My Electrician’s Industrial Property, Intellectual Property and Know-How.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.9</td>
                <td class="grid justify" colspan="9">“Data” means information contained within the MOP, including Form Data and Solution Data. </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.10</td>
                <td class="grid justify" colspan="9">“Defaulting Party” has the meaning given in clause 7.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.11</td>
                <td class="grid justify" colspan="9">“Energy Saving Certificate” has the meaning given in the Electricity Supply Act 1995 (NSW).</td>
            </tr>

        </table>
        <div class="footer">
            <p>Page 2 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 2-->

    <!--page 3-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            
            
            <tr>
                <td class="grid center top-line" colspan="3">1.1.12</td>
                <td class="grid justify" colspan="9">“Environmental Certificate” means a tradeable certificate that can be created to represent the environmental improvement created through the deployment of a System, and includes Small-scale Technology Certificates, Large Generation Certificates, Victorian Energy Efficiency Certificates, and Energy Saving Certificates.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.13</td>
                <td class="grid justify" colspan="9">“ESC” means Essential Services Commission.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.14</td>
                <td class="grid justify" colspan="9">“Form Data” means Data that is presented to the Installation Company by MOP to assist in the entry of Solution Data.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.15</td>
                <td class="grid justify" colspan="9">“My Electrician” has the meaning given at the commencement of this document.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.16</td>
                <td class="grid justify" colspan="9">“GST” means Goods and Services Tax, as that phrase is defined in the A New Tax System (Goods and Services Tax) Act 1999 (Cth).</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.17</td>
                <td class="grid justify" colspan="9">“Industrial Property” means all My Electric’s (whether or not in documentary visual, oral, machine readable, electronic, digital, tangible or other forms) designs, specifications, artwork, trademarks, copyright, business names, trade secrets, methodologies, insignia, logogram, brochures, promotional material, advertising layouts, bromides, illustrations, client lists, master files of all administration forms, disseminated information and other industrial property the interest in which is the property of My Electrician.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.18</td>
                <td class="grid justify" colspan="9">“Injured Party” has the meaning given in clause 7.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.19</td>
                <td class="grid justify" colspan="9">
                    “Insolvent” means the occurrence of any one or more of the following events in respect of a person:
                    <ol style="list-style-type:lower-alpha">
                        <li class="justify">an application is made that it be wound up, declared bankrupt or that an Administrator be appointed;</li>
                        <li class="justify">an Administrator is appointed to any of its assets;</li>
                        <li class="justify">it enters into an arrangement with its creditors (or proposes to do so);</li>
                        <li class="justify">it makes an assignment to benefit one or more creditors (or proposes to do so);</li>
                        <li class="justify">it is insolvent, states that it is insolvent or it is presumed to be insolvent under an applicable law;</li>
                        <li class="justify">it becomes an insolvent under administration or action is taken which could result in that event;</li>
                        <li class="justify">a writ of execution is levied against it or its property;</li>
                        <li class="justify">it ceases to carry on business or threatens to do so; or</li>
                        <li class="justify">anything occurs under the law of any applicable jurisdiction which has a substantially similar effect to paragraphs a) - h) above.</li>
                    </ol>
                </td>
            </tr>

        </table>
        <div class="footer">
            <p>Page 3 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 3-->

    <!--page 4-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            
            <tr>
                <td class="grid center top-line" colspan="3">1.1.20</td>
                <td class="grid justify" colspan="9">“Intellectual Property” means all My Electrician’s rights and industrial property rights, titles, interests in and to data, instructions, plans, formulae, technology, computer software, designs, process descriptions, reports, results, technical advice and trade secrets whether in documentary, visual, oral, machine, readable, electronic or other forms.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.21</td>
                <td class="grid justify" colspan="9">“Know-how” means My Electrician’s expertise in the supply of services described in Introduction paragraph B, marketing of My Electrician’s products through the System, techniques developed business systems and methods of operation and the business trading name My Electric some of which is secret.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.22</td>
                <td class="grid justify" colspan="9">“MOP” has the meaning given in paragraph E 2 of the Introduction.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.23</td>
                <td class="grid justify" colspan="9">“Offer Price” means, on any given day and in respect of a particular type of Environmental Certificate, the price specified by My Electrician for that particular type of Environmental Certificate on that day.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.24</td>
                <td class="grid justify" colspan="9">“Parcel of Environmental Certificates” means, in respect of a specific System, the Environmental Certificates that may be created as a result of the installation or operation of that System.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.25</td>
                <td class="grid justify" colspan="9">“Parcel Volume” means, in respect of a particular Parcel of Environmental Certificates, the number of Environmental Certificates contained in that parcel. </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.26</td>
                <td class="grid justify" colspan="9">“Parties” means My Electrician, Installation Company and their transferees, assigns and successors in title.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.27</td>
                <td class="grid justify" colspan="9">“Penalty Interest Rate” means the rate specified in the Penalty Interest Rates Act 1983 (Vic).</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.28</td>
                <td class="grid justify" colspan="9">“Privacy Act” means the Privacy Act 1988 (Cth).</td>
            </tr>

        </table>
        <div class="footer">
            <p>Page 4 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 4-->

    <!--page 5-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            
            <tr>
                <td class="grid center top-line" colspan="3">1.1.29</td>
                <td class="grid justify" colspan="9">“Purchase Order” means any request from time to time by the Installation Company of My Electrician to provide to System Owners the activities contained in paragraph B of the Introduction to these Terms and Conditions. Upon acceptance by My Electrician of such Purchase Order the Terms and Conditions of this Agreement come into operation.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.30</td>
                <td class="grid justify" colspan="9">“Regulator” means a government department or agency that has responsibility for the administration of programs involving or associated with Environmental Certificates, and includes the CER, the ESC and IPART.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.31</td>
                <td class="grid justify" colspan="9">“Solution Data” means Data entered into the MOP by the Installation Company that is not Form Data.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.32</td>
                <td class="grid justify" colspan="9">“Solution Provider” has the meaning given at the commencement of this document.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.33</td>
                <td class="grid justify" colspan="9">“Submit” means, in respect of an Assignment Agreement, delivery to My Electrician for Acceptance and processing by either physical or electronic means.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.34</td>
                <td class="grid justify" colspan="9">“System” means MOP portal, the Assignment Agreement, this Agreement, the method of operation of each of the Assignment Agreement, MOP and this Agreement whether in documentary, visual, oral, machine readable, electronic, tangible or other forms and equipment which can be installed or removed to enable the creation of Environmental Certificates.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.35</td>
                <td class="grid justify" colspan="9">“System Owner”, in respect of a specific System, means the owner of that System or other person entitled to create the Environmental Certificates associated with the System.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.1.36</td>
                <td class="grid justify" colspan="9">“Victorian Energy Efficiency Certificate” has the meaning given in the Victorian Energy Efficiency Target Act 2007 (Vic) (“VEET”).</td>
            </tr>
            <tr>
                <td class="grid center" colspan="2">2.1</td>
                <td class="grid" colspan="10">Interpretation: For the purposes of this agreement:</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.2.1</td>
                <td class="grid justify" colspan="9">
                    <strong>Plural and Singular:</strong> <br>
                    Words importing the singular number include the plural and vice versa.
                </td>
            </tr>

            

        </table>
        <div class="footer">
            <p>Page 5 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 5-->

    <!--page 6-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            
            <tr>
                <td class="grid center top-line" colspan="3">1.2.2</td>
                <td class="grid justify" colspan="9">
                    <strong>Persons:</strong> <br>
                    References to persons include references to individuals, companies, corporations, firms, partnerships, joint ventures, associations, organisations, trusts, states or agencies of state, government departments and local and municipal authorities, whether or not having separate legal personality.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.2.3</td>
                <td class="grid justify" colspan="9">
                    <strong>Headings:</strong> <br>
                    Clause and other headings are for ease of reference only and do not form any part of the context or to affect the interpretation of this Agreement.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="3">1.2.4</td>
                <td class="grid justify" colspan="9">
                    <strong>Introduction and Schedules:</strong> <br>
                    The Introduction and Schedules to this Agreement form part of the Agreement.
                </td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    2.
                </td>
                <td class="grid" colspan="11">
                    Assignment Agreement:
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">2.1</td>
                <td class="grid" colspan="10">The Installation Company may contract with a System Owner for the supply, deployment and/or removal of a System on terms including a benefit conditional on System Owner entering into an Assignment Agreement with My Electrician or its nominated Certificate Creation Partner.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">2.2</td>
                <td class="grid" colspan="10">When an Assignment Agreement executed by the System Owner is Submitted to and Accepted by My Electrician or its nominated Certificate Creation Partner, the Installation Company make the representations and warranties set out below.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">2.3</td>
                <td class="grid justify top-line" colspan="10">
                    On Submission of an Assignment Agreement executed by a System Owner, the Installation Company represents and warrants to My Electrician and My Electrician that it:
                    <ol class="top-line" style="list-style-type:lower-alpha">
                        <li class="top-line">is unaware of System Owner dealing with its Environmental Certificate creation rights except through the Assignment Agreement;</li>
                        <li>has no claim over and will not interfere with My Electrician’s rights under the Assignment Agreement;</li>
                        <li>consents to My Electrician or its nominated Certificate Creation Partner making statements to third parties in reliance on the information provided under this Agreement by My Electrician, its associates and customers;</li>
                        <li>considers the information contained in any Assignment Agreement submitted to My Electrician to be true and correct in every particular;</li>
                    </ol>
                </td>
            </tr>
            

        </table>
        <div class="footer">
            <p>Page 6 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 6-->

    <!--page 7-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            
            <tr>
                <td class="grid center" colspan="2">&nbsp;</td>
                <td class="grid" colspan="10">
                    &nbsp;
                    <ol style="list-style-type:lower-alpha;" start="5">
                        <li class="">will inform My Electrician of any new information it obtains in respect of a System that indicates that previously supplied information in respect of that System is misleading, incorrect or inaccurate;</li>
                        <li class="">indemnifies My Electrician for any and all losses that My Electrician may suffer if information described in 2.3 (e), (g) and (i) is misleading, incorrect or inaccurate;</li>
                        <li class="">will provide My Electrician and My Electrician any information or assistance required to create Environmental Certificates or comply with Regulator requirements;</li>
                        <li class="">will assist My Electrician and My Electrician to conduct an inspection of the System the subject of the Assignment Agreement if requested by My Electrician;</li>
                        <li class="">has supplied a System that is fit for purpose and in compliance with all regulatory requirements (not just those relating to Environmental Certificates);</li>
                        <li class="">will refund to My Electrician erroneous payments, or payments made where the Parcel of Environmental Certificates for a System is less than that specified in the Assignment Agreement.</li>
                    </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">2.4</td>
                <td class="grid justify top-line" colspan="10">
                    On Acceptance of an Assignment Agreement, My Electrician represents and warrants to the Installation Company that it will:
                    <ol class="top-line" style="list-style-type:lower-alpha">
                        <li class="top-line">pay the amount specified in the Assignment Agreement by electronic transfer to the Installation Company’s account within the timeframe specified by My Electrician;</li>
                        <li>comply with usual Regulator requirements associated with the creation of the Parcel of Environmental Certificates.</li>
                    </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">2.5</td>
                <td class="grid justify top-line" colspan="10">
                    The Installation Company must:
                    <ol class="top-line">
                        <li class="top-line">2.5.1 provide to every System Owner evidence of the system installed by it using and completing the purchase receipt attached to the Assignment Agreement in MOP and handing that receipt to the System Owner; and</li>
                        <li>ensure that the duplicate copy of the purchase receipt attached to the Assignment Agreement in MOP and electronically completed on MOP is identical to the purchase receipt provided to every System Owner in terms of Clause 2.5.1.</li>
                    </ol>
                </td>
            </tr>
            

        </table>
        <div class="footer">
            <p>Page 7 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 7-->

    <!--page 8-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    3.
                </td>
                <td class="grid" colspan="11">
                    My Electrician Online Portal (“MOP”):
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">3.1</td>
                <td class="grid justify top-line" colspan="10">
                   At any time, the Installation Company may request access to MOP.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">3.2</td>
                <td class="grid justify top-line" colspan="10">
                   If the Installation Company requests access to MOP, My Electrician may accept or decline the request in its absolute discretion.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">3.3</td>
                <td class="grid justify top-line" colspan="10">If My Electrician allows the Installation Company access to MOP, the Installation Company agrees that it will comply with the terms of use that are in force at the time of access and which are available on MOP.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">3.4</td>
                <td class="grid justify top-line" colspan="10">It is the Installation Company’s responsibility to manage user access. The Installation Company must nominate Authorised Representatives and advise of any user access changes by written email to My Electrician.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">3.5</td>
                <td class="grid justify top-line" colspan="10">
                    MOP incorporates two areas, an Administrator’s Area and a User’s Area. Different rules apply to Data in the two areas: <br>
                    a). in both the Administrator’s Area and the User’s Area:
                    <ol style="list-style-type:lower-roman">
                        <li>Form Data is owned by My Electrician and licenced to My Electrician;</li>
                        <li>Solution Data is owned by My Electrician and licenced to My Electrician;</li>
                        <li>Solution Data has been received by My Electrician for the purposes of the Privacy Act.</li>
                    </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">3.6</td>
                <td class="grid justify top-line" colspan="10">Solution Data is in the Private Area until it is Lodged, when it enters the Shared Area.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">3.7</td>
                <td class="grid justify top-line" colspan="10">Form Data is in the Private Area if it is associated with Solution Data that has not been Submitted. Otherwise it is in the Shared Area.</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">3.8</td>
                <td class="grid justify top-line" colspan="10">The Installation Company has a licence to use Form Data and Solution Data for the sole and exclusive purposes of complying with its obligations under this Agreement and must not whether directly or indirectly allow the Form Data and Solution Data to be used for any other purpose. The Installation Company acknowledges that if any such licence is breached in any manner whatsoever that licence terminates immediately upon such breach and the Installation Company immediately becomes liable to My Electrician for all and any loss and damage they may suffer and My Electrician is entitled in its absolute discretion to prevent access to MOP by the Installation Company and without further notice.</td>
            </tr>
           
            

        </table>
        <div class="footer">
            <p>Page 8 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 8-->

    <!--page 9-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    4.
                </td>
                <td class="grid" colspan="11">
                    General obligations:
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">4.1</td>
                <td class="grid justify top-line" colspan="10">
                   During the course of this Agreement, the Installation Company must:
                   <ol style="list-style-type:lower-alpha">
                       <li>comply with directions and advice issued by My Electrician and its associates;</li>
                       <li>ensure that its employees, agents and independent contractors comply with this Agreement and any Associated Agreements;</li>
                       <li>promptly advise My Electrician if its becomes aware of any action that a third party is taking or may take that could result in a claim being made against the Installation Company or result in a requirement for the Installation Company to notify its insurer;</li>
                       <li>not represent that it can bind My Electrician or its associates in any way;</li>
                       <li>comply with all applicable privacy and confidentiality laws;</li>
                       <li>submit Assignment Agreements to My Electrician promptly and at least one (1) month prior to any relevant regulatory deadline;</li>
                       <li>take all steps to make it clear to all third parties that it is not an agent, partner or joint venturer with My Electrician or its nominated Certificate Creation Partner and that it is an independent business entity;</li>
                       <li>immediately it becomes or ought to become aware thereof notify My Electrician of any non-compliance by the Installation Company with any provision of this Agreement or associated Agreements.</li>
                   </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    5.
                </td>
                <td class="grid" colspan="11">
                    Payments:
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">5.1</td>
                <td class="grid justify top-line" colspan="10">
                   At any time the Parties may have payment obligations to each other under this or any other Agreements.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">5.2</td>
                <td class="grid justify top-line" colspan="10">
                   Where this is the case, My Electrician may in its sole and absolute discretion:
                   <ol style="list-style-type:lower-alpha">
                       <li>deduct amounts owed by the Installation Company from amounts owed to the Installation Company, and pay a net amount as a final settlement; or</li>
                       <li>in the event where outstanding payments are referred to a collection agency and/or law firm, pass on all costs which would be incurred as if the debt were collected in full, including legal demand costs.</li>
                   </ol>
                </td>
            </tr>

        </table>
        <div class="footer">
            <p>Page 9 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 9-->

    <!--page 10-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">5.3</td>
                <td class="grid justify top-line" colspan="10">
                   For the avoidance of doubt, these provisions are applicable to any amounts owed to/claimed by My Electrician including amounts resulting from:
                   <ol style="list-style-type:lower-alpha">
                       <li>the terms of this Agreement;</li>
                       <li>a breach of this Agreement;</li>
                       <li>the invalidity of any Environmental Certificates;</li>
                       <li>the operation of the common law or applicable statutes; or</li>
                       <li>any other actions taken by My Electrician.</li>
                   </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">5.4</td>
                <td class="grid justify top-line" colspan="10">
                   Where My Electrician has previously been directed by the Installation Company to make a payment in a particular manner, My Electrician is entitled to assume that future payments may be made in the same manner until the Installation Company informs My Electrician otherwise.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">5.5.1</td>
                <td class="grid justify top-line" colspan="10">
                   The Installation Company must submit any payment claim to My Electrician:
                   <ol style="list-style-type:lower-alpha">
                       <li>in such format as My Electrician may from time to time notify the Installation Company in writing;</li>
                       <li>on or before 4:00pm on any Monday that is not a public holiday in Victoria.</li>
                   </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">5.5.2</td>
                <td class="grid justify top-line" colspan="10">
                   <ol style="list-style-type:lower-alpha">
                       <li>Any payment claims shall be paid within fourteen (14) days reckoned from the first Monday after the date on which the Installation Company lodges the payment claim with My Electrician.</li>
                       <li>Despite anything to the contrary contained in this Agreement unless and until the Installation Company complies with clause 2.4 of this Agreement it is not entitled to and My Electrician is not obliged to make any payment to the Installation Company for any transaction in respect of which the requirements of clause 2.4 have not been met.</li>
                   </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">5.5.3</td>
                <td class="grid justify top-line" colspan="10">
                   Payment rates, changes to certificate pricing and schedule updates will change from time to time and will be notified to the Installation Company in terms of MOP. By continuing to use MOP the Installation Company by its conduct accepts such changes.
                </td>
            </tr>
            
        </table>
        <div class="footer">
            <p>Page 10 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 10-->

    <!--page 11-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    6.
                </td>
                <td class="grid" colspan="11">
                    Third party requests and directions:
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">6.1</td>
                <td class="grid justify top-line" colspan="10">
                   In the course of running its business My Electrician may interact with third parties including Certificate Creation Partners, Regulators and Financiers.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">6.2</td>
                <td class="grid justify top-line" colspan="10">
                   As a result of the interactions described in clause 6.1, or otherwise, a third party may:
                   <ol style="list-style-type:lower-alpha">
                       <li>make requests of and issue directions to My Electrician;</li>
                       <li>bring proceedings against My Electrician; or</li>
                       <li>otherwise impose costs on My Electrician.</li>
                   </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">6.3</td>
                <td class="grid justify top-line" colspan="10">
                    Where the actions described in clause 6.2 relate to the Installation Company’s actions (or failure to act) or those of its subcontractors, customers or associates, the Installation Company must:
                   <ol style="list-style-type:lower-alpha">
                       <li>provide any assistance that My Electrician may require in order to respond to or comply with the Regulators’ requests and directions;</li>
                       <li>indemnify My Electrician for all its costs (both direct and indirect) associated with responding to and complying with the requests and directions (for the avoidance of doubt, these costs may include costs associated with the procurement of professional services including consulting, legal, tax, accounting, and engineering).</li>
                   </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    7.
                </td>
                <td class="grid" colspan="11">
                    Breach and termination:
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">7.1</td>
                <td class="grid justify top-line" colspan="10">
                   A Party will notify the other if it is in default of this Agreement.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">7.2</td>
                <td class="grid justify top-line" colspan="10">
                   A Party is in default of this Agreement if it:
                    <ol style="list-style-type:lower-alpha">
                       <li>fails to comply with any term of this Agreement; or</li>
                       <li>becomes Insolvent.</li>
                   </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">7.3</td>
                <td class="grid justify top-line" colspan="10">
                   If a Party (Defaulting Party) is in default of this Agreement, the other Party (Injured Party) is entitled to:
                    <ol style="list-style-type:lower-alpha">
                       <li>terminate this Agreement immediately by written notice; or</li>
                       <li>recover damages in respect of its losses; or</li>
                       <li>both a) and b).</li>
                   </ol>
                </td>
            </tr>
            
        </table>
        <div class="footer">
            <p>Page 11 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 11-->

    <!--page 12-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">7.4</td>
                <td class="grid justify top-line" colspan="10">
                   If the Defaulting Party does not pay any damages within fourteen (14) days of the Injured Party making a written request for payment, interest will accrue at 10% per annum or the current Penalty Interest Rate (whichever is the greater) on the damages from the date of default until the damages are paid.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">7.5</td>
                <td class="grid justify top-line" colspan="10">
                  If the Injured Party holds any monies, Environmental Certificates or other property, rights or interests on behalf of the Defaulting Party or is obliged to make payments to the Defaulting Party, then the Injured Party is entitled to utilise these items to satisfy any damages claimed under this Agreement. This right is additional to the rights created elsewhere in this Agreement.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">7.6</td>
                <td class="grid justify top-line" colspan="10">
                  Either Party may Terminate this Agreement by thirty (30) days written notice.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">7.7</td>
                <td class="grid justify top-line" colspan="10">
                  If this Agreement is terminated then the Parties’ obligations under clauses 2.2(d)-(j), 2.3(a)-(b), 3.8, 5, 7, 8, 10, 11, 12, 13, 15 and 16 remain in place.
                </td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    8.
                </td>
                <td class="grid" colspan="11">
                    Non-disclosure:
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">8.1</td>
                <td class="grid justify top-line" colspan="10">
                  The Parties may not disclose information relating to or shared under this Agreement to any person except:
                  <ol style="list-style-type:lower-alpha">
                      <li>to the extent that it is already in the public domain;</li>
                      <li>with the written consent of the other Party;</li>
                      <li>to its officers, employees and professional advisers; or</li>
                      <li>as required by an applicable law or Code of Conduct after first consulting (to the extent lawful and reasonably practical) with the other Party about the form and content of the disclosure.</li>
                  </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">8.2</td>
                <td class="grid justify top-line" colspan="10">
                    Where permitted disclosures are made by a Party on any basis other than clause 8.1 (a), they will use reasonable endeavours to ensure the disclosed material is kept confidential by the Party to whom it has been shared.
                </td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    9.
                </td>
                <td class="grid" colspan="11">
                    Representations and warranties
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">9.1</td>
                <td class="grid justify top-line" colspan="10">
                    In respect of this Agreement the Installation Company represents and warrants that:
                    <ol style="list-style-type:lower-alpha">
                      <li>all information, documents and material provided to My Electrician are and will be accurate and complete; and</li>
                      <li>it will deal with My Electrician in good faith and respond promptly to Solution Provider reasonable requests for additional information or other support that may be required.</li>
                  </ol>
                </td>
            </tr>
            
        </table>
        <div class="footer">
            <p>Page 12 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 12-->

    <!--page 13-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    10.
                </td>
                <td class="grid" colspan="11">
                    Relationship: <br>
                    The Installation Company is not an agent, partner or joint venturer nor is it authorised to act on behalf or bind My Electrician or its nominated Certificate Creation Partner and hereby indemnifies My Electrician or its nominated Certificate Creation Partner in relation to all and any liability or responsibility arising from or connected in any manner whatsoever to the Installation Company’s dealings with System Owners.
                </td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    11.
                </td>
                <td class="grid" colspan="11">
                    Confidentiality:
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">11.1</td>
                <td class="grid justify top-line" colspan="10">
                    The Installation Company must keep confidential during and after the end of this Agreement the following information whether or not it is in material electronic, digital or machine readable or other form:
                    <ol style="list-style-type:lower-alpha">
                      <li>all confidential information including but not limited to intellectual property, trade secrets, confidential know-how (and the software) relating to this Agreement, MOP, data contained within MOP and the Assignment Agreement;</li>
                      <li>those parts of all notes and other records prepared by the Installation Company based on or incorporating the information referred to in paragraphs (a) and (b) above; and</li>
                      <li>all copies of the information and those parts of the notes and other records referred to in paragraph (a), (b) and (c).</li>
                  </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">11.2</td>
                <td class="grid justify top-line" colspan="10">
                    The Installation Company must use Confidential Information solely and exclusively for the purpose of complying with its obligations under this Agreement and must not whether directly or indirectly allow the Confidential Information to be used or disclosed for any other purpose.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">11.3</td>
                <td class="grid justify top-line" colspan="10">
                    The Installation Company may disclose Confidential Information only to those of its officers, employees and agents who:
                    <ol style="list-style-type:lower-alpha">
                      <li>have a need to know (and only to the extent that each has a need to know); and</li>
                      <li>have been directed to keep Confidential Information.</li>
                  </ol>
                </td>
            </tr>
            
        </table>
        <div class="footer">
            <p>Page 13 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 13-->

    <!--page 14-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">11.4</td>
                <td class="grid justify top-line" colspan="10">
                    The Installation Company must at its own costs:
                    <ol style="list-style-type:lower-alpha">
                      <li>ensure at all times that each person to whom Confidential Information has been disclosed complies with the agreement contemplated by paragraph 11;</li>
                      <li>notify My Electrician immediately if it becomes aware of a suspected or actual breach of the Confidentiality Agreement;</li>
                      <li>take all steps to prevent or stop the suspected or actual breach;</li>
                      <li>comply with any direction issued by My Electrician from time to time regarding the enforcement of Confidentiality Agreement (including but not limited to starting, conducting and settling enforcement proceedings);</li>
                      <li>assign any Confidential Agreement to My Electrician at My Electrician’s request.</li>
                  </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">11.5</td>
                <td class="grid justify top-line" colspan="10">
                    The obligations of confidentiality under this Agreement do not extend to information that (whether before or after this Agreement is executed):
                    <ol style="list-style-type:lower-alpha">
                      <li>is rightfully known to or is in the possession or control of the Installation Company and not subject to an obligation of confidentiality on the Installation Company; or</li>
                      <li>is public knowledge (otherwise than as a result of a breach of this Agreement).</li>
                  </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">11.6</td>
                <td class="grid justify top-line" colspan="10">
                    If the Installation Company is required by law to disclose any Confidential Information to a third party (including but not limited to a government authority):
                    <ol style="list-style-type:lower-alpha">
                      <li>before doing so it must notify My Electrician in writing; and</li>
                      <li>it must not do so until My Electrician has had a reasonable opportunity to take any steps that My Electrician considers necessary to protect the confidentiality of that information.</li>
                  </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">11.7</td>
                <td class="grid justify top-line" colspan="10">
                    The Installation Company acknowledges that its knowledge concerning MOP and the intellectual property comes from My Electrician.
                </td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    12.
                </td>
                <td class="grid" colspan="11">
                    Non-dealing with users of the System:<br>
                    For the purposes of this clause a user of the System (“the User”) means every person or entity who uses the System or whose details or information is recorded or appears in the System or any part thereof but excludes a Certificate Creation Partner and excludes a person or entity whose details or information is in the System solely by reason of being a System Owner with whom the Installation Company first transacted any business activities.
                </td>
            </tr>
            
        </table>
        <div class="footer">
            <p>Page 14 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 14-->

    <!--page 15-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">12.1</td>
                <td class="grid justify top-line" colspan="10">
                    The Installation Company must not:
                    <ol style="list-style-type:lower-alpha">
                        <li>
                            other than as permitted by this Agreement engage in the business:
                            <ol syle="list-style-type:lower-roman">
                                <li>to secure directly or indirectly from any User Environmental Certificates;</li>
                                <li>to supply directly or indirectly software systems for Environmental Certificate creation for any User.</li>
                            </ol>
                        </li>
                        <li>
                            from the date of ending or expiration of this Agreement for a period of two (2) years within the State of Victoria:
                            <ol syle="list-style-type:lower-roman">
                                <li>engage in the business of or to secure directly or indirectly from any User Environmental Certificates;</li>
                                <li>
                                    directly or indirectly solicit, canvass, approach or accept any approach from any person who was at any time during the last twelve (12) months of the Agreement:
                                    <ol syle="list-style-type:lower-alpha">
                                        <li>a User;</li>
                                        <li>a referral contact of My Electrician;</li>
                                        <li>any System Owner whose information appears on MOP other than a System Owner whose details are first entered into MOP solely as a result of the provision of services by the Installation Company of the nature referred to in Introduction paragraph C to such Service Owner.</li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                  </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">12.2</td>
                <td class="grid justify top-line" colspan="10">
                    In this clause “engaging” means to participate, assist or otherwise be directly or indirectly involved as a member, shareholder, unit holder, director, consultant, advisor, contractor, principal, agent, manager, employee, beneficiary, partner, associate, trustee or financier.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">12.3</td>
                <td class="grid justify top-line" colspan="10">
                    The Installation Company acknowledges that each of the prohibitions and restrictions contained in this Clause 12:
                </td>
            </tr>
            
        </table>
        <div class="footer">
            <p>Page 15 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 15-->

    <!--page 16-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">&nbsp;</td>
                <td class="grid justify top-line" colspan="10">
                    &nbsp;
                    <ol style="list-style-type:lower-alpha">
                        <li>
                            should be read and construed and will have the effect as a separate, severable and independent prohibition or restriction and will be enforceable accordingly; and
                        </li>
                        <li>
                            is reasonable as to period, territory or limitations and subject matter in order to protect the legitimate commercial interest of My Electrician.
                        </li>
                    </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    13.
                </td>
                <td class="grid" colspan="11">
                    Costs:
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">13.1</td>
                <td class="grid justify top-line" colspan="10">
                    Each Party is responsible for its own costs and expenses in connection with and incidental to the preparation and execution of this Agreement.
                </td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    14.
                </td>
                <td class="grid" colspan="11">
                    Goods and Services Tax:
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">14.1</td>
                <td class="grid justify top-line" colspan="10">
                    To the extent that GST is applicable to the transactions contemplated by this Agreement, the Parties agree to apply it in a manner consistent with any public rulings or other explanatory material published by the Australian Tax Office.
                </td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    15.
                </td>
                <td class="grid" colspan="11">
                    Dispute resolution:
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">15.1</td>
                <td class="grid justify top-line" colspan="10">
                    If a dispute arises between the Parties under this Agreement or Other Agreements, the following steps must be taken by the Parties prior to taking any action to commence legal proceedings:
                    <ol style="list-style-type:lower-alpha">
                        <li>the aggrieved Party must provide a written notice to the other Party specifying their concerns.</li>
                        <li>The Party receiving a notice under clause 15.1(a) must provide a written response within fourteen (14) days.</li>
                        <li>If the aggrieved party is not satisfied with a response provided under clause 15.1(b) then they must provide a further notice in writing to the other Party requesting that a meeting be held between the chief executives of each Party.</li>
                        <li>The meeting between the chief executives must be held within fourteen (14) days of the other Party receiving a notice under clause 15.1(c).</li>
                        <li>If the dispute remains unresolved after the meeting described in clause 15.1(d) then either Party may by notice in writing to the other direct that the matters raised in the documents referred to in clauses 15.1(a) and 15.1(b) be resolved by mediation.</li>
                    </ol>
                </td>
            </tr>
            
        </table>
        <div class="footer">
            <p>Page 16 of 17 </p>
        </div>
        <div class="pagebreak"></div>
    </div>
    <!-- close page 16-->

    <!--page 17-->
    <div class="content">
        <table style="width:100%">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">&nbsp;</td>
                <td class="grid justify top-line" colspan="10">
                    &nbsp;
                    <ol style="list-style-type:lower-alpha" start="5">
                        <li>A party receiving a notice under clause 15.1(e) must co-operate with the other Party to hold a mediation within fourteen (14) days of receipt of the notice.</li>
                        <li>If the mediation concludes and the dispute remains unresolved then either Party may bring legal proceedings in respect of the dispute as specified in the documents referred to in clauses 15.1(a) and 15.1(b).</li>
                        <li>If a Party does not comply with the requirements of this clause 15 then the other party may bring legal proceedings immediately notwithstanding that all of the steps set out in this clause have not yet been completed.</li>
                    </ol>
                </td>
            </tr>
            <tr>
                <td class="grid center" colspan="1">
                    16.
                </td>
                <td class="grid" colspan="11">
                    General
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">16.1</td>
                <td class="grid justify top-line" colspan="10">
                    The invalidity of any part or provision of this Agreement or Other Agreement will not affect the enforceability of any other part or provision of this Agreement.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">16.2</td>
                <td class="grid justify top-line" colspan="10">
                    The Parties will sign and execute all assurances, documents and deeds and do such deeds, acts and things as are required by the provisions of this Agreement to give effect to it.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">16.3</td>
                <td class="grid justify top-line" colspan="10">
                    This Agreement is governed by and construed in accordance with the laws applicable in Victoria and the Parties submit to the jurisdiction of the Courts in that State.
                </td>
            </tr>
            <tr>
                <td class="grid center top-line" colspan="2">16.4</td>
                <td class="grid justify top-line" colspan="10">
                    This Agreement is intended to be legally binding upon the parties. For the purpose of this Clause 16.4 the Parties include but is not limited to legal successors, agents, assigns, related entities and subcontractors and members, shareholders, unit holders, directors, consultants, advisors, contractors, principals, agents, managers, employees, beneficiaries, partners, associates, trustees or financiers of each of the parties.
                </td>
            </tr>
        </table>
        <div class="footer">
            <p>Page 17 of 17 </p>
        </div>
    </div>
    <!-- close page 17-->


   
    
</div>
    

</body>
</html>