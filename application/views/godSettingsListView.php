<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Setting List</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="box_label text-center">
                            <input type="hidden" name="action" id="action">
                            <input name="action" id="action" type="hidden">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>REGISTERED COMPANIES</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <!--page user-->
                            <div class="page" id="">
                                <div class="row">
                                    <div class="col-sm-4 hidden-xs">
                                        <img src="<?= $logo ?>" style="height:80px" alt="">
                                    </div>
                                    <div class="col-sm-4">
                                        <h2 class="text-center bold">VEEC 21</h2>
                                        <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                    </div>
                                    <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                    <div class="col-sm-3 hidden-xs">
                                        <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-xs-12 box_desc">
                                            <label>FILTER COMPANIES BY APPLICATION</label>
                                            <select id="filterAppId">
                                              <option value="0">Show All</option>
                                              <?php foreach ($appAdmins as $app): ?>
                                                <option value="<?= $app->cid ?>"><?= $app->electricians_company_name ?></option>
                                              <?php endforeach ?>
                                            </select>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-xs-12 box_desc">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="table-responsive" style="overflow-x:auto;max-width:100%">
                                                    <table id="company_list" cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
                                                        <thead>
                                                            <tr class="skyblue">
                                                                <th>APPLICATION</th>
                                                                <th>COMPANY</th>
                                                                <th style="width:5%"></th>
                                                                <!--<th style="width:5%"</th>-->
                                                                <th style="width:5%"</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($items as $item): ?>
                                                                <tr data-appId="<?= $item->appId ?>">
                                                                    <td><?= $item->appCompany ?></td>
                                                                    <td><?= $item->electricians_company_name ?></td>
                                                                    <td class="text-center" >
                                                                        <a href="<?= site_url('settingscontroller/edit/'.$item->cid) ?>" class="btn btn-sm btn-warning">SETTINGS</a>
                                                                    </td>
                                                                    <!--
                                                                    <td>
                                                                        <button type="button" class="btn btn-sm btn-warning" onclick="addUser(<?= $item->cid ?>)">ADD USER</button>
                                                                    </td>
                                                                -->
                                                                    <td class="text-center" >
                                                                        <a href="<?= site_url('settingscontroller/delete_company/'.$item->cid.'/confirm') ?>" class="btn btn-sm btn-danger">DELETE</a>
                                                                        <!--<td><a href="<?= site_url('settingscontroller/delete_item/'.$it['item_id'].'/confirm') ?>" class="btn btn-sm btn-danger">DELETE</a></td>-->
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr class="skyblue">
                                                                <th></th>
                                                                <th></th>
                                                                <th class="text-center">
                                                                	<?php if($current_user['is_admin'] == 1): ?>
                                                                		<a href="<?= site_url('settingscontroller/invite') ?>" 
                                                                		class="btn btn-sm btn-warning">INVITE NEW CONTRACTOR</a>
                                                                	<?php endif; ?>
                                                                </th>
                                                                <th class="text-center">
                                                                	<a 
                                                                		href="<?= isset($btn_add) ? $btn_add['href']:site_url('settingscontroller/create') ?>" 
                                                                		class="btn btn-sm btn-warning"><?= isset($btn_add) ? $btn_add['text']:'ADD NEW CONTRACTOR' ?>
                                                                	</a>
                                                                </th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--close user-->
                        </div>
                    </div>
                        
                        
                </div>
            </div>

            <form id="user" class="form-horizontal" action="" role="form" method="post">
            <div class="modal fade" id="addUser">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Add User For Your Company</h4>
                        </div>

                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="email">Email</label>
                                <div class="col-lg-8">
                                    <input type="hidden" name="id" class="form-control" id="id">
                                    <input type="hidden" name="cid" />
            						<input type="email" name="email" class="form-control validate[required]" id="email" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="password">Password</label>
                                <div class="col-lg-8">
                                    <input type="password" name="password" class="form-control validate[required]" id="password" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="confirm_password">Confirm Password</label>
                                <div class="col-lg-8">
                                    <input type="password" class="form-control validate[required,equals[password]] " name="confirm_password" id="confirm_password" placeholder="Confirm Password">
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning reset" data-dismiss="modal">Close</button>
                            <button id="save_user" type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            </form>    
        </div>


        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript">
          function addUser (cid) {
          	$('#addUser').find('[name="cid"]').val(cid)
          	$('#addUser').modal('show')
          }
          $('#filterAppId').change(function () {
            var appId = $(this).val()
            if (appId==0) $('#company_list tbody tr').show()
            else {
              $('#company_list tbody tr').hide()
              $('#company_list tbody tr[data-appId="'+appId+'"]').show()
            }
          })
        </script>
    </body>
</html>