<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function site_url($uri = '')
	{
		$CI =& get_instance();
		$site_url = $CI->config->site_url($uri);
		$current_user = $CI->session->all_userdata();
		if (isset($current_user['uid']) && isset($current_user['cid']) && $uri != '') {
		  if ($current_user['is_admin'] == 4) return $site_url;
			$CI->load->model('settingsmodel');
			$company = $CI->settingsmodel->retrieve($current_user['cid']);
			$appCompany = $CI->settingsmodel->retrieve($company['appId']);
			$CI->load->library('sluggify');
			$companySlug = $CI->sluggify->slug($appCompany['electricians_company_name']);
			$split = explode('index.php/', $site_url);
			$site_url = $split[0] . "index.php/$companySlug/" . $split[1];
		}
		return $site_url;
	}