<?php

class stockallocationmodel extends getveetModel {
	
    function __construct() {
        parent::__construct();
		date_default_timezone_set('Australia/Victoria');
        $this->table = 'stock_allocation';
        $this->id = 'stock_allocation_id';
    }
	
	function get_list($conditions){
		$this->db->join('stock','stock.stock_id=stock_allocation.stock_id');
		$this->db->join('cd_product','stock.item_id=cd_product.item_id');
		$this->db->select('stock_allocation.time');
		$this->db->select('cd_product.id');
		$this->db->select('stock_allocation.stock');
		$this->db->order_by('stock_allocation.time','DESC');
		return parent::get_list($conditions);
	}
	
	function save($data){
		foreach(array('installer_id','item_id') as $related){
			$$related = $data[$related];
			unset($data[$related]);
		}
		
		$exists = $this->db->get_where('stock',array(
			'installer_id' => $installer_id,
			'item_id' => $item_id
		))->result();
		if(count($exists)<1){
			$this->db->insert('stock',array(
				'stock_id' => null,//time(),
				'installer_id' => $installer_id,
				'item_id' => $item_id,
				'stock' => $data['stock']
			));
			$data['stock_id'] = $this->db->insert_id();
		}else{
			$data['stock_id'] = $exists[0]->stock_id;
			$this->db
				->where('stock_id', $data['stock_id'])
				->set('stock','stock+'.$data['stock'], false)
				->update('stock');
		}
		
		$date = date('Y-m-d');
		if (isset ($data['date'])) {
			$date = $data['date'];
			unset ($data['date']);
		}
		$data['time'] = strtotime("$date 00:00:00");
		
		if(isset($data['stock_allocation_id'])) return parent::save($data);
		$data['stock_allocation_id'] = null;
		$this->db->insert('stock_allocation',$data);
		return $this->db->insert_id();
	}
	
}