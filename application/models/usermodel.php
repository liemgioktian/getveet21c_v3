<?php

class usermodel extends getveetModel{
    function __construct() {
        parent::__construct();
        $this->table = 'user';
        $this->id = 'uid';
    }
    
    function save($data) {
        if(!isset($data['uid'])){
        	$data['active']= isset ($data['active']) ? $data['active'] : 1;
            $exist = $this->get_list(array('email'=>$data['email']));
            if(count($exist)>0) return false;
        }
        parent::save($data);
    }
    
    function send_new_password($user) {
        if (is_object($user)) $user = (array) $user;
				$uid = $user['uid'];
				$encpwd = $user['password'];
				$link = site_ur("authcontroller/reset_password/$uid/$encpwd");
        $this->getveet_send_mail($user['email'], 
                "Please go to following page to reset your password : $link"
                , "Reset password request");
    }
		
		function get_list($conditions) {
			$this->db->select('user.*');
			$this->db->select('company.electricians_company_name');
			$this->db->join('company','user.cid = company.cid');
			return parent::get_list($conditions);
		}
    
    function god_get_list($conditions) {
      $this->db->select('user.*');
      $this->db->select('company.electricians_company_name');
      $this->db->select('app.electricians_company_name as appCompany');
      $this->db->join('company','user.cid = company.cid');
      $this->db->join('company as app','user.appId = app.cid');
      return parent::get_list($conditions);
    }

}
