<?php

class evidence34model extends getveetModel {
  
  function __construct() {
    parent::__construct();
    $this->table = 'evidence34';
    $this->id = 'eid';
  }

  function deleteTree ($eid) {
    $evi = parent::retrieve($eid);
    if (!$evi) return;
    $childs = parent::get_list(array('parent'=>$eid));
    
    // foreach ($childs as $child) $this->deleteTree($eid); NOT WORKS YET
    foreach ($childs as $child) {
      if (file_exists(EVIDENCE34_DIR . $child->name)) unlink(EVIDENCE34_DIR . $child->name);
      parent::delete($child->eid);      
    }
    // DELETE SINGLE NEST 1st, for a while. until LINE14 works
    
    if (file_exists(EVIDENCE34_DIR . $evi['name'])) unlink(EVIDENCE34_DIR . $evi['name']);
    return parent::delete($eid);
  }
}