<?php

class filemodel extends getveetModel {
    function __construct() {
        parent::__construct();
        $this->table = 'file';
        $this->id = 'fid';
    }
    
    function get_list($conditions, $condition_like = array(), $sort = array()){
    	
			if ($this->session->userdata('is_admin') == 3) {
				
				$this->db->order_by("file.processed","ASC");
				$this->db->order_by("file.checked_date","ASC");
				$this->db->order_by("file.admin3_status","ASC");

				
				
				
				/*$this->db->order_by("file.admin3_status","ASC","file.completed_date", "DESC");*/
				
				
			}
			
    		$this->db->select('file.*, user.*, company.electricians_company_name');
        	$this->db->join('user','file.uid=user.uid','LEFT');
			$this->db->join('company','user.cid=company.cid','LEFT');
			if (empty($sort)) {
	        	$this->db->order_by('file.complete','ASC');
				$this->db->order_by('file.clientNumber','DESC');					
			} else foreach ($sort as $field) $this->db->order_by($field,'DESC');

		foreach($condition_like as $field => $value) $this->db->like($field, $value);
        return parent::get_list($conditions);
    }
    
    function delete($id){
        $file = $this->retrieve($id);
        foreach (json_decode($file['data'], true) as $key => $value){
            if(strpos($value, '@file@') && file_exists($value)) unlink ($value);
            if($key == 'E153' && file_exists($value)) unlink ($value);
            if($key == 'E225' && file_exists($value)) unlink ($value);
        }
        parent::delete($id);
    }
    
    function getClientNumber(){
        $prefix = '21C';
        $number = $this->db->select_max('clientNumber')->get($this->table)->row_array();
		$number = $number['clientNumber'];
        $suffix = str_replace($prefix, '', $number);
        $suffix++;
        while (strlen($suffix) < 7) $suffix = "0$suffix";
        return $prefix.$suffix;
    }
	
	function get_where_in($ids){
		return 
			$this->db
			->where_in('fid',$ids)
			->get($this->table)->result();
	}
	
	function get_weekly_report($conditions){
		if(isset($conditions['cid'])) {
			$this->db->join('user', 'user.uid=file.uid');
			$this->db->where("user.cid=".$conditions['cid']);
			unset($conditions['cid']);
		}
		return parent::get_list($conditions); 
	}
	
	function get_file_company ($fid) {
		return $this->db
		->select('*')
		->from('file')
		->join('user', 'user.uid=file.uid', 'LEFT')
		->join('company', 'user.cid=company.cid', 'LEFT')
		->where('file.fid', $fid)
		->get()
		->row_array();
	}
	
	function save ($data) {
		if (isset($data['fid'])) {
			if (!isset($data['cid'])) $data['cid'] = $this->session->userdata('cid');
			if (!isset($data['appId'])) $data['appId'] = $this->session->userdata('appId');
			if (!isset($data['appAdmin'])) $data['appAdmin'] = $this->session->userdata('is_admin') == 1 ? 1 : 0;
		}
		return parent::save($data);
	}
}
