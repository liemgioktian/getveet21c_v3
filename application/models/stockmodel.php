<?php

Class stockmodel extends getveetModel {
    function __construct() {
        parent::__construct();
        $this->table = 'stock';
        $this->id = 'stock_id';
    }
	
	function get_stock_list($since, $until, $installer_id){
		return $this->db->query(
			"SELECT stock_id as sid, cd_product.id as item_name,
				
				(SELECT SUM(stock_allocation.stock) FROM stock_allocation WHERE stock_allocation.stock_id = sid
				AND stock_allocation.time >= $since AND stock_allocation.time <= $until) as collected,
			    
			    (SELECT SUM(stock_usage.stock) FROM stock_usage WHERE stock_usage.stock_id = sid
				AND stock_usage.time >= $since AND stock_usage.time <= $until) as installed
				
			FROM stock JOIN cd_product ON stock.item_id=cd_product.item_id WHERE installer_id=$installer_id"
		)->result();
	}
	
	function getItems($installer_id){
		return $this->db
		->select('cd_product.id,stock_id')
		->join('cd_product','cd_product.item_id=stock.item_id')
		->where('installer_id',$installer_id)
		->get('stock')->result();
	}
	
	function save($data){
		if(isset($data[$this->id])) return parent::save($data);
		$data[$this->id] = null;
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
}