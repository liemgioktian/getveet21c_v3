<?php

class product34model extends getveetModel {
  
  function __construct() {
    parent::__construct();
    $this->table = 'product34';
    $this->id = 'pid';
  }

  function getJson ($conditions) {
    $this->db
      ->select("$this->table.*")
      ->select("pid as id", false)
      ->select("CONCAT (brand, ' ', modelNumber) as text", false);
    return parent::get_list($conditions);
  }
}