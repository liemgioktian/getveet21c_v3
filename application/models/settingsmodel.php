<?php

class settingsmodel extends getveetModel {
	
	var $cid;
	
    function __construct() {
        parent::__construct();
		date_default_timezone_set('Australia/Victoria');
        $this->table = 'company';
        $this->id = 'cid';
		$this->cid = $this->session->userdata('cid');
    }
    
	function _change_table($table){
		$this->table = $table;
		switch ($table) {
			case 'company':
				$this->id = 'cid';
				break;
			case 'installer':
				$this->id = 'iid';
				break;
			case 'item':
				$this->id = 'item_id';
				break;
		}
	}
	
	function save($data){
		$installers = $data['installer'];
		$items = $data['item'];
		$company = $data;
		unset($company['installer']);
		unset($company['item']);
		
		$this->_change_table('company');
		if(!empty($_FILES['company_logo']['name'])){
			$logo_location = FCPATH.'asset/css/images/';
			$newlogoname = $_FILES['company_logo']['name'];
			$logo_ext = end(explode('.', $newlogoname));
			$newlogoname = time().'.'.$logo_ext;
			
			if(move_uploaded_file($_FILES['company_logo']['tmp_name'],$logo_location.$newlogoname)){
				$company['logo'] = $newlogoname;
			}
			
			if(isset($company['cid'])){
				$oldlogo = parent::retrieve($company['cid']);
				$oldlogo = $oldlogo['logo'];
				if(!empty($oldlogo) && file_exists($logo_location.$oldlogo)) unlink($logo_location.$oldlogo);
			}
		}
		$cid = parent::save($company);
		
		$this->_change_table('installer');
		$index = 0 ;

		// validate installer, make sure no same email registered more than once in different company
		$registered_installer_email_another_company = array();
		$db_installers = $this->db
			->where('cid !=',$cid)
			->get('installer')
			->result();
		foreach($db_installers as $another) $registered_installer_email_another_company[] = $another->email_address; 

		foreach($installers as $in){
			$index++;
			if(!empty($in['first_name'])){
				if (in_array($in['email_address'], $registered_installer_email_another_company)) continue;
				$in['cid'] = $cid;
				$renewal_date = new DateTime($in['renewal_date']);
				$in['renewal_date'] = !empty($in['renewal_date']) ? $renewal_date->format('Y-m-d') : '';
				$expiry_date = new DateTime($in['expiry_date']);
				$in['expiry_date'] = !empty($in['expiry_date']) ? $expiry_date->format('Y-m-d') : '';
				
				if(!empty($_FILES['installer']['name'][$index]['photo'])){
					$new_photo = $_FILES['installer']['name'][$index]['photo'];
					$settings_app_installer_photos = FCPATH."settings_app_installer_photos/";
					if(move_uploaded_file($_FILES['installer']['tmp_name'][$index]['photo'], $settings_app_installer_photos.$new_photo)){
						$in['photo'] = $new_photo;
						
						if(isset($in['iid'])){
							$oldphoto = parent::retrieve($in['iid']);
							$oldphoto = $oldphoto['photo'];
							if(!empty($oldphoto) && file_exists($settings_app_installer_photos.$oldphoto))
								unlink($settings_app_installer_photos.$oldphoto);
						}
					}
				}

        if(!empty($_FILES['installer']['name'][$index]['photo2'])){
          $new_photo = $_FILES['installer']['name'][$index]['photo2'];
          $settings_app_installer_photos = FCPATH."settings_app_installer_photos/";
          if(move_uploaded_file($_FILES['installer']['tmp_name'][$index]['photo2'], $settings_app_installer_photos.$new_photo)){
            $in['photo2'] = $new_photo;
            
            if(isset($in['iid'])){
              $oldphoto = parent::retrieve($in['iid']);
              $oldphoto = $oldphoto['photo2'];
              if(!empty($oldphoto) && file_exists($settings_app_installer_photos.$oldphoto))
                unlink($settings_app_installer_photos.$oldphoto);
            }
          }
        }
				$in['appId'] = $company['appId'];
				parent::save($in);
			}
		}
		
		$this->_change_table('item');
		foreach($items as $it){
			$it['cid'] = $cid;
			if(!empty($it['id'])) parent::save($it);
		}
		
		return $cid;
	}
	
	function retrieve($cid){
		$this->_change_table('company');
		$settings = parent::retrieve($cid);
		$settings['company_full_address'] = $settings['company_address'].' '.$settings['suburb'].' '.$settings['postal_code'];
		
		$this->_change_table('installer');
		$installer = parent::get_list(array('cid'=>$cid));
		foreach($installer as $in){
			$in = (array) $in;
			$renewal_date = new DateTime($in['renewal_date']);
			$in['renewal_date'] = $in['renewal_date'] != '0000-00-00' ? $renewal_date->format('Y-m-d') : '';
			$expiry_date = new DateTime($in['expiry_date']);
			$in['expiry_date'] = $in['expiry_date'] != '0000-00-00' ? $expiry_date->format('Y-m-d') : '';
			
			$in['id'] = $in['first_name'].' '.$in['last_name'];
			$in['phone'] = $in['phone_number'];
			$in['email'] = $in['email_address'];
			$in["type"] = "A Class";
			$in['number'] = $in['license_number'];
			$in['issue'] = $in['renewal_date'];
			$in['expire'] = $in['expiry_date'];
			
			$settings['installer'][] = $in;
		}
		
		$this->_change_table('item');
		$settings['item'] = array();
		foreach(parent::get_list(array('cid'=>$cid)) as $item) $settings['item'][] = (array) $item;
		
		return $settings;
	}
	
	function delete_installer($iid){
		$this->_change_table('installer');
		$installer = parent::retrieve($iid);
		parent::delete($iid);
		$settings_app_installer_photos = FCPATH."settings_app_installer_photos/";
		if(!empty($installer['photo']) && file_exists($settings_app_installer_photos.$installer['photo']))
								unlink($settings_app_installer_photos.$installer['photo']);
    if(!empty($installer['photo2']) && file_exists($settings_app_installer_photos.$installer['photo2']))
                unlink($settings_app_installer_photos.$installer['photo2']);
		return $installer['cid'];
	}

	function delete_company($cid){
		$this->_change_table('company');
		$comp = parent::retrieve($cid);
		parent::delete($cid);
		return $cid;
	}
	
	function delete_item($item_id){
		$this->_change_table('item');
		$item = parent::retrieve($item_id);
		parent::delete($item_id);
		return $item['cid'];
	}
	
	function get_non_super_admin_companies(){
		return $this->db->query(
		"SELECT * FROM company WHERE cid NOT IN (SELECT cid FROM user WHERE is_admin=1)
		ORDER BY electricians_company_name ASC"
		)->result();
	}

	function get_super_admin_companies(){
		return $this->db->query(
		"SELECT * FROM company WHERE cid IN (SELECT cid FROM user WHERE is_admin=1)
		ORDER BY electricians_company_name ASC"
		)->result();
	}
		
	function client_company_price_policy($setting){
		$admin_c = $setting['c_certificate'];
		$admin_d = $setting['d_certificate'];
		$client_c = $setting['client_c_certificate'];
		$client_d = $setting['client_d_certificate'];
		$appId = $this->session->userdata('appId');
		// update all client company's price
		$this->db
			->set('c_certificate', $client_c)
			->set('d_certificate', $client_d)
			->where('appId', $appId)
			->where('appAdmin', 0)
			->update('company');
		
		// update price of saved form as draft
		$files = $this->db
			->where('appId', $appId)
			->where('lodge_draft', 'SAVE DRAFT')
			->get('file')
			->result();
		foreach ($files as $file) {
			$data = json_decode($file->data, true);
			$data['M51'] = $file->appAdmin == 0 ? $client_c : $admin_c;
			$data['M55'] = $file->appAdmin == 0 ? $client_d : $admin_d;
			$file->data = json_encode($data);
			$this->db
			->where('fid', $file->fid)
			->where('lodge_draft', 'SAVE DRAFT')
			->update('file', (array) $file);
		}
	}
	
	function update ($company) {
		return $this->db->where('cid', $company['cid'])->update('company', $company);
	}

  function god_get_list($conditions) {
    $this->db
      ->select('company.*')
      ->select('appAdmin.electricians_company_name as appCompany')
      ->from('company')
      ->join('company as appAdmin', 'company.appId = appAdmin.cid');
    
    if(count($conditions) > 2) foreach ($conditions as $field => $value) $this->db->where($field, $value);
    else $this->db->where($conditions);
    
    return $this->db->get()->result();
  }
}
