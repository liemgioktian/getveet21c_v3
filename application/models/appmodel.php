<?php

Class appmodel extends getveetModel {
		
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	function get_list($conditions) {
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where('appAdmin', 1);
		if(count($conditions) > 2)
			foreach ($conditions as $field => $value) $this->db->where($field, $value);
		else $this->db->where($conditions);
    return $this->db->get()->result();
	}
}
