<?php

class form34controller extends getveetController {

  function __construct() {
    parent::__construct();
    $this->load->model('form34Model');
    date_default_timezone_set('Australia/Victoria');
  }

  function index() {
    $form34s['items'] = $this->form34Model->getFileList();
    $this->loadView($form34s, 'form34ListView');
  }

  function create() {
    $post = $this->input->post();
    if ($post) {
      $fid = $this->form34Model->save(array('data'=>$post));
      exit("$fid");
    }
    $this->load->model('product34model');
    $param['product34'] = $this->product34model->getJson(array());
    $this->loadView($param,'34form');
  }

  function edit ($fid) {
    $post = $this->input->post();
    if ($post) {
      // echo'<pre>';print_r($post);die();
      $form34 = array (
        'fid' => $fid,
        'data' => $post
      );
      $fid = $this->form34Model->save($form34);
      exit("$fid");
    }
    $form34 = $this->form34Model->retrieve($fid);
    $form34['data'] = json_decode($form34['data'], true);
    // echo'<pre>';print_r($form34);die();
    $this->load->model('product34model');
    $form34['product34'] = $this->product34model->getJson(array());
    $this->loadView($form34,'34form');
  }

  function delete ($fid, $confirmed='confirmed') {
    if ($confirmed=='confirmed') {
      $this->form34Model->delete($fid);
      redirect(site_url('form34controller'));
    } else {
      $this->loadView (null, 'confirmationView');
    }
  }

  function subform () {
    $param['link21'] = site_url('evidence34controller/open');
    $param['text21'] = 'CREATE EVIDENCE';
    $param['icon21'] = base_url('asset/images/folder_open.png');
    $param['link34'] = site_url('form34controller/create');
    $param['text34'] = 'CREATE FORM';
    $this->loadView($param,'subform');
  }
}
