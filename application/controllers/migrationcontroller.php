<?php

class migrationcontroller extends getveetController {
		
	function __construct() {
		parent::__construct();
		if ($this->session->userdata('is_admin')!=4) die('u r unauthorized!');
  }
	
	function stock(){
		$query = "SELECT * FROM file WHERE clientNumber NOT IN (SELECT job_refference FROM stock_usage)";
		$limit = 0;
		foreach($this->db->query($query)->result() as $post){
			$limit++;
			$post = (array) $post;
			foreach(json_decode($post['data'], true) as $key => $value){
				$post[$key] = $value;
			}
			$installer_email = $post['Z127'];
			if(strlen($installer_email)<1) continue;
			$query = "SELECT * FROM installer WHERE email_address = '$installer_email'";
			$installers = $this->db->query($query)->result();
			if(count($installers) < 1) continue;
			else if(count($installers) > 1){
				$user = $this->db->get_where('user', array('uid' => $post['uid']))->row_array();
				$query .= " AND cid = ".$user['cid'];
				$installers = $this->db->query($query)->result();
				if(count($installers) > 1){
					$name = explode(' ', $post['C127']);
					$first_name = $name[0];
					$query .= " AND first_name = '$first_name'";
					$installers = $this->db->query($query)->result();
				}
			}
			$post['iid'] = $installers[0]->iid;
			$out_of_stock = $this->generate_stock_usage($post);
			$this->db->where('fid',$post['fid'])->set('out_of_stock', $out_of_stock)->update('file');
		}
	}

	function appadmin () {
		/* 	
		 * -- ALTER TABLE `file` ADD `cid` int(11) NOT NULL DEFAULT '0';
		 * -- ALTER TABLE `file` ADD `appAdmin` TINYINT(1) NOT NULL DEFAULT '0';
		 */
		$this->load->model(array('filemodel', 'usermodel', 'settingsmodel'));
		
		$users = array();
		foreach ($this->usermodel->get_list(array()) as $user) $users[$user->uid] = $user;
		
		$companies = array();
		foreach ($this->settingsmodel->get_list(array()) as $company) $companies[$company->cid] = $company;
		
		$forms = $this->db->get('file')->result();
		foreach ($forms as $form) {
			if (!isset ($users[$form->uid])) continue;
			$formUser = $users[$form->uid];
			$formCompany = $companies[$formUser->cid];
			$form->cid = $formCompany->cid;
			$form->appId = $formCompany->appId;
			$form->appAdmin = $formCompany->appAdmin;
			$this->filemodel->save((array)$form);
		}
		
	}
}