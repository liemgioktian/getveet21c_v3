<?php

class LeadController extends getveetController
{

    public function index()
    {
        if (!$this->session->userdata('uid')) {
            $this->load->view('loginView');
            return;
        }

        $this->load->model('settingsModel');

        $user = $this->session->all_userdata();

        $data = [
            'settings'  => $this->settingsModel->retrieve($user['cid']),
        ];
        $this->loadView($data, 'leadManagerView');
    }
}
