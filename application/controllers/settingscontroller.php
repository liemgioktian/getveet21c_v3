<?php

class settingscontroller extends getveetController {

    function __construct() {
        parent::__construct();
        $this->load->model('settingsModel');
        $this->load->library('sluggify');

    }

    function index() {
    	$param = array();
      $is_god = $this->session->userdata('is_admin') == 4 ? true : false;
			if ($this->session->userdata('is_admin')==1) {
				$param['company.appId'] = $this->session->userdata('appId');
			}
			$param['items'] =  $is_god ? 
                  			 $this->settingsModel->god_get_list($param) : 
                  			 $this->settingsModel->get_list($param);
      if ($is_god) $param['appAdmins'] = $this->settingsModel->get_list(array('company.appAdmin' => 1));
      $this->loadView($param, $is_god ? 'godSettingsListView' : 'settingsListView');
    }

    function create() {
        if($this->input->post()){
        	$post =  $this->input->post();
					$cid = $this->settingsModel->save($post);
					redirect(site_url('settingscontroller/edit/'.$cid));
        }
        $this->loadView(array(),'settingsView');
    }

    function edit($cid) {
    	if($this->input->post()){
    		$post =  $this->input->post();
    		if (isset($post['add_user'])) {
    			$this->load->model('userModel');
    			$this->userModel->save(array(
    				'cid' => $this->input->post('cid'),
    				'email' => $this->input->post('email'),
    				'password' => md5($this->input->post('password'))
					));
				} else {
						
					if(isset($post['client_c_certificate'])){
						$this->settingsModel->client_company_price_policy($post);
						unset($post['client_c_certificate']);
						unset($post['client_d_certificate']);
					}
					
					if (($_FILES['landing_header_image']['error'] == 0)) {
						$file = $_FILES['landing_header_image'];
						$ext = end(explode('.', $file['name']));
						$newfile = md5(microtime());
						$post['landing_header_image'] = $newfile . '.' . $ext;
						move_uploaded_file($file['tmp_name'], FCPATH . 'asset/css/images/' . $newfile .'.' . $ext);
					}

          if (($_FILES['landing_footer_image']['error'] == 0)) {
          	$file = $_FILES['landing_footer_image'];
            $ext = end(explode('.', $file['name']));
            $newfile = md5(microtime());
            $post['landing_footer_image'] = $newfile . '.' . $ext;
            move_uploaded_file($file['tmp_name'], FCPATH . 'asset/css/images/' . $newfile .'.' . $ext);
          }

          $post['lead_manager_enabled'] = isset($post['lead_manager_enabled']) ? 1 : 0;
          $post['cid'] = $cid;
          $this->settingsModel->save($post);
				}
      }
        
	    $data = $this->settingsModel->retrieve($cid);
			
			if ($data['appAdmin']==1) {
				$this->settingsModel->_change_table('company');
				$clients = $this->settingsModel->get_list(array('appId'=>$cid, 'cid <>'=>$cid));
				if (count($clients)>0) {
					$data['client_c_certificate'] = $clients[0]->c_certificate;
					$data['client_d_certificate'] = $clients[0]->d_certificate;
				}
			}
			
			$this->loadView($data,'settingsView');
    }

    function profit(){
    	$data = $this->settingsModel->get_non_super_admin_companies();
    	$data = (array)$data[0];
		$param['setting'] = $data;
		$this->loadView($param, 'profitView');
    }

    function delete_company($cid, $confirm = null){
    	if (is_null($confirm)) {
    		$list = $this->settingsModel->delete_company($cid);
    		redirect(site_url('settingscontroller'));
    	}else $this->loadView (null, 'confirmationView');

    }

	function delete_installer($iid, $confirm = null){
        if(is_null($confirm)){
            $cid = $this->settingsModel->delete_installer($iid);
            redirect(site_url('settingscontroller/edit/'.$cid));
        }else $this->loadView (null, 'confirmationView');
	}

	function delete_item($item_id, $confirm = null){
        if(is_null($confirm)){
            $cid = $this->settingsModel->delete_item($item_id);
            redirect(site_url('settingscontroller/edit/'.$cid));
        }else $this->loadView (null, 'confirmationView');
	}

	function settings_app(){
		$post = $this->input->post();
		if($post){
			$current_installers = $this->_get_settings_app();
			$current_installers = $current_installers['installer'];
			$settings_app = fopen(APPPATH.'config/settings_app.csv','w') or die("Can't open settings settings");
			foreach($post as $name => $value){
				if(is_array($value)){
					if($name == 'item')
						foreach($value as $key => $val){
							if(empty($val['id'])) unset($value[$key]);
						}
					else if($name == 'installer')
						foreach($value as $key => $val){
							$value[$key]['photo'] = '';
							if(empty($val['first_name'])) unset($value[$key]);// exclude installer who doesn't has firstname  from saving (including empty footer form)

							// set photo for included installer and has photo uploaded
							if(isset($value[$key]) && !empty($_settingsS['installer']['name'][$key]['photo'])){
								$new_photo = $_settingsS['installer']['name'][$key]['photo'];
								$settings_app_installer_photos = FCPATH."settings_app_installer_photos/";

								if(move_uploaded_settings($_settingsS['installer']['tmp_name'][$key]['photo'], $settings_app_installer_photos.$new_photo)){
									$value[$key]['photo'] = $new_photo;

									// remove old photo
									if(isset($current_installers[$key]['photo']) && !empty($current_installers[$key]['photo']) && settings_exists($settings_app_installer_photos.$current_installers[$key]['photo']))
										unlink($settings_app_installer_photos.$current_installers[$key]['photo']);
								}
							}

							// keep installer photo data in csv if no new photo uploaded
							if(isset($value[$key]) && empty($_settingsS['installer']['name'][$key]['photo']))
								if(isset($current_installers[$key]['photo']))$value[$key]['photo'] = $current_installers[$key]['photo'];
						}


					fputcsv($settings_app, array($name,json_encode($value)));
				}else fputcsv($settings_app, array($name,$value));
			}
			fclose($settings_app);

			if(!empty($_settingsS['company_logo']['name'])){
				$logo_location = FCPATH.'asset/css/images/logo_2.png';
				if(settings_exists($logo_location)) unlink($logo_location);
				move_uploaded_settings($_settingsS['company_logo']['tmp_name'],$logo_location);
			}

		}

		$this->loadView($this->_get_settings_app(),'settingsView');
	}

	function _get_settings_app(){
		$settings = fopen(APPPATH.'config/settings_app.csv',"r");
		$lines= array();
		while($line = fgetcsv($settings)) $lines[] = $line;
		fclose($settings);
		$param = array();
		foreach($lines as $var => $val)
			$param[$val[0]] = strpos($val[0], 'installer') === 0 || strpos($val[0], 'item') === 0 ? json_decode($val[1],true) : $val[1];
		$param['company_full_address'] = $param['company_address'].' '.$param['suburb'].' '.$param['postal_code'];
		if(isset($param['installer'])) foreach($param['installer'] as $key => $value){
			$param['installer'][$key]['id'] = $param['installer'][$key]['first_name'].' '.$param['installer'][$key]['last_name'];
			$param['installer'][$key]['phone'] = $param['installer'][$key]['phone_number'];
			$param['installer'][$key]['email'] = $param['installer'][$key]['email_address'];
			$param['installer'][$key]["type"] = "A Class";
			$param['installer'][$key]['number'] = $param['installer'][$key]['license_number'];
			$param['installer'][$key]['issue'] = $param['installer'][$key]['renewal_date'];
			$param['installer'][$key]['expire'] = $param['installer'][$key]['expiry_date'];
		}
		return $param;
	}

	function ajax_company_name_exists(){
		$ecn = $this->input->post('ecn');
		$companies = $this->settingsModel->get_list(array('electricians_company_name' => $ecn));
		print (count($companies));
	}

	function ajax_user_email_exists(){
		$email = $this->input->post('email');
		$this->load->model('userModel');
		$users = $this->userModel->get_list(array('email' => $email));
		print (count($users));
	}
	
	function invite() {
		$param = array();
		if ($this->input->post()) {
			$cid = $this->session->userdata('cid');
			$this->settingsModel->getveet_send_mail(
				$this->input->post('email'), 
				'Congratulations, you are invited as new company, <a href="'.site_url("signup/0/$cid").'">click here to signup</a>', 
				'Invitation for new company', null);
		}
		$this->loadView($param,'inviteAppAdminView');
	}
	
}
