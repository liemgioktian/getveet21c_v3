<?php

class LandingPageController extends getveetController
{
    public function landing($id)
    {
        $this->load->model('settingsModel');
        $cid = explode('-', $id, 2);

        $data = [
            'settings'  => $this->settingsModel->retrieve($cid['0']),
        ];
        $this->load->view('landingView', $data);
    }
}
