<?php

Class appcontroller extends getveetController {

	function __construct() {
		parent::__construct();
		$this->load->model('appmodel');
	}
	
	function index() {
		$param['items'] = $this->appmodel->get_list(array());
		$param['btn_add'] = array(
			'href' => site_url('appcontroller/invite'),
			'text' => 'INVITE NEW APP ADMIN'
		);
		$this->loadView($param,'settingsListView');
	}
	
	function invite() {
		$param = array();
		if ($this->input->post()) {
			$this->appmodel->getveet_send_mail(
				$this->input->post('email'), 
				'Congratulations, you are invited to be app admin, <a href="'.site_url('app-admin-signup').'">click here to begin</a>', 
				'Invitation for app admin', null);
		}
		$this->loadView($param,'inviteAppAdminView');
	}
	
}
