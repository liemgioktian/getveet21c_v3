<?php

class product34controller extends getveetController {

  function __construct() {
    parent::__construct();
    $this->load->model('product34model');
  }

  function index () {
    $param['items'] = $this->product34model->get_list(array());
    $this->loadView($param, 'product34ListView');
  }

  function create () {
    $post = $this->input->post();
    if ($post) {
      $this->product34model->save(array(
        'brand' => $post['brand'],
        'modelNumber' => $post['modelNumber'],
        'lcp' => $post['lcp'],
        'ratedLifetime' => $post['ratedLifetime'],
      ));
      redirect(site_url('product34controller'));
    }
    $this->loadView(null, 'product34DetailView');
  }
  
  function delete($pid, $confirm=null){
    if(is_null($confirm)){
      $this->product34model->delete($pid);
      redirect(site_url('product34controller'));
    }else $this->loadView (null, 'confirmationView');
  }
  
  function edit ($pid) {
    $post = $this->input->post();
    if ($post) {
      $this->product34model->save(array(
        'pid' => $pid,
        'brand' => $post['brand'],
        'modelNumber' => $post['modelNumber'],
        'lcp' => $post['lcp'],
        'ratedLifetime' => $post['ratedLifetime'],
      ));
      redirect(site_url('product34controller'));
    }
    $param = $this->product34model->retrieve($pid);
    $this->loadView($param, 'product34DetailView');
  }
}