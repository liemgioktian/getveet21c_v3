<?php

Class stockcontroller extends getveetController{
	
	function installer($installer_id=null, $type = 'installer'){
		if(null==$installer_id){
				
			$this->load->model(array(
				'settingsmodel',
				'installermodel'
			));
			
			$param['companies'] = $this->settingsmodel->get_list(array('appId' => $this->session->userdata('appId'), 'appAdmin' => 0));
			$this->settingsmodel->_change_table('installer');
			$param['installers'] = $this->installermodel->get_list(array('cid' => $this->session->userdata('cid')));
			$this->loadView($param,'chooseInstallerView');
			
		}else{
			$this->load->model(array(
				'stockallocationmodel',
				'cdproductmodel',
				'installermodel',
				'stockmodel',
				'stockusagemodel',
				'settingsmodel'
			));
			
			$param['products'] = $this->cdproductmodel->get_list();
			$post = $this->input->post();
			if(isset($post['allocation'])){
				foreach($param['products'] as $item){
					if(strlen($post[$item->item_id]) > 0){
						$allocation = array();
						$allocation['date'] = $post['date'];
						$allocation['item_id'] = $item->item_id;
						$allocation['installer_id'] = $installer_id;
						$allocation['stock'] = $post[$item->item_id];
						$this->stockallocationmodel->save($allocation);
					}
				}
			}
			
			if($type != 'installer'){
				$param['installer'] = $this->settingsmodel->retrieve($installer_id);
				$param['installer']['first_name'] = $param['installer']['last_name'] = ''; 
			}else $param['installer'] = $this->installermodel->retrieve($installer_id);
			
			$param['logs'] = $this->stockallocationmodel->get_list(array('installer_id' => $installer_id));
			
			// register
			$since = strtotime('01/01/1970');
			$until = time();
			if(isset($post['filter'])){
				foreach(array('since','until') as $date)
					if(strlen($post[$date]) > 0){
						$param[$date] = $post[$date];
						$$date = strtotime($post[$date]);
					}
			}
			$param['stocks'] = $this->stockmodel->get_stock_list($since, $until, $installer_id);
			
			// runningsheet
			$param['items'] = $this->stockmodel->getItems($installer_id);
			$param['forms'] = $this->stockusagemodel->getData($installer_id);
			foreach($param['forms'] as &$form){
				$items = explode(',', $form->items);
				foreach($items as $item) $form->item[] = explode('|', $item); 
			}
			$this->loadView($param, 'stockViewAll');
		}
	}

	function recalculate_out_of_stock () {
		$this->load->model(array(
		'stockusagemodel'
		));

		$allocated = array();
		$db_allocate = $this->db->query("
			SELECT stock_id, SUM(stock) as qty FROM stock_allocation GROUP BY stock_id 
		")->result();
		foreach ($db_allocate as $available) {
			$allocated[$available->stock_id] = $available->qty;
		}
		//echo '<pre>';print_r($allocated);

		$files = array();
		foreach($this->stockusagemodel->get_list(array()) as $usage){
			$file = array(
				'stock_id' => $usage->stock_id,
				'qty' => $usage->stock
			);
			$files[$usage->job_refference][] = $file;
		}

		//echo'<pre>';print_r($files);
		$proccessed = array();
		foreach ($files as $clientNumber => $items) {
			foreach ($items as $item) {
				if (!isset($allocated[$item['stock_id']])) unset($files[$clientNumber]);
				else if ($allocated[$item['stock_id']] >= $item['qty']) {
					$proccessed[] = $clientNumber;
					$allocated[$item['stock_id']] -= $item['qty'];
				} else {
					unset($files[$clientNumber]);
				}
			}
		}
		//echo'<pre>';print_r($proccessed);
		//echo'<pre>';print_r($files);

		$clientNumbers = array();
		foreach ($files as $key => $value) {
			$clientNumbers[] = $key;
		}

		if (!empty($clientNumbers))
			$this->db
			->set('out_of_stock', 0)
			->where_in('clientNumber', $clientNumbers)
			->update('file');

		die(json_encode($clientNumbers));
	}
}