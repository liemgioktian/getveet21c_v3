-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 29, 2016 at 12:59 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `getveet21c_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_34`
--

CREATE TABLE `product34` (
  `pid` int(11) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `modelNumber` varchar(255) NOT NULL,
  `lcp` varchar(255) NOT NULL,
  `ratedLifetime` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product_34`
--
ALTER TABLE `product34`
  ADD PRIMARY KEY (`pid`);
