-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 31, 2016 at 08:21 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `getveet21c_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `evidence34`
--

CREATE TABLE `evidence34` (
  `eid` int(11) NOT NULL,
  `fid34` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `evidence34`
--
ALTER TABLE `evidence34`
  ADD PRIMARY KEY (`eid`);
