-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 28, 2016 at 12:52 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `getveet21c_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `form34`
--

CREATE TABLE `form34` (
  `fid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `appId` int(11) NOT NULL,
  `appAdmin` tinyint(4) NOT NULL,
  `data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `form34`
--
ALTER TABLE `form34`
  ADD PRIMARY KEY (`fid`);
